

% create figure
Fig = figure(1);
set(Fig, 'Position', [0, 00, 1200, 760]);

allStr = cellstr(allStr_arr);

cols = 4;
rows = floor(size(allStr,1)/cols)+1;

for index = 1:size(allStr,1)
    
    subplot(rows,cols,index);
    hold on;
    grid on;
    mystr = allStr{index};
    plot(eval(genvarname(strcat('data_',mystr))));
    title(mystr);
    ex = eval(genvarname(strcat('extr_',mystr)));
    
    if (ex > 0)
        axis([0 max_iter 0 ex]);
    elseif (ex < 0)
        axis([0 max_iter ex 0]);
    else
        axis([0 max_iter 0 1]);
    end
    
    hold off;

end

% save to file?
if (saveToFile) 
    print(Fig, '-dpng', strcat(mat_path, '_plots.png'));
    save(strcat(mat_path, '_data.mat'));
end
    
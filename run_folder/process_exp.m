
% get the list of all mat files
listing = dir('*.mat');

% get the number of sets
n_sets = size(listing,1);

for i = 1:size(listing,1)
    % open the file
    
    load(listing(i).name);
    
    for index = 1:size(allStr,1)
        
        mystr = allStr{index};
        varn = strcat('data_',mystr);
        
        % get the number of variables
        if (i == 1 && index == 1)
            n_vars = size(allStr,1);
            
            % fill the data holder
            all_data = zeros(size(eval(genvarname(varn)),1), n_vars, n_sets);
            size(all_data)
        end
        
        % fill the data
        all_data(:,index, i) = eval(genvarname(varn));
                
    end
    
end

all_means = mean(all_data,3);

display('COMPUTED-NOW PLOTTING');

% create figure
Fig = figure(1);
set(Fig, 'Position', [0, 00, 1200, 760]);

cols = 4;
rows = floor(size(allStr,1)/cols)+1;

allStr = cellstr(allStr_arr);

for index = 1:size(allStr,1)
    
    subplot(rows,cols,index);
    hold on;
    grid on;
    mystr = allStr{index};
    plot(all_means(:,index));
    title(mystr);
    hold off;

end

display('PLOTTING - FINISHED');

% save to file?
% if (saveToFile) 
%     print(Fig, '-dpng', strcat(mat_path, '_plots.png'));
%     save(strcat(mat_path, '_data.mat'));
% end



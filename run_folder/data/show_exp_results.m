
Fig = figure(1);
set(Fig, 'Position', [0, 00, 1200, 760]);

cols = 2;
rows = 2;

for index = 1:size(myAllStr,1)
    
    mystr = myAllStr{index};
            
    subplot(rows,cols,index);
    hold on;
    grid on;
    
    plot([means_none(:,index) means_simple(:,index) means_heavy(:,index) means_veryheavy(:,index)]);
    
    title(mystr);
    legend('None', 'Simple', 'Heavy', 'Very Heavy', 'Location', 'NorthWest');
    
    hold off;

    
end
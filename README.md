# cic::hyperGP - by Honza Dvorsky.

This git repository holds the source code of the *cic* framework, including several example experiments (cic::symreg and cic::hyperGP so far). Our work started in early February, 2013 and the first phase ended in May, 2013.
The thesis paper is available here: [pdf version](http://cyber.felk.cvut.cz/research/theses/papers/365.pdf).
You can find videos and current progress on [my website](http://honzadvorsky.com/hypergp).

## Dependencies
- We used the robot simulator developed at the [Czech Technical University in Prague](http://www.fel.cvut.cz) by [Daniel Fiser et al](http://gitview.danfis.cz/sim).
- For XML I/O we're using the [libxml2](http://www.xmlsoft.org) library, available for almost all platforms.
- For plots exporting we used [Matlab](http://en.wikipedia.org/wiki/MATLAB) dynamic libraries. In order to use the figure exporting on your machine, you will need to have Matlab installed. This might be optional in the future, though (as I know not everyone has Matlab).
- Documentation is exported with [Doxygen](http://www.stack.nl/~dimitri/doxygen/).

## Licence
*cic* is licensed under LGPLv3 license. Text of license can be found at http://www.gnu.org/copyleft/lesser.html or in COPYING.LESSER file distributed along with sources.

## Progress (updated May 21st, 2013)
### DONE
- *binary expression tree implementation*
- *tree test classes*
- *random tree generator*
- *symreg: individual (genotype, phenotype)*
- *symreg: population*
- *symreg: control block, GP, encoding*
- *global settings*
- *xml serialization of settings, population*
- *matlab integration*
- *symreg: bloat control implementation*
- *symreg: bloat control - testing*
- *recurrent neural network implementation (hyperGP phenotype)*
- *robotic simulator plugin*
- *hyperGP: individual, population, GP, bloat control, control block (shared with symreg)*
- *hyperGP: hypercubic encoding*
- *improved Matlab integration + plots exporting of pretty much all outputs*
- *hyperGP: debugging*
- *hyperGP: testing*

#### PHASE 1 finished.

### IN PROGRESS
---

### PLANNED
- fixes, improvements
- secret plans

## Info

Please see our ever-growing [wiki](https://bitbucket.org/honzadvorsky/hypergp/wiki/Home) for more information on the project.

If you have any questions, please contact me at jan.dvorsky at yahoo dot com.
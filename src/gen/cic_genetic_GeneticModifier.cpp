/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_genetic_GeneticModifier.cpp
//  hyperGP
//
//  Created by Honza Dvorsky on 2/3/13.
//

#include "cic_genetic_GeneticModifier.h"
#include <assert.h>


#include "memoryutils.h"
#include "cic_algorithm.h"
#include "cic_tree_TreeGenerator.h"

#ifdef TARGET_SYMREG

#include "cic_genetic_symreg_Population.h"
#include "cic_genetic_symreg_Individual.h"

#endif

#ifdef TARGET_HYPERGP

#include "cic_genetic_hyperGP_Population.h"
#include "cic_genetic_hyperGP_Individual.h"

#endif

using namespace std;
using namespace cic;

//private methods
namespace cic {
    namespace genetic {
        vector<tree::Tree<genetic::tree_type>* >* generateTrees(int count, tree::GenType genType);
        
        /** Find tree in an array by comparing not the address, but the topology of the tree. */
        tree::tree_ptr_vec_t::iterator findTreeInArray(const tree::tree_t& tree, tree::tree_ptr_vec_t& array);
    }
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void genetic::GeneticModifier::init()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    cout << "GP inited." << endl;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
shared_ptr<Population> genetic::GeneticModifier::processPopulation(shared_ptr<cic::Population> _popIn) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    genetic::Population* oldpop = dynamic_cast<genetic::Population*>(_popIn.get());
    if (!oldpop) throw bad_cast();
    
    typedef genetic::Population::size_type sz_t;
    
    //if the number of individuals in the population is lower than usual (maybe they were removed in bloat control),
    //generate brand new ones and add them to the population
    
    assert(Settings::get()->_gp.generatedPopulationSize >= oldpop->size());
    
    sz_t more = Settings::get()->_gp.generatedPopulationSize - oldpop->size();
    
    typedef vector< shared_ptr< genetic::Individual > > ind_vec;
    unique_ptr<ind_vec> genInds = genetic::GeneticModifier::generateIndividuals(more, tree::GenType::PTC1);
    
    for (ind_vec::const_iterator it = genInds->begin(); it != genInds->end(); ++it) {
        //generate new individuals, add them to the population
        oldpop->addIndividual(*it);
    }
    
    //it this is not the (0th) iteration (if it is, ignore)
    if (oldpop->iteration() == 0) {
        //just an inconvenience of the place of the control block, GP needs fitness which it doesn't have in the 0th iteration
        _popIn->setIteration(1);
        return _popIn;
    }
    
    //create empty population
    genetic::Population* freshPop;
#ifdef TARGET_SYMREG
    freshPop = new genetic::symreg::Population();
#elif TARGET_HYPERGP
    freshPop = new genetic::hyperGP::Population();
#endif
    
    //increment iteration
    freshPop->setIteration(oldpop->iteration()+1);
    
    //new iter
    cout << endl << "*************************" << endl << endl;;
    
    //fill in the population with newly created or the best individuals from last population
    
    //---------------------------------------
    //----- GP OPERATORS TAKE PLACE HERE ----
    //---------------------------------------
    
    sz_t pop_size = oldpop->size();
    double portion_crossover = 0.70;
    double portion_mutation = 0.25;
    sz_t count_elitist = 1;
    
//#warning elitism 1, xover 70 %, mut 20 %, reprod
    
    if (portion_crossover + portion_mutation > 1) throw logic_error("please fix you portions so that they sum up under 1.00");
    
    sz_t count_crossover = portion_crossover*pop_size;
    //must be even
    if (count_crossover % 2 != 0) count_crossover--;
    sz_t count_mutation = portion_mutation*pop_size;
    int cnt = (int)(pop_size - count_crossover - count_mutation - count_elitist);
    assert(cnt >= 0);
    sz_t count_reproduction = cnt;
    
    shared_ptr<genetic::Individual> indA = nullptr, indB = nullptr, childA = nullptr, childB = nullptr;
    Selection selType = (Selection)Settings::get()->_gp.selection_type;
    
    pair< shared_ptr<genetic::Individual>, shared_ptr<genetic::Individual> > xover_pair;
    
    //first do the crossover
    for (sz_t i = 0; i < count_crossover/2; ++i) {
        indA = GeneticModifier::getIndWithSelectionFromPopulation(selType, oldpop);
        indB = GeneticModifier::getIndWithSelectionFromPopulation(selType, oldpop);
        xover_pair = this->crossoveredIndividuals(indA, indB);
        freshPop->addIndividual(xover_pair.first);
        freshPop->addIndividual(xover_pair.second);
    }
    
    //then mutation
    for (sz_t i = 0; i < count_mutation; ++i) {
        indA = GeneticModifier::getIndWithSelectionFromPopulation(selType, oldpop);
        indA = this->mutatedIndividual(indA);
        freshPop->addIndividual(indA);
    }
    
    //then reproduction
    for (sz_t i = 0; i < count_reproduction; ++i) {
        indA = GeneticModifier::getIndWithSelectionFromPopulation(selType, oldpop);
        freshPop->addIndividual(indA);
    }
    
    //if the number of elitists is > 0, sort the population by fitness so that we can choose the best count_elitist individuals
    if (count_elitist > 0) {
        
        //sort them first
        sort(oldpop->begin(), oldpop->end(), Population::compareFitness);
        
        genetic::Population::iterator it = oldpop->begin();
        
        //the rest is elitist selection - just put them into the new population
        for (sz_t i = 0; i < count_elitist; ++i) {
            //copy the count_elitist best individuals into the new population (thus we never lose the best ind)
            freshPop->addIndividual(*it++);
        }
    }
    
    //---------------------------------------
    
    //we have our correct population, proceed
    cout << "Iteration: " << freshPop->iteration() << " : Population " << freshPop << " just passed through GP!" << endl;
    
    //cast back up
    return shared_ptr<cic::Population>(freshPop);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
shared_ptr<genetic::Individual> genetic::GeneticModifier::mutatedIndividual(shared_ptr<genetic::Individual> ind)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //clone, mutate and return
    shared_ptr<genetic::Individual> fresh = shared_ptr<genetic::Individual>(dynamic_cast<genetic::Individual*>(ind->clone()));
    
    fresh->genotype()->mutateGenotype();
    fresh->addParent(ind);
//#warning NOT SAVING PARENTS
    
    return fresh;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
pair< shared_ptr<genetic::Individual>, shared_ptr<genetic::Individual> >
genetic::GeneticModifier::crossoveredIndividuals(shared_ptr<genetic::Individual> indA, shared_ptr<genetic::Individual> indB)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //clone both, crossover and return
    shared_ptr<genetic::Individual> freshA = shared_ptr<genetic::Individual>(dynamic_cast<genetic::Individual*>(indA->clone()));
    shared_ptr<genetic::Individual> freshB = shared_ptr<genetic::Individual>(dynamic_cast<genetic::Individual*>(indB->clone()));
    
    freshA->genotype()->crossoverWithGenotype(freshB->genotype());
    
    freshA->addParent(indA);
    freshA->addParent(indB);
    freshB->addParent(indA);
    freshB->addParent(indB);
//#warning NOT SAVING PARENTS
    
    return pair< shared_ptr<genetic::Individual>, shared_ptr<genetic::Individual> >(freshA, freshB);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
shared_ptr<genetic::Individual> genetic::GeneticModifier::getIndWithSelectionFromPopulation(Selection sel,
                                                                                       const genetic::Population* pop)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    vector<double> probs;
    
    switch (sel) {
        case Selection::Tournament:
        {
            /*
             tournament selection. chooses k individuals from population at random.
             selects the best individual,
             */
            
            //the tournament size
            int k = Settings::get()->_gp.tournament_size;
            
            assert(k < Settings::get()->_gp.generatedPopulationSize);
            
            vector< shared_ptr<genetic::Individual> > tournament_pool; //the tournament pool
            vector<double> probs = alg::equalProbs((int)pop->size());
            
            //choose k individuals from the population at random
            for (int i = 0, index; i < k; ++i) {
                index = alg::randomInteger(probs);
                tournament_pool.push_back((*pop)[index]);
            }
            
            //sort them by fitness
            sort(tournament_pool.begin(), tournament_pool.end(), genetic::Population::compareFitness);
            
            //get the chosen individual
            shared_ptr<genetic::Individual> selected = tournament_pool[0];
            
            //use lexicographic parsimony pressure?
            if (Settings::get()->_gp.tournamentSelectionUseLexicographicParsimonyPressure) {
                
                //define 'equal' edge
                genetic::Fitness equal_diff = pow(10, -10);
                
                //search the tournament pool for individuals that have the same fitness as the selected one
                //if you find some other ones, choose the smallest one (smallest depth)
                vector< shared_ptr<genetic::Individual> >::const_iterator it = tournament_pool.begin();
                vector< shared_ptr<genetic::Individual> > sameFit;
                
                for (; it != tournament_pool.end(); ++it) {
                    if (abs((*it)->fitness() - selected->fitness()) < equal_diff
                        && (*it)->genotype()->depth() < selected->genotype()->depth())
                    {
                        sameFit.push_back(*it);
                    }
                }
                
                //find out if there are any candidates
                if (sameFit.size() > 0) {
                    //ok, choose the smallest one
                    selected = *min_element(sameFit.begin(), sameFit.end(), Population::compareDepth);
                }
            }
            
            return selected;
        }
            break;
        case Selection::Roulette:
        {
            /*
             Roulette wheel selection (also fitness-proportional selection).
             All individuals have a probability of being selected.
             The probability is directly proportional to their fitness.
             */
                    
            //create a vector of probabilities == fitnesses of the whole population.
            for (genetic::Population::const_iterator it = pop->begin(); it != pop->end(); ++it) {
                probs.push_back((*it)->fitness());
            }
            
            //make sure the lowest fitness is == 1 and all others are increased accordingly (because we can't assign negative probs)
            double diff = (1.0 - *std::min_element(probs.begin(), probs.end()));
            for (vector<double>::iterator it = probs.begin(); it != probs.end(); ++it) (*it) += diff;
            
            //get an integer of the selected individual
            int idx = alg::randomInteger(probs);
            return (*pop)[idx];
        }
            break;
        default:
            break;
    }
    throw logic_error("unrecognized selection type!");
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
unique_ptr< vector< shared_ptr<genetic::Individual> > > genetic::GeneticModifier::generateIndividuals(size_t count, tree::GenType genType)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    unique_ptr< vector< shared_ptr<genetic::Individual> > > individuals =
    make_unique< vector< shared_ptr<genetic::Individual> > >();
    
    int treesPerForest = Settings::get()->_gp.numberOfTreesInForestGenotype;
    int cnt = (int)count*treesPerForest;
    
    vector<tree::Tree<tree_type>*>* ts = genetic::generateTrees(cnt, genType);
    unique_ptr< vector<tree::Tree<tree_type>* > > trees =
    unique_ptr<vector<tree::Tree<tree_type>* > >(ts);
    
    //create individuals from generated trees
    genetic::Genotype* gen = nullptr;
    shared_ptr<genetic::Individual> ind = nullptr;
    tree::Forest<tree_type> * forest = nullptr;
    
    for (int i = 0; i < count; ++i) {
        
        forest = new tree::Forest<tree_type>(treesPerForest);
        
        //create forest, fill the trees, assign to individual
        for (int j = i*treesPerForest; j < (i+1)*treesPerForest; ++j) {
            forest->setTreeAtIndex(trees->at(j), j - i*treesPerForest);
        }
        
#ifdef TARGET_SYMREG
        gen = new genetic::symreg::Genotype(forest);
        ind = make_shared<genetic::symreg::Individual>(gen);
#elif TARGET_HYPERGP
        gen = new genetic::hyperGP::Genotype(forest);
        ind = make_shared<genetic::hyperGP::Individual>(gen);
#endif
        
        individuals->push_back(ind);
    }

    trees = nullptr;
    return individuals;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
tree::tree_ptr_vec_t::iterator genetic::findTreeInArray(const tree::tree_t& tree, tree::tree_ptr_vec_t& array)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    typedef tree::tree_ptr_vec_t::iterator it_t;
    it_t it = array.begin();
    
    for (; it != array.end(); ++it) {
        if (**it == tree) {
            return it;
            break;
        }
    }
    return array.end();
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
vector<tree::Tree<genetic::tree_type>* >* genetic::generateTrees(int count, tree::GenType genType)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    vector<tree::Tree<genetic::tree_type>* >* trees = new vector<tree::Tree<genetic::tree_type>* >();

    if (count < 1) return trees;
    
    tree::TreeGenerator* gen = tree::TreeGenerator::get();
    
    /*
     PTC1
     With eSize == 6 -> avg depth == ~2
     With eSize == 20 -> avg depth == ~5
     */
    
    int eSize = Settings::get()->_gp.expectedGeneratedTreeSize;
    int maxPredicted = 30;
    int maxDepth = Settings::get()->_gp.maximumGeneratedTreeDepth;
    int minDepth = Settings::get()->_gp.minimumGeneratedTreeDepth;
    
    vector<int> occurSize(maxPredicted);
    vector<int> occurDepth(maxDepth+1);
    int gens = count;
    
    //----------------------------------------------------------------------------
    
    //generate probabilities for terminals
    vector<double> termPs(Settings::get()->_gp.terminalVariableCount+Settings::get()->_gp.terminalConstsCount);
    fill(termPs.begin(), termPs.end(), -1); //pre-fail the vector so that we know we assigned every node
    
    //generate probabilities for functions
    vector<double> funPs(Settings::get()->_gp.functionCount);
    fill(funPs.begin(), funPs.end(), -1); //pre-fail the vector so that we know we assigned every node
    
    //prepare the probabilities
    //    {
    //get the from settings
    vector<double> termsSettings = Settings::get()->_gp.terminalProbabilities;
    vector<double> funSettings = Settings::get()->_gp.functionProbabilities;
    
    std::copy(termsSettings.begin(), termsSettings.end(), termPs.begin());
    std::copy(funSettings.begin(), funSettings.end(), funPs.begin());
    //    }
    
    //ensure the right count of the probabilities in both terminals and functions
    for (vector<double>::const_iterator it = termPs.begin(); it != termPs.end(); ++it)
        if (*it < 0) throw logic_error("probability cannot be negative!");
    for (vector<double>::const_iterator it = funPs.begin(); it != funPs.end(); ++it)
        if (*it < 0) throw logic_error("probability cannot be negative!");
    
    //----------------------------------------------------------------------------
    
    //also compute the average size
    double avgsumSize = 0;
    double avgsumDepth = 0;
    int floatingOccurSize;
    int floatingOccurDepth;
    
    int duplicates = 0;
    
    if (genType == tree::GenType::RAMPED_HALF_AND_HALF) {
        
        int withFull = gens/2;
        int withGrow = gens - withFull;
        
        //number of depth steps
        int countForEachStep = withFull / (maxDepth - minDepth + 1);
        int fullCount = 0;
        
        for (int level = minDepth; level <= maxDepth; ++level) {
            
            for (int i = 0; i < countForEachStep; ++i) {
                
                //
                unique_ptr< tree::Tree<genetic::tree_type> > tree;
                
                tree = gen->genTree(tree::GenType::FULL,
                                    tree::GenParam(level, level, eSize),
                                    tree::GenFilter(minDepth),
                                    tree::GenProbs(termPs, funPs));
                
                if (Settings::get()->_gp.preventDuplicates) {
                    
                    //is duplicate?
                    tree::tree_ptr_vec_t::iterator it;
                    if ((it = genetic::findTreeInArray(*tree, *trees)) != trees->end()) {
//                    cout << "Found duplicates: " << endl << tree->description() << endl << (*it)->description() << endl;
                        
                        i--;
                        duplicates++;
                        //we need to generate another one
                        continue;
                    }
                }
                
                floatingOccurSize = tree->nodeCount();
                floatingOccurDepth = tree->depth();
                if (floatingOccurSize < maxPredicted) occurSize[floatingOccurSize]++;
                if (floatingOccurDepth <= maxDepth) occurDepth[floatingOccurDepth]++;
                avgsumSize += floatingOccurSize;
                avgsumDepth += floatingOccurDepth;
                
                //add to the vector
                trees->push_back(tree.release());
                
                fullCount++;
                
            }
        }
        
        //rounding might have caused to not generate sufficient number of trees
        //we need to generate the rest randomly in the desired range [minDepth, maxDepth].
        
        int rnd = withFull - fullCount;
        for (int i = 0; i < rnd; ++i) {

            int index = minDepth + alg::randomInteger(alg::equalProbs(maxDepth - minDepth + 1));
//            cout << "--- Gen'd: " << index << " from range [" << minDepth << ", " << maxDepth << "]" << endl;
            
            //
            unique_ptr< tree::Tree<genetic::tree_type> > tree;
            
            tree = gen->genTree(tree::GenType::FULL,
                                tree::GenParam(index, index, eSize),
                                tree::GenFilter(minDepth),
                                tree::GenProbs(termPs, funPs));
            
            if (Settings::get()->_gp.preventDuplicates) {
                
                //is duplicate?
                tree::tree_ptr_vec_t::iterator it;
                if ((it = genetic::findTreeInArray(*tree, *trees)) != trees->end()) {
                    //                    cout << "Found duplicates: " << endl << tree->description() << endl << (*it)->description() << endl;
                    
                    i--;
                    duplicates++;
                    //we need to generate another one
                    continue;
                }
            }
            
            floatingOccurSize = tree->nodeCount();
            floatingOccurDepth = tree->depth();
            if (floatingOccurSize < maxPredicted) occurSize[floatingOccurSize]++;
            if (floatingOccurDepth <= maxDepth) occurDepth[floatingOccurDepth]++;
            avgsumSize += floatingOccurSize;
            avgsumDepth += floatingOccurDepth;
            
            //add to the vector
            trees->push_back(tree.release());
            
            fullCount++;            
        }
        
        assert(fullCount == withFull);
        
        for (int i = 0; i < withGrow; ++i) {
            unique_ptr< tree::Tree<genetic::tree_type> > tree;
            
            tree = gen->genTree(tree::GenType::GROW,
                                tree::GenParam(maxDepth, minDepth, eSize),
                                tree::GenFilter(minDepth),
                                tree::GenProbs(termPs, funPs));
            
            if (Settings::get()->_gp.preventDuplicates) {
                
                //is duplicate?
                tree::tree_ptr_vec_t::iterator it;
                if ((it = genetic::findTreeInArray(*tree, *trees)) != trees->end()) {
//                    cout << "Found duplicates: " << endl << tree->description() << endl << (*it)->description() << endl;
                    
                    i--;
                    duplicates++;
                    
                    //we need to generate another one
                    continue;
                }
            }
            
            floatingOccurSize = tree->nodeCount();
            floatingOccurDepth = tree->depth();
            if (floatingOccurSize < maxPredicted) occurSize[floatingOccurSize]++;
            if (floatingOccurDepth <= maxDepth) occurDepth[floatingOccurDepth]++;
            avgsumSize += floatingOccurSize;
            avgsumDepth += floatingOccurDepth;
            
            //add to the vector
            trees->push_back(tree.release());
        }        
        
    } else {
        
        for (int i = 0; i < gens; ++i) {
            unique_ptr< tree::Tree<genetic::tree_type> > tree;
            
            tree = gen->genTree(genType,
                                tree::GenParam(maxDepth, minDepth, eSize),
                                tree::GenFilter(minDepth),
                                tree::GenProbs(termPs, funPs));
            
            if (Settings::get()->_gp.preventDuplicates) {
                
                //is duplicate?
                tree::tree_ptr_vec_t::iterator it;
                if ((it = genetic::findTreeInArray(*tree, *trees)) != trees->end()) {
//                    cout << "Found duplicates: " << endl << tree->description() << endl << (*it)->description() << endl;
                    
                    //subtract index by TWO. idk why, but it doesn't work otherwise to keep the same idx :-/
                    i -= 2;
                    
                    duplicates++;
                    
                    //we need to generate another one
                    continue;
                }
            }
            
            floatingOccurSize = tree->nodeCount();
            floatingOccurDepth = tree->depth();
            if (floatingOccurSize < maxPredicted) occurSize[floatingOccurSize]++;
            if (floatingOccurDepth <= maxDepth) occurDepth[floatingOccurDepth]++;
            avgsumSize += floatingOccurSize;
            avgsumDepth += floatingOccurDepth;
            
            //add to the vector
            trees->push_back(tree.release());
        }
    }
    
    if (Settings::get()->_gp.preventDuplicates) {
        cout << "Tree Generator: Generated and removed " << duplicates << " duplicates." << endl;
    }
    
    cout << "Tree Generator: The average tree size is " << avgsumSize/gens << "." << endl;
    cout << "Tree Generator: The average tree depth is " << avgsumDepth/gens << "." << endl;

    //return the trees
    return trees;
}

/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_genetic_Individual.h
//  hyperGP
//
//  Created by Honza Dvorsky on 2/2/13.
//

#ifndef __hyperGP__cic_genetic_Individual__
#define __hyperGP__cic_genetic_Individual__

#include "cic_Individual.h"
#include <string>
#include <vector>
#include <map>
#include <memory>
#include "cic_Settings.h"
#include "cic_tree_forest.h"

namespace cic {
    namespace genetic {
        class Individual;
        class Phenotype;
        class Genotype;
        
        typedef double Fitness;
        typedef double tree_type;

    } //namespace genetic
    
    namespace io {
        class xmlAccess;
    }
} //namespace cic

/** Genetic type: phenotype for all genetic algorithms. All phenotypes have to be subclasses of this abstract class. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::genetic::Phenotype
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    
public:
    /** The type for the size of the phenotype. */
    typedef size_t size_type;
    
    /** Compute the value for inputs. */
    virtual tree_type valueForInputs(const std::vector<tree_type>& inputs) const = 0;
    
    /** Get string description of the phenotype. */
    virtual std::string description() const = 0;
    virtual ~Phenotype() { };
    
    /** Get a copy of the phenotype. */
    virtual Phenotype* clone() = 0;
};

/** Genetic type: ghenotype for all genetic algorithms. All genotypes have to be subclasses of this abstract class. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::genetic::Genotype
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    friend class io::xmlAccess;
    
protected:
    /** The underlying tree of the genotype. */
    tree::Forest<genetic::tree_type>*      _forest;
    
public:
    /** The type for the size of the genotype. */
    typedef size_t                      size_type;
    
//    /** Compute the value for inputs. */
//    virtual tree_type                   valueForInputs(const std::vector<tree_type>& inputs) const;
    
    /** Get string description of the genotype. */
    virtual std::string                 description() const { return "\n Genotype: \n" + _forest->humanReadable() + "\n" + _forest->description() + "\n"; };

    /** Create genotype for genetic experiment with a tree. */
    Genotype(tree::Forest<genetic::tree_type>* fr) : _forest(fr) { };

    virtual                             ~Genotype() { delete _forest; };
    
    /** Mutates genotype (replaces random subtree in each tree (in forest) with a freshly generated one). */
    virtual void                        mutateGenotype();
    
    /** Crossoveres with another genotype. */
    virtual void                        crossoverWithGenotype(Genotype * otherGen);
    
    /** Returns a human readable form. */
    virtual std::string                 humanReadable() const { return _forest->humanReadable(); };
    
    /** Returns the depth of the genotype (forest). */
    virtual int                         depth() const { return _forest->totalDepth(); };
    
    /** Returns the size of the genotype (number of nodes in the underlying forest). */
    virtual int                         size() const { return _forest->totalSize(); };
    
    /** Returns a (deep) copy of the Genotype. */
    virtual Genotype*                   clone() = 0;
    
    /** Return the readonly forest. */
    virtual const tree::Forest<tree_type>* forest() const { return _forest; };
};

/** Type of Individual used in genetic algorithms. Needs to have a genotype and a phenotype. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::genetic::Individual : public cic::Individual
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
protected:
    /** Genotype of the individual. */
    cic::genetic::Genotype*                                 _genotype;
    
    /** Phenotype of the individual. */
    cic::genetic::Phenotype*                                _phenotype;
    
    /** Fitness value of the individual. */
    cic::genetic::Fitness                                   _fitness;
    
    /** Vector or pointers to parents. */
    std::vector< std::shared_ptr<genetic::Individual> >*    _parents;
    
    /** Runtime attributes, for example for bloat control etc. */
    std::map<std::string, double>*                          _runtimeAttributes;

public:
    
    /** Return the fitness of the individual. Default fitness if it has not been assigned yet. */
    virtual Fitness                         fitness() const { return _fitness; };
    
    /** Set fitness to the individual. The job of the simulator. */
    virtual void                            setFitness(Fitness val) { _fitness = val; };
    
    /** Get read-only genotype. */
    virtual Genotype*                       genotype() const { return _genotype; };
    
    /** Get read-only phenotype. */
    virtual const Phenotype*                phenotype() const { return _phenotype; };
    
    /** Set phenotype (just once, will throw an exception if trying to reset it). */
    virtual void                            setPhenotype(std::unique_ptr<Phenotype> enc);

    /** Getter for runtime attribute. */
    virtual double                          runtimeAttributeValue(std::string key) const;
    
    /** Setter for runtime attribute. */
    virtual void                            setRuntimeAttributeValueForKey(double value, std::string key);
    
    /** Adds a parent. */
    virtual void                            addParent(std::shared_ptr<Individual> par);
    
    /** Removes all parents. */
    virtual void                            removeParents();
    
    /** Getter for parents. */
    virtual std::vector< std::shared_ptr<genetic::Individual> >* parents() const;
    
    /** Construct an Individual with its genotype. */
    Individual(Genotype* gen) : _genotype(gen), _phenotype(nullptr), _fitness(Settings::get()->_gp.defaultFitness)
    { _parents = new std::vector< std::shared_ptr<genetic::Individual> >();
        _runtimeAttributes = new std::map<std::string, double>();
    };
    ~Individual() {
//        std::cout << "Deleting ind " << std::endl;
        delete _genotype; delete _phenotype; delete _parents; delete _runtimeAttributes; };
    
    std::string description() { return "Ind: \n" + _genotype->description() + "\n" + (_fitness!=Settings::get()->_gp.defaultFitness?cic::to_string("Fitness: %f", _fitness):"") + "\n" + std::string(_phenotype ? _phenotype->description() : "") + "\n"; };
    
};

#endif /* defined(__hyperGP__cic_genetic_Individual__) */









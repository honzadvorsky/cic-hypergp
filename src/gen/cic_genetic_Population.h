/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_genetic_Population.h
//  hyperGP
//
//  Created by Honza Dvorsky on 2/2/13.
//

#ifndef __hyperGP__cic_genetic_Population__
#define __hyperGP__cic_genetic_Population__

#include <memory>
#include <map>
#include <vector>
#include <iostream>
#include "cic_Population.h"

namespace cic {
    namespace genetic {
        class Population;
        class Individual;
    } //namespace genetic
    
    namespace io {
        class xmlAccess;
    }
} //namespace cic

/** Genetic population class for genetic-based experiments.
    Implemented as a vector.
 */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::genetic::Population : public cic::Population
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    friend class cic::io::xmlAccess;
 
protected:
    /** Container of the Individuals. */
    std::vector< std::shared_ptr<cic::genetic::Individual> > *_inds;
    
    /** Statistics map. */
    std::map<std::string, double> *_statistics;

public:
    /** Define our saved type. */
    typedef std::shared_ptr<cic::genetic::Individual> T;

    /** Define the underlying storage type.
     Note: Must be visible because of the iterator types :( */
    typedef std::vector<T> T_vec;
    
    /** Define iterator. */
    typedef T_vec::iterator iterator;
    
    /** Define const iterator. */
    typedef T_vec::const_iterator const_iterator;
    
    /** Define size type for population. */
    typedef size_t size_type;
        
    /** Define the type iterated in our population. */
    typedef T value_type;
    
    /** Return the size of the container. */
    virtual size_type size() const { return _inds->size(); };
    
    /** [] operator for population. */
    virtual T& operator[](size_type i) { return (*_inds)[i]; };
    
    /** Const [] operator for the population. */
    virtual const T& operator[](size_type i) const { return (*_inds)[i]; };
    
    /** Erases all items. */
    virtual void clear() { _inds->clear(); };
    
    /** Returns iterator pointing to the beginning of the Population. */
    virtual iterator begin() { return _inds->begin(); };
    
    /** Returns constant iterator pointing to the beginning of the Population. */
    virtual const_iterator begin() const { return _inds->begin(); };
    
    /** Returns iterator pointing to the end of the Population. */
    virtual iterator end() { return _inds->end(); };
    
    /** Returns constant iterator pointing to the end of the Population. */
    virtual const_iterator end() const { return _inds->end(); };
    
    /** Based on the shared pointer passed in, finds the same object (individual) 
     in the population and returns its index. Returns the Population size if not found. */
    virtual size_type find(std::shared_ptr<cic::genetic::Individual> ind) const;
    
    /** Adds individual to the population. */
    virtual void addIndividual(std::shared_ptr<cic::genetic::Individual> ind);
    
    /** Erases individual from the population, based on iterator. */
    virtual void eraseIndividual(iterator ind);
    
    /** Getter for statistics value. 
     syntax of keys:
     key: "{min, max, avg}_{fitness, depth, size}"
     examples of keys: "min_fitness", "avg_size", ...
     
     If 0 is returned, there is a chance that the value has not been saved in (0 is default val for double).
     */
    virtual double statisticsValueForKey(std::string key) const;
    
    /** Setter for statistics value. */
    virtual void setStatisticsValueForKey(std::string key, double value);
    
    /** Get read-only map with all statistics data. */
    virtual const std::map<std::string, double>* statistics_data() const { return _statistics; };
    
    /** Get string representation of the population statistics. */
    virtual std::string statistics() const;
    
    /** Returns the best individual (by fitness). */
    virtual cic::genetic::Individual* bestIndividual() const;
    
    /** Comparator for sorting. Returns true if fitness of i > j. */
    static bool compareFitness (std::shared_ptr<genetic::Individual> i, std::shared_ptr<genetic::Individual> j);
    
    /** Comparator for sorting. Returns true if fitness of i > j. */
    static bool compareDepth (std::shared_ptr<genetic::Individual> i, std::shared_ptr<genetic::Individual> j);
    
    /** Comparator for sorting. Returns true if fitness of i > j. */
    static bool compareSize (std::shared_ptr<genetic::Individual> i, std::shared_ptr<genetic::Individual> j);
    
    /** Description string. */
    std::string description() const;
    
    Population();
    ~Population() { delete _inds; delete this->_statistics; };
    
};

#endif /* defined(__hyperGP__cic_genetic_Population__) */















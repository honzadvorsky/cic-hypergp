/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_genetic_GeneticModifier.h
//  hyperGP
//
//  Created by Honza Dvorsky on 2/3/13.
//

#ifndef __hyperGP__cic_genetic_GeneticModifier__
#define __hyperGP__cic_genetic_GeneticModifier__

#include "cic_PopulationModifier.h"
#include "cic_genetic_Individual.h"
#include "cic_genetic_Population.h"
#include "cic_tree_TreeGenerator.h"

namespace cic {
    namespace genetic {
        class GeneticModifier;
        
    } //namespace genetic
    
    namespace tree {
        typedef tree::Tree<genetic::tree_type> tree_t;
        typedef std::vector<tree_t*> tree_ptr_vec_t;
    } //namespace tree
} //namespace cic

/** Common GP for genetic experiments. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::genetic::GeneticModifier : public cic::PopulationModifier
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
protected:
    
    /** Selection types. */
    enum Selection {
        Tournament,
        Roulette,
        _LAST_ITEM
    };
    
    /** Choose individual based on selection from population. */
    static std::shared_ptr<cic::genetic::Individual> getIndWithSelectionFromPopulation(Selection sel, const cic::genetic::Population* pop);
    
    /** Returns a new, mutated copy of the input individual. */
    static std::shared_ptr<cic::genetic::Individual> mutatedIndividual(std::shared_ptr<cic::genetic::Individual> ind);
    
    /** Returns a pair of new, crossovers copies of the input individuals. */
    static std::pair< std::shared_ptr<cic::genetic::Individual>, std::shared_ptr<cic::genetic::Individual> > crossoveredIndividuals(std::shared_ptr<cic::genetic::Individual> indA, std::shared_ptr<cic::genetic::Individual> indB);
    
public:
    
    void        init();
    
    /** Process population, aka apply genetic operators on individuals, create and fill a new population. */
    std::shared_ptr<cic::Population> processPopulation(std::shared_ptr<cic::Population> popIn) const;
    
    /** Generates 'count' random individuals (uses the Tree Generator). */
    static std::unique_ptr< std::vector< std::shared_ptr<genetic::Individual> > > generateIndividuals(size_t count, cic::tree::GenType genType);
    
};

#endif /* defined(__hyperGP__cic_genetic_GeneticModifier__) */

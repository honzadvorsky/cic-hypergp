/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_genetic_Encoding.h
//  hyperGP
//
//  Created by Honza Dvorsky on 2/3/13.
//

#ifndef __hyperGP__cic_genetic_Encoding__
#define __hyperGP__cic_genetic_Encoding__

#include "cic_PopulationModifier.h"
#include "cic_genetic_Individual.h"
#include <memory>

namespace cic {
    namespace genetic {
        class Encoding;
    } //namespace genetic
} //namespace cic

/** Encoding block - generates phenotypes for genotypes. Handles the population. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::genetic::Encoding : public cic::PopulationModifier
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    
protected:
    /** Generate a phenotype for genotype. */
    virtual std::unique_ptr<cic::genetic::Phenotype> generatePhenotypeForGenotype(const cic::genetic::Genotype* gen) const = 0;
public:
    
    /** Process population, aka create phenotypes for all individuals. */
    std::shared_ptr<cic::Population> processPopulation(std::shared_ptr<cic::Population> popIn) const;
    
};

#endif /* defined(__hyperGP__cic_genetic_Encoding__) */

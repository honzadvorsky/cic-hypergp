/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_genetic_ControlBlock.cpp
//  hyperGP
//
//  Created by Honza Dvorsky on 2/5/13.
//

#include "cic_genetic_ControlBlock.h"

#include <iostream>
#include <limits>
#include <algorithm>

#include "memoryutils.h"
#include "cic_stringutils.h"
#include "cic_Settings.h"
#include "cic_io_xmlHandler.h"
#include "cic_io_matlab_handler.h"
#include "cic_time.h"

#include "cic_tree_forest.h"

#ifdef TARGET_SYMREG

#include "cic_genetic_symreg_Individual.h"
#include "cic_genetic_symreg_Population.h"

#endif

#ifdef TARGET_HYPERGP

#include "cic_genetic_hyperGP_Individual.h"
#include "cic_genetic_hyperGP_Population.h"

#endif

using namespace std;
using namespace cic;

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
genetic::ControlBlock::ControlBlock()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //create all needed blocks
    
#ifdef TARGET_SYMREG
    
    _gp = new symreg::GP();
    _encoding = new symreg::Encoding();
    _sim = new symreg::Simulator();
    
#endif
    
#ifdef TARGET_HYPERGP
    
    _gp = new hyperGP::GeneticProgramming();
    _encoding = new hyperGP::HypercubicEncoding();
    _sim = new hyperGP::Simulator();
    
#endif
    
    cout << "Control Block created." << endl;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void genetic::ControlBlock::initExperimentWithParams(int argc, const char ** argv)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    string settings_filename;
    
    //parse settings path
    if (argc > 1) {
        settings_filename = argv[1];
    } else {
#ifdef TARGET_SYMREG
        settings_filename = "symreg_settings.xml"; //empty string means don't load from xml
#endif
#ifdef TARGET_HYPERGP
        settings_filename = "hypergp_settings.xml"; //empty string means don't load from xml
#endif
    }
    
    //create settings, conditionally load them from file
    Settings * set = Settings::get();
    if (set->loadSettings(settings_filename)) {
        cout << "Settings loaded from file: " << settings_filename << "!" << endl;
    } else {
        cout << "!\n!\n!\n!\n!\n!\n" <<"Settings couldn't be loaded, creating new from defaults." << "!\n!\n!\n!\n!\n!\n" << endl;
    }
    
    // init all needed blocks
    this->init();
    _gp->init();
    _encoding->init();
    _sim->init();
    
    if (Settings::get()->_bloat.useBloatControl) {
        
#ifdef TARGET_SYMREG
        _bloat = new symreg::BloatControl();
#endif
        
#ifdef TARGET_HYPERGP
        
        _bloat = new hyperGP::BloatControl();
#endif
        
        _bloat->init();
    } else {
        _bloat = nullptr;
    }
    
    //setup the limits
    this->_maximum_iterations = Settings::get()->_control.maximumIterations;
    this->_minimum_fitness = Settings::get()->_control.minimumFitness;    
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void genetic::ControlBlock::init()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    cout << "Control Block inited." << endl;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void genetic::ControlBlock::start(int argc, const char ** argv)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //forward to the experiment initializer
    this->initExperimentWithParams(argc, argv);
    
    //******************************************
    bool ifLoadedFromFileThenShowBestOnStart = Settings::get()->_control.showAndQuitOnInputPopulation;
    bool showInitialsAndEnd = false;
    bool showBestIndividualAtTheEnd = true;
    bool isAutonomousExperiment = Settings::get()->_control.isAutonomousExperiment;
    //******************************************
    
    //setup properties
    shared_ptr<cic::Population> pop = nullptr;
    unique_ptr<io::xmlHandler> xml = make_unique<io::xmlHandler>();
    bool savePopToXML = true;
    bool saveSettingsToXML = true;
    string filename = Settings::get()->_control.populationXmlLoadFrom;
    
    //generate initial population
    if (filename.empty()) {
        int inds = Settings::get()->_gp.generatedPopulationSize;
        
        genetic::Population * sympop = nullptr;
        
#ifdef TARGET_SYMREG
        pop = make_shared<genetic::symreg::Population>();
        sympop = dynamic_cast<genetic::symreg::Population*>(pop.get());
#endif
        
#ifdef TARGET_HYPERGP
        pop = make_shared<genetic::hyperGP::Population>();
        sympop = dynamic_cast<genetic::hyperGP::Population*>(pop.get());
#endif
        
        //generate individuals
        typedef  vector< shared_ptr<genetic::Individual> > ind_vec;
        unique_ptr<ind_vec> genInds = GeneticModifier::generateIndividuals(inds, tree::GenType::RAMPED_HALF_AND_HALF);
            
        for (ind_vec::iterator it = genInds->begin(); it != genInds->end(); ++it) {
            sympop->addIndividual(*it);
        }
        
    } else {
        pop = xml->loadPopulationFromXml(filename.c_str());
        if (!pop) {
            throw logic_error("input xml file couldn't find a population.");
        }
        cout << "Control Block: Population loaded from file " << filename << endl;
        
        if (ifLoadedFromFileThenShowBestOnStart) {
            cout << "Showing the best individual on start" << endl;
            
            const genetic::hyperGP::Population* mypop = dynamic_cast<genetic::hyperGP::Population*>(pop.get());
            const genetic::hyperGP::Simulator* mysim = dynamic_cast<genetic::hyperGP::Simulator*>(_sim);
            genetic::hyperGP::Individual* myind = dynamic_cast<genetic::hyperGP::Individual*>(mypop->bestIndividual());
            mysim->simulateIndividual(myind, true, true);
            
            cout << "Ending, this was just a showoff session" << endl;
            if (Settings::get()->_control.useMatlabPlot) {
                io::MatlabHandler::get()->destroy();
            }
            return;
        }
    }
    
    //should export the initial population?
    
#ifdef TARGET_HYPERGP

    if (showInitialsAndEnd) {
        
        genetic::Population * genpop = dynamic_cast<genetic::hyperGP::Population*>(pop.get());
        const genetic::hyperGP::Simulator* mysim = dynamic_cast<genetic::hyperGP::Simulator*>(_sim);

        for (auto it = genpop->begin(); it != genpop->end(); ++it) {
            string appendix = cic::to_string("Ind%i", std::distance(genpop->begin(), it));
            genetic::hyperGP::Individual* myind = dynamic_cast<genetic::hyperGP::Individual*>(it->get());
            mysim->simulateIndividual(myind, false, true, appendix);
        }
        
        //save the population as is
        xml->savePopulationToXml(genpop);
        
        cout << "Ending, just showed individuals." << endl;
        if (Settings::get()->_control.useMatlabPlot) {
            io::MatlabHandler::get()->destroy();
        }
        return;
    }
    
#endif

    if (!_bloat) {
        cout << "Bloat control is disabled." << endl;
    }
    
    long begin_millis = tm::currentMs();
    long begin_loop_millis;
    
    //run
    while (!this->shouldStop(pop)) {

        begin_loop_millis = tm::currentMs();
        
#ifdef TARGET_HYPERGP
        const genetic::hyperGP::Population* mypop = dynamic_cast<genetic::hyperGP::Population*>(pop.get());
#elif TARGET_SYMREG
        const genetic::symreg::Population* mypop = dynamic_cast<genetic::symreg::Population*>(pop.get());
#endif
        
        pop = _gp->processPopulation(pop);
        pop = _encoding->processPopulation(pop);
        pop = _sim->processPopulation(pop);
        
#ifdef TARGET_HYPERGP
        mypop = dynamic_cast<genetic::hyperGP::Population*>(pop.get());
#elif TARGET_SYMREG
        mypop = dynamic_cast<genetic::symreg::Population*>(pop.get());
#endif
        
        if (_bloat) pop = _bloat->processPopulation(pop);
        pop = this->processPopulation(pop);
        
        if (pop->iteration() == this->_last_iter_number) {
            throw logic_error("seems like you forgot to increment the iteration number in your GP!");
        } else {
            _last_iter_number = pop->iteration();
        }
        
        //show estimated time
        double end_iter_millis = tm::currentMs() - begin_loop_millis;
        double end_total_millis = tm::currentMs() - begin_millis;
        
        double one_iter_millis = end_total_millis / mypop->iteration();

        cout << "------- TIME: last iter: " << tm::formattedTimeFromMs(end_iter_millis);
        cout << ", total elapsed: " << tm::formattedTimeFromMs(end_total_millis);
        cout << " s, estimated left: " << tm::formattedTimeFromMs((Settings::get()->_control.maximumIterations - mypop->iteration())*one_iter_millis) << endl;
        
//        cout << "Pop: " << mypop->description() << endl;
        
        if (pop->iteration() % 50 == 0) {
            cout << "Backing up population ... iteration " << pop->iteration() << endl;
            if (savePopToXML) {
                genetic::Population * endpop = dynamic_cast<genetic::Population*>(pop.get());
                sort(endpop->begin(), endpop->end(), Population::compareFitness);
                xml->savePopulationToXml(pop.get());
            }
        }
    }
    
    //show tree generator stats
    tree::TreeGenerator::get()->printStatistics();
        
    cout << "----- EXPERIMENT ENDED -----" << endl;
    
    // matlab was used, save the figure?
    if (Settings::get()->_control.useMatlabPlot) {
        io::MatlabHandler::get()->saveToFile();
    }
    
    //save the global settings
    if (saveSettingsToXML) {
        //save to the default location as the last used settings
        Settings::get()->saveSettings();
    }
    
    // save the population to xml?
    string populationPath = "";
    if (savePopToXML) {
        genetic::Population * endpop = dynamic_cast<genetic::Population*>(pop.get());
        sort(endpop->begin(), endpop->end(), Population::compareFitness);
        populationPath = xml->savePopulationToXml(pop.get());
        
        if (isAutonomousExperiment) {
            Settings::get()->_control.populationXmlLoadFrom = populationPath;
        }
    }
    
    // save experiment-specific settings to file
    if (saveSettingsToXML) {
        string copySetName = Settings::get()->outputFilePrefix()+"_settings.xml";
        
        //save experiment specific settings to data
        Settings::get()->saveSettings(copySetName);
    }
    
#ifdef TARGET_HYPERGP
    
    if (showBestIndividualAtTheEnd) {
        cout << "Showing the best individual" << endl;
        
        const genetic::hyperGP::Population* mypop2 = dynamic_cast<genetic::hyperGP::Population*>(pop.get());
        const genetic::hyperGP::Simulator* mysim2 = dynamic_cast<genetic::hyperGP::Simulator*>(_sim);
        genetic::hyperGP::Individual* myind2 = dynamic_cast<genetic::hyperGP::Individual*>(mypop2->bestIndividual());
        mysim2->simulateIndividual(myind2, !isAutonomousExperiment, true);
    }
    
#endif
    
    //experiment is over, write out results and return control
    cout << "Experiment ended." << endl;

    if (Settings::get()->_control.useMatlabPlot) {
        io::MatlabHandler::get()->destroy();
    }
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
bool genetic::ControlBlock::shouldStop(shared_ptr<cic::Population> _popIn)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    genetic::Population* pop = dynamic_cast<genetic::Population*>(_popIn.get());
    if (!pop) throw bad_cast();
    
    //check iterations and minimum fitness
    if (pop->iteration() >= this->_maximum_iterations) {
        return true;
    }
    
    double max_fit_pop = pop->statisticsValueForKey("max_fitness");
    double max_fit_poss = Settings::get()->_control.minimumFitness;
    
    if (!std::isnan(max_fit_pop) && max_fit_pop >= max_fit_poss) {
        return true;
    }
    
    return false;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
shared_ptr<cic::Population> genetic::ControlBlock::processPopulation(shared_ptr<cic::Population> _popIn) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    genetic::Population* pop = dynamic_cast<genetic::Population*>(_popIn.get());
    if (!pop) throw bad_cast();
    
    //do analytics
    
    //pull out fitnesses, sizes and depths
    double avg_fitness = 0;
    double avg_depth = 0;
    double avg_size = 0;
    double min_fitness = __DBL_MAX__;
    double min_depth = __DBL_MAX__;
    double min_size = __DBL_MAX__;
    double max_fitness = -__DBL_MAX__;
    double max_depth = -__DBL_MAX__;
    double max_size = -__DBL_MAX__;
    
    double fitness = -__DBL_MAX__;
    double depth = -__DBL_MAX__;
    double size = -__DBL_MAX__;
    
    const genetic::Genotype* gen = nullptr;
    for (genetic::Population::const_iterator it = pop->begin(); it != pop->end(); ++it) {
        fitness = (*it)->fitness();
        gen = dynamic_cast<const genetic::Genotype*>((*it)->genotype());
        depth = gen->depth();
        size = gen->size();
        
        //avg
        avg_fitness += fitness;
        avg_depth += depth;
        avg_size += size;
        
        //min
        min_fitness = min(min_fitness, fitness);
        min_depth = min(min_depth, depth);
        min_size = min(min_size, size);
        
        //max
        max_fitness = max(max_fitness, fitness);
        max_depth = max(max_depth, depth);
        max_size = max(max_size, size);
    }
    
    avg_fitness /= pop->size();
    avg_depth /= pop->size();
    avg_size /= pop->size();
    
    //write these statistics to the population
    pop->setStatisticsValueForKey("avg_fitness", avg_fitness);
    pop->setStatisticsValueForKey("avg_depth", avg_depth);
    //    pop->setStatisticsValueForKey("avg_size", avg_size);
    
    //    pop->setStatisticsValueForKey("min_fitness", min_fitness);
    //    pop->setStatisticsValueForKey("min_depth", min_depth);
    //    pop->setStatisticsValueForKey("min_size", min_size);
    
    pop->setStatisticsValueForKey("max_fitness", max_fitness);
    pop->setStatisticsValueForKey("max_depth", max_depth);
    //    pop->setStatisticsValueForKey("max_size", max_size);
    
    //print statistics
    cout << pop->statistics();
    
    //update the statistic in Matlab fig
    if (Settings::get()->_control.useMatlabPlot) {
        io::MatlabHandler::get()->updateStatistic(*pop->statistics_data(), pop->iteration());
    }
    
    //we have our correct population, proceed
    cout << "Iteration: " << pop->iteration() << " : Population " << pop << " just passed through Control Block!" << endl;
    
    return _popIn;
}

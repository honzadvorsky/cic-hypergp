/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_genetic_Population.cpp
//  hyperGP
//
//  Created by Honza Dvorsky on 2/2/13.
//

#include "cic_genetic_Population.h"
#include "cic_genetic_Individual.h"

#include <sstream>

using namespace cic;
using namespace std;

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
genetic::Population::size_type genetic::Population::find(std::shared_ptr<genetic::Individual> ind) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    Population::size_type sz = this->size();
    for (Population::size_type i = 0; i < sz; ++i) {
        if ((*_inds)[i].get() == ind.get()) return i;
    }
    return sz;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void genetic::Population::addIndividual(std::shared_ptr<genetic::Individual> ind)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    _inds->push_back(ind);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void genetic::Population::eraseIndividual(genetic::Population::iterator it)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    _inds->erase(it);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
genetic::Population::Population()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    _inds = new std::vector< std::shared_ptr<genetic::Individual> >();
    _statistics = new std::map<std::string, double>();
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
double genetic::Population::statisticsValueForKey(std::string key) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    if (_statistics->size() != 0) {
        return _statistics->at(key);
    }
    return std::numeric_limits<double>::quiet_NaN();
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void genetic::Population::setStatisticsValueForKey(std::string key, double value)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    _statistics->insert(pair<string, double>(key, value));
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
string genetic::Population::statistics() const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    stringstream ss;
    ss << "*** POPULATION ANALYTICS ***" << endl;
    
    for (map<string, double>::const_iterator it = _statistics->begin(); it != _statistics->end(); ++it) {
        ss << "[" << it->first << ":" << it->second << "]" << endl;
    }
    
    return ss.str();
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
bool genetic::Population::compareFitness(std::shared_ptr<genetic::Individual> i, std::shared_ptr<genetic::Individual> j)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    return (i->fitness() > j->fitness());
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
bool genetic::Population::compareDepth (std::shared_ptr<genetic::Individual> i, std::shared_ptr<genetic::Individual> j)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    return (i->genotype()->depth() < j->genotype()->depth());
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
bool genetic::Population::compareSize (std::shared_ptr<genetic::Individual> i, std::shared_ptr<genetic::Individual> j)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    return (i->genotype()->size() < j->genotype()->size());
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
cic::genetic::Individual* genetic::Population::bestIndividual() const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    sort(_inds->begin(), _inds->end(), genetic::Population::compareFitness);
    return (*_inds)[0].get();
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
string genetic::Population::description() const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    string s = "Population: \n";
    
    for (genetic::Population::const_iterator it = this->begin(); it != this->end(); ++it) {
        s += string((*it)->description() + "\n");
    }
    
    return s;
}









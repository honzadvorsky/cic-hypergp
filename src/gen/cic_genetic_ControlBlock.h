/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_genetic_ControlBlock.h
//  hyperGP
//
//  Created by Honza Dvorsky on 2/5/13.
//

#ifndef __hyperGP__cic_genetic_ControlBlock__
#define __hyperGP__cic_genetic_ControlBlock__

#include "cic_PopulationModifier.h"

#ifdef TARGET_SYMREG

#include "cic_genetic_symreg_Encoding.h"
#include "cic_genetic_symreg_Simulator.h"
#include "cic_genetic_symreg_Population.h"
#include "cic_genetic_symreg_BloatControl.h"
#include "cic_genetic_symreg_GP.h"

#endif

#ifdef TARGET_HYPERGP

#include "cic_genetic_hyperGP_HypercubicEncoding.h"
#include "cic_genetic_hyperGP_Simulator.h"
#include "cic_genetic_hyperGP_Population.h"
#include "cic_genetic_hyperGP_BloatControl.h"
#include "cic_genetic_hyperGP_GeneticProgramming.h"

#endif

namespace cic {
    namespace genetic {
        class ControlBlock;
    } //namespace genetic
} //namespace cic

/** Control block of genetic algorithms. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::genetic::ControlBlock : public cic::PopulationModifier
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
protected:

    /** Hold the GP block. */
    cic::genetic::GeneticModifier*          _gp;
    
    /** Holds the encoding block. */
    cic::genetic::Encoding*                 _encoding;
    
    /** Holds the simulator block. */
    cic::genetic::Simulator*                _sim;
    
    /** Holds the bloat control block. */
    cic::genetic::BloatControl*             _bloat;
    
    /** Saves the statistics of each population. */
    std::map< std::string, std::vector<double> > *_globalStatistics;

    /** Property: maximum number of iterations. */
    size_t      _maximum_iterations;
    
    /** Property: minimum fitness that would satisfy the stopping condition. */
    double      _minimum_fitness;
    
    /** Previous saved iteration number (for anti-cycling purposes). */
    size_t      _last_iter_number;
    
    /** Initializes the experiment. */
    void                                    initExperimentWithParams(int argc, const char * argv[]);
    
public:
    void                                    init();
    
    /** Loop termination condition (negated). */
    bool                                    shouldStop(std::shared_ptr<cic::Population>);
    
    /** Take a population in, work with it and send out again. */
    std::shared_ptr<cic::Population>        processPopulation(std::shared_ptr<cic::Population>) const;

    /** Call this function whenever you want to start the experiment. */
    virtual void                start(int argc, const char ** argv);
    
    ControlBlock();
    virtual ~ControlBlock() { delete _gp; delete _encoding; delete _sim; delete _bloat; std::cout << "Control Block deleted." << std::endl; }
};

#endif /* defined(__hyperGP__cic_genetic_ControlBlock__) */

/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_genetic_BloatControl.cpp
//  hyperGP
//
//  Created by Honza Dvorsky on 2/3/13.
//

#include "cic_genetic_BloatControl.h"
#include "memoryutils.h"
#include "cic_Settings.h"

#ifdef TARGET_SYMREG

#include "cic_genetic_symreg_Population.h"
#include "cic_genetic_symreg_GP.h"

#endif

#ifdef TARGET_HYPERGP

#include "cic_genetic_hyperGP_Population.h"
#include "cic_genetic_hyperGP_GeneticProgramming.h"

#endif

using namespace cic;
using namespace std;

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void genetic::BloatControl::init()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    Settings::get()->_bloat.currentLimit = Settings::get()->_gp.maximumGeneratedTreeDepth;
    
    cout << "Bloat Control inited." << endl;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
int genetic::BloatControl::dynamicLimitForIndividual(const genetic::Individual* ind) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    double dynLim = ind->runtimeAttributeValue("dynamic_limit");
    if (dynLim == 0 || isnan(dynLim)) {
        //use global
        return Settings::get()->_bloat.currentLimit;
    }
    return dynLim;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
pair<double, int> genetic::BloatControl::newLimitFromPopulation(const genetic::Population* pop) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //go through the population and find the depth/size of the individual with the best fitness
    
    int best_size = 0;
    double best_fit = -__DBL_MAX__;
    
    for (genetic::Population::const_iterator it = pop->begin(); it != pop->end(); ++it) {
        if ((*it)->fitness() > best_fit) {
            best_fit = (*it)->fitness();
            best_size = (*it)->genotype()->depth();
        }
    }
    return pair<double, int>(best_fit, best_size);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
std::shared_ptr<genetic::Individual> genetic::BloatControl::parentReplacement(std::shared_ptr<genetic::Individual> ind) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
//    return nullptr;
//#warning NOT REPLACING WITH PARENTS!
    
    const vector< shared_ptr<genetic::Individual> >* parents = ind->parents();
    shared_ptr<genetic::Individual> par = nullptr;
    
    if (parents->size() != 0) {
        //take the fittest parent
        par = *max_element(parents->begin(), parents->end(), Population::compareFitness);
    }
    
    return par;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
shared_ptr<Population> genetic::BloatControl::processPopulation(shared_ptr<cic::Population> _popIn) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    if (!Settings::get()->_bloat.useBloatControl) {
        cout << "Skipping Bloat Control block - disabled." << endl;
        return _popIn;
    }
    
    genetic::Population* pop = dynamic_cast<genetic::Population*>(_popIn.get());
    if (!pop) throw bad_cast();
    
    int orig_count = (int)pop->size();
    
    //---------------------------------------
    //-- BLOAT CONTROL TAKES PLACE HERE -----
    //---------------------------------------
    
    //copy pointers to all individuals to a temp population, because we will only move back in those
    //that pass the test
    
    unique_ptr<genetic::Population> tempPop;
    
#ifdef TARGET_SYMREG
    tempPop = make_unique<genetic::symreg::Population>();
#endif
    
#ifdef TARGET_HYPERGP
    tempPop = make_unique<genetic::hyperGP::Population>();
#endif
    
    genetic::Population::iterator tempIt = pop->begin();
    for (; tempIt != pop->end(); ++tempIt) {
        tempPop->addIndividual(*tempIt);
    }
    //empty the original population
    pop->clear();
    
    // DYNAMIC LIMITS
    
    //statistics
    int marked_illegal = 0;
    int replaced_with_parent = 0;
    int passed_ok = 0;
    int removed = 0;
    
    BloatControlVariant variant = (BloatControlVariant)Settings::get()->_bloat.bloatControlVariant;
    
    typedef vector< shared_ptr<genetic::Individual> > ind_vec;
    
    //go through all individuals in the population
    genetic::Population::iterator it = tempPop->begin();
    
    //assign old and new dynlimit
    pair<double, int> best = this->newLimitFromPopulation(tempPop.get());
    // <fitness, limit>
    
    cout << "Best fitness: " << best.first << " and depth: " << best.second << endl;
    
    int old_limit = Settings::get()->_bloat.currentLimit;
    int new_limit;
    
    //depends on the variant. if simple, it only increases. if heavy/veryHeavy, it can decrease
    if (variant == BloatControlVariant::Simple) {
        new_limit = max(old_limit, best.second);
    } else if (variant == BloatControlVariant::Heavy) {
        new_limit = max(Settings::get()->_gp.maximumGeneratedTreeDepth, best.second);
    } else if (variant == BloatControlVariant::VeryHeavy) {
        new_limit = best.second;
    } else {
        throw logic_error("unrecognized bloat control variant.");
    }
    
    Settings::get()->_bloat.currentLimit = new_limit;
    
    bool illegal_parents;
    bool dynlim_parents;
    
    while (it != tempPop->end()) {
        
        //for heavy and very heavy variants we need to take care of illegals
        if (variant != BloatControlVariant::Simple) {
            //find out if this individual has illegal parents
            
            illegal_parents = false;
            dynlim_parents = false;
            
            int bigger_parent = 0;
            int bigger_limit = 0;

            const ind_vec* parents = (*it)->parents();
            ind_vec::const_iterator it_par = parents->begin();
            
            for (; it_par != parents->end(); ++it_par) {
                double ill = (*it_par)->runtimeAttributeValue("illegal");
                double dynlim = (*it_par)->runtimeAttributeValue("dynamic_limit");
                
                if(!isnan(ill)) {
                    illegal_parents |= (bool)ill;
                    bigger_parent = max(bigger_parent, (*it_par)->genotype()->depth());
                } else if (!isnan(dynlim)) {
                    dynlim_parents |= (bool)dynlim;
                    bigger_limit = max(bigger_limit, (int)dynlim);
                }
            }
            
            //if it has illegal parents, change the limit for it
            if (illegal_parents) {
                //has illegal parents, take the limit of the bigger one
                (*it)->setRuntimeAttributeValueForKey(bigger_parent, "dynamic_limit");
            } else if (dynlim_parents) {
                //if it had parents with dynamic limit, change the limit for it
                (*it)->setRuntimeAttributeValueForKey(bigger_limit, "dynamic_limit");
            }
                        
        }
        
        //check whether size is ok or not
        
        int mydymlim = this->dynamicLimitForIndividual((*it).get());
        int mysz = (*it)->genotype()->depth();
        
        if (mysz <= mydymlim) {
            //under limit, OK
            //accept individual into the population
            
            pop->addIndividual(*it);
            tempPop->eraseIndividual(it);
            passed_ok++;
            
        } else {
            
            //only if we didn't just remove an individual (which would move us one right automatically)
            ++it;
            
        }
    }
    
    shared_ptr< genetic::Individual > par = nullptr;
    
    //so now we have all OK individuals back in pop, but still some are probably left in tempPop
    Population::iterator rem_it = tempPop->begin();
    while (rem_it != tempPop->end()) {
        
        //the variant will determine what we do with them
        if (variant == BloatControlVariant::Simple) {
            //replace the individual with its better parent, meaning put the parent into the new pop, erase current from tempPop
            
            par = this->parentReplacement(*rem_it);
            if (par) {
                pop->addIndividual(par);
                replaced_with_parent++;
            } else {
                removed++;
            }
            tempPop->eraseIndividual(rem_it);
            
        } else {
            
            //if heavy/veryheavy, we can mark those bigger than new limit, but lower than old limit as illegals
            int sz = (*rem_it)->genotype()->depth();
            if (sz > new_limit && sz <= old_limit) {
                //mark illegal, they were legal last iteration, but the limit decreased
                //but allow them into the population
                (*rem_it)->setRuntimeAttributeValueForKey(1, "illegal");
                
                pop->addIndividual(*rem_it);
                tempPop->eraseIndividual(rem_it);
                marked_illegal++;
                
            } else {
                //they're hopeless, just replace them with the best parent
                
                par = this->parentReplacement(*rem_it);
                if (par) {
                    pop->addIndividual(par);
                    replaced_with_parent++;
                } else {
                    removed++;
                }
                tempPop->eraseIndividual(rem_it);
                
            }
        }
    }
    
    //DEBUG
    
    //    for (Population::const_iterator it = pop->begin(); it != pop->end(); ++it) {
    //        if (new_limit < (*it)->genotype()->depth()) {
    //            cout << "limit is " << new_limit << " but i passed an individual with " << (*it)->genotype()->depth() << endl;
    //            throw logic_error("WTF?");
    //        }
    //    }
    
    //END DEBUG
    
    //make sure we cleaned up all individuals
    if (tempPop->size() > 0 || orig_count != marked_illegal+passed_ok+replaced_with_parent+removed) {
        throw logic_error("you didn't deal with all individuals!");
    }
    
    //---------------------------------------
    
    pop->setStatisticsValueForKey("dynamic_limit", new_limit);
    //    pop->setStatisticsValueForKey("replaced_with_parent", replaced_with_parent);
    //    pop->setStatisticsValueForKey("marked_illegal", marked_illegal);
    //    pop->setStatisticsValueForKey("passed_ok", passed_ok);
    
    //we have our correct population, proceed
    cout << "Bloat control: variant " << variant << ", marked illegal: " << marked_illegal;
    cout << ", replaced with parent: " << replaced_with_parent << ", removed: " << removed << " and " << passed_ok << " passed ok." << endl;
    cout << "Iteration: " << pop->iteration() << " : Population " << pop << " just passed through Bloat Control!" << endl;
    
    //cast back up
    return _popIn;
}


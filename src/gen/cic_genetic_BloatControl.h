/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_genetic_BloatControl.h
//  hyperGP
//
//  Created by Honza Dvorsky on 2/3/13.
//

#ifndef __hyperGP__cic_genetic_BloatControl__
#define __hyperGP__cic_genetic_BloatControl__

#include "cic_PopulationModifier.h"
#include "cic_genetic_Population.h"

namespace cic {
    namespace genetic {
        class BloatControl;
        
        enum BloatControlVariant {
            Simple = 0,
            Heavy = 1,
            VeryHeavy = 2
        };
    } //namespace genetic
} //namespace cic

/** Common bloat control block for genetic experiments. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::genetic::BloatControl : public cic::PopulationModifier
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    int dynamicLimitForIndividual(const genetic::Individual* ind) const;
    std::pair<double, int> newLimitFromPopulation(const genetic::Population* pop) const;
    std::shared_ptr<genetic::Individual> parentReplacement(std::shared_ptr<genetic::Individual> ind) const;
    
public:
    
    void        init();
    
    /** Processes population, does its thing. */
    std::shared_ptr<cic::Population> processPopulation(std::shared_ptr<cic::Population> popIn) const;
    
};

#endif /* defined(__hyperGP__cic_genetic_BloatControl__) */

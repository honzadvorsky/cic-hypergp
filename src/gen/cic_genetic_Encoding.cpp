/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_genetic_Encoding.cpp
//  hyperGP
//
//  Created by Honza Dvorsky on 2/3/13.
//

#include "cic_genetic_Encoding.h"

#ifdef USE_GCD
#include <dispatch/dispatch.h>
#endif

using namespace std;
using namespace cic;


//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
shared_ptr<Population> genetic::Encoding::processPopulation(shared_ptr<cic::Population> _popIn) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    genetic::Population* pop = dynamic_cast<genetic::Population*>(_popIn.get());
    if (!pop) throw bad_cast();
    
    //---------------------------------------
    //----- ENCODING TAKES PLACE HERE -------
    //---------------------------------------
    
    //assign/create a phenotype for the individual, IF IT DOESN'T HAVE ONE!
        
    __block int count = 0;
    
#ifdef USE_GCD
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_apply(pop->size(), queue, ^(size_t k)
                   {
                       genetic::Individual *ind = (pop->begin() + k)->get();
#else
                       genetic::Population::iterator it = pop->begin();
                       for (; it != pop->end(); ++it) {
                           genetic::Individual *ind = it->get();
#endif
                           //do the hard work
                           if (!ind->phenotype()) {
                               ind->setPhenotype(this->generatePhenotypeForGenotype(ind->genotype()));
                               count++;
                           }
                           
#ifdef USE_GCD                       
                       });
#else
                   }
#endif
   
    cout << "Encoding updated " << count << "/" << pop->size() << " individuals." << endl;
    
    //---------------------------------------
    
    //we have our correct population, proceed
    cout << "Iteration: " << pop->iteration() << " : Population " << pop << " just passed through Encoding!" << endl;
    
    //cast back up
    return _popIn;
}

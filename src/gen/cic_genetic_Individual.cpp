/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_genetic_Individual.cpp
//  hyperGP
//
//  Created by Honza Dvorsky on 2/2/13.
//

#include "cic_genetic_Individual.h"
#include "cic_tree_TreeGenerator.h"

using namespace std;
using namespace cic;

////--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
//double genetic::Genotype::valueForInputs(const vector<double>& inputs) const
////--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
//{
//    vector<double>* vec = new vector<double>(inputs);
//    _tree->setTermMappedValues(vec);
//    return _tree->rootValue();
//}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void genetic::Genotype::mutateGenotype()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    int maxDepth = Settings::get()->_gp.maximumGeneratedTreeDepth;
    int minDepth = Settings::get()->_gp.minimumGeneratedTreeDepth;
    
    unique_ptr< tree::Tree<tree_type> > newTr;
    tree::Tree<tree_type> * ourTr;
    
    //for each tree in the forest
    for (int i = 0; i < _forest->treeCount(); ++i) {
        
        //take out our tree
        ourTr = _forest->treeAtIndex(i);
        
        //generate a random tree
        newTr = tree::TreeGenerator::get()->genTree(tree::GenType::GROW, tree::GenParam(maxDepth, minDepth)); //max depth
        
        //get a random place on the tree
        tree::NodeInfo ni = tree::TreeGenerator::get()->randomNodeInfoForTree(ourTr);

        //attach the random generated tree to our tree
        ourTr->attachSubtree(ni, newTr.release());
        
    }
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void genetic::Genotype::crossoverWithGenotype(genetic::Genotype * otherGen)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    genetic::Genotype *otherSymGen = dynamic_cast<genetic::Genotype*>(otherGen);
    if (!otherGen) throw logic_error("incompatible genotype!");
    
    tree::Tree<tree_type> * ourTr;
    tree::Tree<tree_type> * otherTr;
    
    //for each tree in the forest
    for (int i = 0; i < _forest->treeCount(); ++i) {
        
        ourTr = _forest->treeAtIndex(i);
        otherTr = otherSymGen->_forest->treeAtIndex(i);
        
        //get a random place on both trees
        tree::NodeInfo niA = tree::TreeGenerator::get()->randomNodeInfoForTree(ourTr);
        tree::NodeInfo niB = tree::TreeGenerator::get()->randomNodeInfoForTree(otherTr);

        //swap subtrees
        ourTr->swapSubtrees(niA, otherTr, niB);
    }
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void genetic::Individual::setPhenotype(unique_ptr<genetic::Phenotype> phen)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    if (_phenotype) throw logic_error("phenotype is already generated! cannot change during lifetime!");
    _phenotype = phen.release();
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
double genetic::Individual::runtimeAttributeValue(string key) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    if (_runtimeAttributes->count(key) != 0) {
        return _runtimeAttributes->at(key);
    }
    return std::numeric_limits<double>::quiet_NaN();
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void genetic::Individual::setRuntimeAttributeValueForKey(double value, string key)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    _runtimeAttributes->insert(pair<string, double>(key, value));
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void genetic::Individual::addParent(std::shared_ptr<Individual> par)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    _parents->push_back(par);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void genetic::Individual::removeParents()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    _parents->clear();
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
std::vector< std::shared_ptr<genetic::Individual> >* genetic::Individual::parents() const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    return _parents;
}





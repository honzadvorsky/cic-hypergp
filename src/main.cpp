//
//  main.cpp
//  hyperGP
//
//  Created by Honza Dvorsky on 11/29/12.
//  Copyright (c) 2012 Honza Dvorsky. All rights reserved.
//

#include <iostream>
#include <string>
#include <unistd.h>
#include <time.h>
#include <ctime>
#include <vector>
#include <map>

#include "memoryutils.h"
#include "cic_auto_test.h"
#include "cic_io_xmlHandler.h"
#include "cic_Settings.h"
#include "cic_io_matlab_handler.h"
#include "cic_tree_forest.h"

#ifdef TARGET_SYMREG

#include "cic_genetic_symreg_ControlBlock.h"

#endif

#ifdef TARGET_HYPERGP

#include "cic_genetic_hyperGP_Individual.h"

#ifdef WITH_SIM
#include "hyperGP_sim2.h"
#endif

#ifdef USE_GCD
#include <dispatch/dispatch.h>
#endif

#include "cic_genetic_hyperGP_ControlBlock.h"

#include "cic_nn_Network.h"
//#include "cic_nn_Neuron.h"

#endif



typedef double tree_type;

using namespace std;

//----------------------------------------------------------------------------
int main(int argc, const char * argv[])
//----------------------------------------------------------------------------
{
    const clock_t begin_time = clock();
    
#if defined(DEBUG) && (defined(TARGET_HYPERGP) || defined(TARGET_SYMREG)) 
    cic::auto_test<tree_type>();
#endif
    //----------------------------------------------------------------------------
    
    
#if defined(TARGET_HYPERGP)
    
    try {
        if (true) {
            std::cout << "Running experiment hyperGP of the hyperGP project." << std::endl;
            
            //create the symreg control block
            std::unique_ptr<cic::genetic::ControlBlock> control = std::make_unique<cic::genetic::hyperGP::ControlBlock>();
            
            //start the run
            control->start(argc, argv);
            
            cout << "hyperGP experiment over." << endl;
            
        } else {
            //test place
            
//            using namespace cic;
//            
//            typedef tree::TermNode<tree_type> ter;
//            typedef tree::ConstTermNode<tree_type> con;
//            typedef tree::FunNode<tree_type> fun;
//            
//            ter* t1 = new ter(0);
//            ter* t2 = new ter(2);
//            fun* f1 = new fun(tree::Sum, t1, t2);
//            ter* t3 = new ter(1);
//            fun*f2 = new fun(tree::Diff, f1, t3);
//            fun* root = f2;
//            std::unique_ptr< cic::tree::Tree<tree_type> > myAutoTree = std::make_unique< cic::tree::Tree<tree_type> >(root);
//            
//            ter* t11 = new ter(1);
//            ter* t21 = new ter(3);
//            fun* f11 = new fun(tree::Diff, t11, t21);
//            fun* root1 = f11;
//            std::unique_ptr< cic::tree::Tree<tree_type> > myAutoTree1 = std::make_unique< cic::tree::Tree<tree_type> >(root1);
//            
//            cic::tree::Forest<tree_type> *forest = new cic::tree::Forest<tree_type>(2);
//            forest->setTreeAtIndex(myAutoTree.release(), 0);
//            forest->setTreeAtIndex(myAutoTree1.release(), 1);
//            
//            std::unique_ptr< cic::tree::Forest<tree_type> > for2 = forest->clone();
//            
//            cout << "For1: " << endl << forest->description() << endl;
//            cout << "For2: " << endl << for2->description() << endl;
//            
//            std::vector<int> depths = for2->treeDepths();
//            std::vector<int> sizes = for2->treeNodeCounts();
//            
//            int depth = for2->totalDepth();
//            int size = for2->totalSize();
//            
////            cout << "For2 depths: " << endl << for2->treeDepths() << endl;
        }
        
    } catch (std::logic_error l) {
        cout << "*****" << endl << l.what() << endl;
    } catch (std::exception e) {
        cout << "*****" << endl << e.what() << endl;
    }
    
#elif defined(TARGET_SYMREG)
    
    try {
        if (true) {
            std::cout << "Running experiment symreg of the hyperGP project." << std::endl;
            
            //create the symreg control block
            std::unique_ptr<cic::genetic::ControlBlock> control = std::make_unique<cic::genetic::symreg::ControlBlock>();
            
            //start the run
            control->start(argc, argv);
            
            cout << "symreg experiment over." << endl;
        } else {
            //test place
            
            
        }
        
    } catch (std::logic_error l) {
        cout << "*****" << endl << l.what() << endl;
    } catch (std::exception e) {
        cout << "*****" << endl << e.what() << endl;
    }

#else
    std::cout << "target not specified!" << std::endl;
#endif
    
    //----------------------------------------------------------------------------
//    sleep(20);
    std::cout << "Timer: " << float( clock () - begin_time ) /  CLOCKS_PER_SEC * 1000 << " millis." << std::endl;

    return 0;
}


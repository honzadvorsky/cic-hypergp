/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_Settings.cpp
//  hyperGP
//
//  Created by Honza Dvorsky on 3/3/13.
//

#include "cic_Settings.h"
#include "cic_tree_nodeFunx.h"
#include "cic_stringutils.h"
#include "cic_io_xmlHandler.h"

using namespace cic;
using namespace std;

Settings* Settings::_setting_inst;

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
Settings::Settings()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //before loading from XML
    _filePrefix = nullptr;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void Settings::resetVectors()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //happens after loading from XML
    if (_gp.terminalProbabilities.size() == 0 || (_gp.terminalVariableCount+_gp.terminalConstsCount != _gp.terminalProbabilities.size())) {
        _gp.terminalProbabilities = vector<double>(_gp.terminalVariableCount+_gp.terminalConstsCount, 1);
    }
    if (_gp.functionProbabilities.size() == 0 || _gp.functionCount != _gp.functionProbabilities.size()) {
        _gp.functionProbabilities = vector<double>(_gp.functionCount, 1);
    }
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void Settings::init()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    this->resetVectors();
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void cic::Settings::saveMe(string filename)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    io::xmlHandler han;
    han.saveSettingsToXml(filename);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
bool cic::Settings::loadMe(string filename)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    io::xmlHandler han;
    return han.loadSettingsFromXml(filename);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
string cic::Settings::outputFilePrefix()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    if (!_filePrefix) {
        
#ifdef TARGET_HYPERGP
        _filePrefix = new std::string("data_hyperGP/hypergp_" + cic::currentDateTime() + "-" + cic::randomString(4));
#endif
        
#ifdef TARGET_SYMREG
        _filePrefix = new std::string("data_symreg/symreg_" + cic::currentDateTime() + "-" + cic::randomString(4));
#endif
        
    }
    return *_filePrefix;
}







/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_tree_node.cpp
//  hyperGP
//
//  Created by Honza Dvorsky on 2/9/13.
//

#include "cic_tree_node.h"
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void cic::tree::initAttMap(Att_Map& atts)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    for (int i = 0; i < cic::tree::Attribute::_LAST_ITEM_ATT; ++i) {
        atts.insert(std::pair<cic::tree::Attribute, cic::tree::att_t>((cic::tree::Attribute)i, -1));
    }
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
const std::string cic::tree::attString(cic::tree::Attribute attType)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    switch (attType) {
        case DEPTH:
            return "DEP";
            break;
        case DEPTH_UNDER:
            return "DEP_U";
            break;
        case ID:
            return "ID";
            break;
        case NODES_UNDER:
            return "NOD_U";
            break;
        default:
            throw std::invalid_argument("unrecognized attribute type!");
            break;
    }
}

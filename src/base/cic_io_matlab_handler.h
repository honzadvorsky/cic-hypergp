/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_io_matlab_handler.h
//  hyperGP
//
//  Created by Honza Dvorsky on 3/4/13.
//

#ifndef __hyperGP__cic_io_matlab_handler__
#define __hyperGP__cic_io_matlab_handler__

#include <iostream>
#include <map>
#include <string>
#include <vector>

namespace cic {
    namespace io {
        class MatlabHandler;
        static MatlabHandler* _mat_inst;
    }
}

/** Matlab integration handler. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::io::MatlabHandler
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    void* _matlab_engine = NULL;
    
    std::map< std::string, std::vector<double>* >*  _allStats;
    std::vector<std::string>*                       _allKeys;
    
    void updateFigure(bool saveToFile);
    
    void test();
    
    void init();
    
    MatlabHandler() { this->init(); };
    virtual ~MatlabHandler() { this->destroy(); };
    
public:
    
    /** Forces the handler to get deleted. */
    void destroy();

    
    /** Returns the singleton instance. */
    static MatlabHandler* get() { if (!_mat_inst) _mat_inst = new MatlabHandler(); return _mat_inst; };
    
    /** Updates statistics with the new population input. */
    void updateStatistic(std::map<std::string, double> statForPop, size_t it_number);
    
    /** Saves figure and data to file. */
    void saveToFile() { this->updateFigure(true); };
    
    /** Shows neural network in a plot. */
    void showNeuralNetwork(void* network) const;
    
    /** Show neural network outputs. */
    void showNeuralOutputs(void* outputs, std::string appendix = "") const;

};






#endif /* defined(__hyperGP__cic_io_matlab_handler__) */

/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_io_xmlHandler.cpp
//  hyperGP
//
//  Created by Honza Dvorsky on 3/1/13.
//

#include "cic_io_xmlHandler.h"
#include "cic_stringutils.h"
#include "cic_tree.h"
#include <memory>
#include <sys/stat.h>
#include <vector>

#ifdef TARGET_SYMREG
#include "cic_genetic_symreg_Population.h"
#include "cic_genetic_symreg_Individual.h"
#endif

#ifdef TARGET_HYPERGP
#include "cic_genetic_hyperGP_Population.h"
#include "cic_genetic_hyperGP_Individual.h"
#endif

using namespace cic;
using namespace std;


#pragma mark - xmlaccess

/** Class which is granted private access to serialized types. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class io::xmlAccess
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
public:
    /** Our used tree type. */
    typedef double tree_type;
    
    /** Serialize BaseTree of tree_type. */
    xmlNodePtr serializeTree(const tree::BaseTree<tree_type>* tree) const;
    
    /** Parse Tree from xml. */
    tree::Tree<tree_type>* parseTree(xmlNodePtr tree_xml) const;
    
    /** Serialize Forest of tree_type. */
    xmlNodePtr serializeForest(const tree::Forest<tree_type>* forest) const;
    
    /** Parse Forest from xml. */
    tree::Forest<tree_type>* parseForest(xmlNodePtr forest_xml) const;
    
    /** Serialize ExpressionNode of tree_type. */
    xmlNodePtr serializeTreeNode(const tree::ExpressionNode<tree_type>* node) const;
    
    /** Parse ExpressionNode. */
    tree::ExpressionNode<tree_type>* parseTreeNode(xmlNodePtr node_xml) const;
    
    /** Serializes settings to xml. */
    xmlNodePtr serializeSettings(Settings* set = Settings::get()) const;
    
    /** Parses Settings from xml file. */
    Settings* parseSettings(xmlNodePtr set_xml) const;
    
    /** Serializes a double vector. */
    xmlNodePtr serializeDoubleVector(vector<double> vec) const;
    
    /** Parses a double vector. */
    vector<double> parseDoubleVector(xmlNodePtr vec_xml) const;
    
    /** Serialize a symreg population. */
    xmlNodePtr serializePopulation(const genetic::Population* pop) const;

    /** Parse a genetic individual. */
    genetic::Individual* parseIndividual(xmlNodePtr ind_xml) const;
    
    /** Parse a genetic population. */
    shared_ptr<genetic::Population> parsePopulation(xmlNodePtr pop_xml) const;
    
    /** Serialize a symreg Individual. */
    xmlNodePtr serializeIndividual(const genetic::Individual* ind) const;
    
};

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
xmlNodePtr io::xmlAccess::serializeForest(const tree::Forest<tree_type>* forest) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //add something to the tree
    xmlNodePtr for_node = xmlNewNode(NULL, BAD_CAST "forest");
    
    //record the number of trees
    xmlNewProp(for_node, BAD_CAST "tree_count", BAD_CAST cic::to_string("%i", forest->treeCount()).c_str());
    
    for (size_t i = 0; i < forest->treeCount(); ++i) {
        //add each child tree
        xmlAddChild(for_node, this->serializeTree(forest->treeAtIndex(i)));
    }
    
    return for_node;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
tree::Forest<io::xmlAccess::tree_type>* io::xmlAccess::parseForest(xmlNodePtr forest_xml) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    xmlChar *cnt = xmlGetProp(forest_xml, BAD_CAST "tree_count");
    
    int treeCount = cic::convertToInt((char*)(cnt));
    int treesLoaded = 0;
    assert(treeCount > 0);
    
    tree::Forest<tree_type>* forest = new tree::Forest<tree_type>(treeCount);
   
    forest_xml = forest_xml->xmlChildrenNode;
    while (forest_xml != NULL) {
        
        if (!xmlStrcmp(forest_xml->name, (const xmlChar *)"tree")) {
            //ok, we have a tree
            forest->setTreeAtIndex(this->parseTree(forest_xml), treesLoaded++);
        }
        
        forest_xml = forest_xml->next;
    }
    
    
    if (treeCount != treesLoaded) {
        cout << "Forest cannot be instantiated, xml was compromised, some trees are missing." << endl;
        xmlFree(cnt);
        return nullptr;
    }
    
    return forest;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
xmlNodePtr io::xmlAccess::serializeTree(const tree::BaseTree<tree_type>* tree) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //add something to the tree
    xmlNodePtr tr = xmlNewNode(NULL, BAD_CAST "tree");
    
    //find out the type of the tree
    const tree::Tree<tree_type>* regular_tree = dynamic_cast<const tree::Tree<tree_type>*>(tree);
    const tree::ConstTree<tree_type>* readonly_tree = dynamic_cast<const tree::ConstTree<tree_type>*>(tree);
    
    if (regular_tree) {
        //save the node type
        xmlNewProp(tr, BAD_CAST "tree_type", BAD_CAST "Tree");
    } else if (readonly_tree) {
        //save the node type
        xmlNewProp(tr, BAD_CAST "tree_type", BAD_CAST "ConstTree");
    } else {
        throw logic_error("unrecognized type of tree!");
    }
    
    //add the root
    xmlAddChild(tr, this->serializeTreeNode(tree->_root));
    
    return tr;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
tree::Tree<io::xmlAccess::tree_type>* io::xmlAccess::parseTree(xmlNodePtr tree_xml) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    xmlChar *type = xmlGetProp(tree_xml, BAD_CAST "tree_type");
    
    if (!xmlStrcmp(type, (const xmlChar *)"Tree")) {
        //free the property
        xmlFree(type);
        
        //instantiate a tree
        tree_xml = tree_xml->xmlChildrenNode; //should be the root, still iterate through all to be sure
        while (tree_xml != NULL) {
            if (!xmlStrcmp(tree_xml->name, (const xmlChar *)"node")) {
                //ok, we have the root
                return new tree::Tree<tree_type>(this->parseTreeNode(tree_xml));
            }
            tree_xml = tree_xml->next;
        }
        throw logic_error("the root node could not be found.");
        return nullptr;
        
    } else {
        cout << "ConstTree cannot be instantiated, it's just a read-only tree. Sorry." << endl;
        xmlFree(type);
        return nullptr;
    }
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
xmlNodePtr io::xmlAccess::serializeTreeNode(const tree::ExpressionNode<tree_type>* node) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //first, find out what kind of node it is and serialize according to that type
    //with TermNode and ConstTermNode, we only save the value.
    //with FunNode, we save the function type and the children nodes
    
    const tree::FunNode<tree_type>*         fun_node = dynamic_cast<const tree::FunNode<tree_type>* >(node);
    const tree::TermNode<tree_type>*        term_node = dynamic_cast<const tree::TermNode<tree_type>* >(node);
    const tree::ConstTermNode<tree_type>*   const_node = dynamic_cast<const tree::ConstTermNode<tree_type>* >(node);
    
    xmlNodePtr ser_node = xmlNewNode(NULL, BAD_CAST "node");
    xmlNodePtr child = nullptr;
    
    if (fun_node) {
        //we have a FunNode, serialize the function and the children
        
        //save the node type
        xmlNewProp(ser_node, BAD_CAST "node_type", BAD_CAST "Fun");
        
        //save the function type
        string type = cic::tree::funString(fun_node->_funType);
        xmlNewChild(ser_node, NULL, BAD_CAST "funType", BAD_CAST type.c_str());
        
        //save the left child
        child = xmlAddChild(ser_node, this->serializeTreeNode(fun_node->_left));
        xmlNewProp(child, BAD_CAST "child_pos", BAD_CAST "left");
        
        //maybe save the right child
        if (tree::funArity(fun_node->_funType) > 1) {
            child = xmlAddChild(ser_node, this->serializeTreeNode(fun_node->_right));
            xmlNewProp(child, BAD_CAST "child_pos", BAD_CAST "right");
        }
        
    } else if (const_node) {
        //we have a ConstTermNode, just serialize its value
        
        //save the node type
        xmlNewProp(ser_node, BAD_CAST "node_type", BAD_CAST "ConstTerm");
        
        //save the value
        xmlNewChild(ser_node, NULL, BAD_CAST "value", BAD_CAST cic::to_string("%f", const_node->_value).c_str());
        
    } else if (term_node) {
        //we have a TermNode, just serialize its value
        
        //save the node type
        xmlNewProp(ser_node, BAD_CAST "node_type", BAD_CAST "Term");
        
        //save the value
        xmlNewChild(ser_node, NULL, BAD_CAST "value", BAD_CAST cic::to_string("%f", term_node->_value).c_str());
        
    } else {
        throw logic_error("unrecognized type of node!");
    }
    
    //return the node
    return ser_node;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
tree::ExpressionNode<io::xmlAccess::tree_type>* io::xmlAccess::parseTreeNode(xmlNodePtr node_xml) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    xmlChar *type = xmlGetProp(node_xml, BAD_CAST "node_type");
    
    if (!xmlStrcmp(type, (const xmlChar *)"ConstTerm")) {
        //free the property
        xmlFree(type);
        
        //instantiate a consttermnode
        node_xml = node_xml->xmlChildrenNode;
        while (node_xml != NULL) {
            if (!xmlStrcmp(node_xml->name, (const xmlChar *)"value")) {
                //ok, we have the value
                const char* s = (const char *)node_xml->children->content;
                if (!s) throw logic_error("ConstTerm value not present!");
                tree_type val = cic::convertToDouble(s);
                return new tree::ConstTermNode<tree_type>(val);
            }
            node_xml = node_xml->next;
        }
        throw logic_error("value was not found");
        return nullptr;
        
    } else if (!xmlStrcmp(type, (const xmlChar *)"Term")) {
        //free the property
        xmlFree(type);
        
        //instantiate a termnode
        node_xml = node_xml->xmlChildrenNode;
        while (node_xml != NULL) {
            if (!xmlStrcmp(node_xml->name, (const xmlChar *)"value")) {
                //ok, we have the value
                const char* s = (const char *)node_xml->children->content;
                if (!s) throw logic_error("Term value not present!");
                tree_type val = cic::convertToDouble(s);
                return new tree::TermNode<tree_type>(val);
            }
            node_xml = node_xml->next;
        }
        throw logic_error("value was not found");
        return nullptr;
        
    } else if (!xmlStrcmp(type, (const xmlChar *)"Fun")) {
        //free the property
        xmlFree(type);
        
        //prepare variables
        tree::ExpressionNode<tree_type>* left_child = nullptr;
        tree::ExpressionNode<tree_type>* right_child = nullptr;
        tree::FunType funType = tree::FunType::_LAST_ITEM_TYPE;
        
        //read info about funnode first
        node_xml = node_xml->xmlChildrenNode;
        while (node_xml != NULL) {
            if (!xmlStrcmp(node_xml->name, (const xmlChar *)"funType")) {
                //ok, we have the value
                const char* ch = (const char*)node_xml->children->content;
                if (!ch) throw logic_error("funType content not present!");
                funType = tree::funTypeFromString(ch);
            } else if (!xmlStrcmp(node_xml->name, (const xmlChar *)"node")) {
                //see if it's the left or right child
                xmlChar *child_side = xmlGetProp(node_xml, BAD_CAST "child_pos");
                if (!xmlStrcmp(child_side, (const xmlChar *)"left")) {
                    //left child
                    left_child = this->parseTreeNode(node_xml);
                } else if (!xmlStrcmp(child_side, (const xmlChar *)"right")) {
                    //right child
                    right_child = this->parseTreeNode(node_xml);
                } else {
                    throw logic_error("unrecognized child side!");
                }
                
                xmlFree(child_side);
            }
            node_xml = node_xml->next;
        }
        
        //check that the function arity and the number of found children is correct
        int arity = tree::funArity(funType);
        switch (arity) {
            case 1:
                if (!left_child || right_child) throw logic_error("function arity doesn't fit the number of children!");
                break;
            case 2:
                if (!left_child || !right_child) throw logic_error("function arity doesn't fit the number of children!");
                break;
            default:
                throw logic_error("arity error!");
                break;
        }
        
        //instantiate the funnode and return
        return new tree::FunNode<tree_type>(funType, left_child, right_child);
        
    } else {
        cout << "This node cannot be parsed." << endl;
        //free the property
        xmlFree(type);
        return nullptr;
    }
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
genetic::Individual* io::xmlAccess::parseIndividual(xmlNodePtr ind_xml) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //create individual based on the genotype and set its fitness, if applicable
    
    genetic::Fitness fit = Settings::get()->_gp.defaultFitness;
    genetic::Genotype *gen = nullptr;
    xmlNodePtr g_xml = nullptr;
    
    ind_xml = ind_xml->xmlChildrenNode;
    while (ind_xml != NULL) {
        if (!xmlStrcmp(ind_xml->name, (const xmlChar *)"fitness")) {
            //ok, we have the fitness
            const char* s = (const char *)ind_xml->children->content;
            if (!s) throw logic_error("fitness tag is empty!");
            fit = cic::convertToDouble(s);
        } else if (!xmlStrcmp(ind_xml->name, (const xmlChar *)"genotype")) {
            //we have the genotype
            g_xml = ind_xml->xmlChildrenNode;
            while (g_xml != NULL) {
                if (!xmlStrcmp(g_xml->name, (const xmlChar *)"forest")) {
                    
#ifdef TARGET_SYMREG
                    gen = new genetic::symreg::Genotype(this->parseForest(g_xml));
#endif
                    
#ifdef TARGET_HYPERGP
                    gen = new genetic::hyperGP::Genotype(this->parseForest(g_xml));
#endif
                    
                }
                g_xml = g_xml->next;
            }
        }
        ind_xml = ind_xml->next;
    }
    
    //make sure we have the genotype
    if (!gen) throw logic_error("we can't have an individual without a genotype!");
    
    //create an individual and return it
    genetic::Individual* ind;
    
#ifdef TARGET_SYMREG
    ind = new genetic::symreg::Individual(gen);
#endif
    
#ifdef TARGET_HYPERGP
    ind = new genetic::hyperGP::Individual(gen);
#endif
    
    if (fit != Settings::get()->_gp.defaultFitness) {
        ind->setFitness(fit);
    }
    return ind;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
xmlNodePtr io::xmlAccess::serializeIndividual(const genetic::Individual* ind) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //just save the genotype and fitness, phenotype can be easily generated from the genotype
    xmlNodePtr ser_node = xmlNewNode(NULL, BAD_CAST "individual");
    xmlNodePtr child = nullptr;
    
    if (ind->fitness() != Settings::get()->_gp.defaultFitness) {
        xmlNewChild(ser_node, NULL, BAD_CAST "fitness", BAD_CAST cic::to_string("%f", ind->fitness()).c_str());
    }
    
    if (ind->genotype()) {
        const genetic::Genotype* gen;
        
#ifdef TARGET_SYMREG
        gen = dynamic_cast<const genetic::symreg::Genotype*>(ind->genotype());
#endif
        
#ifdef TARGET_HYPERGP
        gen = dynamic_cast<const genetic::hyperGP::Genotype*>(ind->genotype());
#endif
                
        if (gen->_forest) {
            xmlNewChild(ser_node, NULL, BAD_CAST "human_readable_gen", BAD_CAST gen->humanReadable().c_str());
            child = xmlNewChild(ser_node, NULL, BAD_CAST "genotype", BAD_CAST "");
            xmlAddChild(child, this->serializeForest(gen->_forest));
        }
    }
    
    return ser_node;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
xmlNodePtr io::xmlAccess::serializePopulation(const genetic::Population* pop) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //save the population count and all the individuals
    xmlNodePtr ser_node = xmlNewNode(NULL, BAD_CAST "population");
    
    //save the iteration number of the population
    xmlNewProp(ser_node, BAD_CAST "iteration", BAD_CAST cic::to_string("%i", pop->iteration()).c_str());
    
    //save the pop size
    xmlNewProp(ser_node, BAD_CAST "size", BAD_CAST cic::to_string("%i", pop->size()).c_str());
    
    //save the statistics, so that we can see the quality of the population right away
    map<string, double>::const_iterator it_stat = pop->_statistics->begin();
    for (; it_stat != pop->_statistics->end(); ++it_stat) {
        xmlNewProp(ser_node, BAD_CAST it_stat->first.c_str(), BAD_CAST cic::to_string("%.3f", it_stat->second).c_str());
    }
    
    //save all the individuals
    genetic::Population::const_iterator ind = pop->begin();
    for (; ind != pop->end(); ++ind) {
        xmlAddChild(ser_node, this->serializeIndividual(dynamic_cast<genetic::Individual*>((*ind).get())));
    }
    
    return ser_node;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
shared_ptr<genetic::Population> io::xmlAccess::parsePopulation(xmlNodePtr pop_xml) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //create a population from xml
    shared_ptr<genetic::Population> pop;
    
#ifdef TARGET_SYMREG
    pop = make_shared<genetic::symreg::Population>();
#endif
    
#ifdef TARGET_HYPERGP
    pop = make_shared<genetic::hyperGP::Population>();
#endif
    
    shared_ptr<genetic::Individual> ind = nullptr;
    
    //get size and check that we loaded all individuals
    xmlChar *size = xmlGetProp(pop_xml, BAD_CAST "size");
    
    //just fill the population with the individual (first put them in shared_ptrs)
    pop_xml = pop_xml->xmlChildrenNode;
    while (pop_xml != NULL) {
        if (!xmlStrcmp(pop_xml->name, (const xmlChar *)"individual")) {
            //ok, we have the individual
            ind = shared_ptr<genetic::Individual>(this->parseIndividual(pop_xml));
            pop->addIndividual(ind);
        }
        pop_xml = pop_xml->next;
    }
    
    const char* s = (const char *)size;
    if (!s) throw logic_error("size not present!");
    int sz = cic::convertToInt(s);
    
    xmlFree(size);
    
    if (sz == pop->size()) {
        cout << "All individuals loaded correctly." << endl;
    } else {
        cout << "Population load failed." << endl;
    }
    
    return pop;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
xmlNodePtr io::xmlAccess::serializeSettings(Settings* set) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //save the population count and all the individuals
    xmlNodePtr set_node = xmlNewNode(NULL, BAD_CAST "settings");
    xmlNodePtr gp = nullptr;
    xmlNodePtr sim = nullptr;
    xmlNodePtr control = nullptr;
    xmlNodePtr bloat = nullptr;
    xmlNodePtr neural = nullptr;
    xmlNodePtr child = nullptr;
    
    const xmlChar* expName;
    
#ifdef TARGET_SYMREG
    expName = BAD_CAST "symreg";
#endif
#ifdef TARGET_HYPERGP
    expName = BAD_CAST "hyperGP";
#endif
    
    xmlNewProp(set_node, BAD_CAST "experiment", expName);
    
    //serialize gp settings
    gp = xmlNewChild(set_node, NULL, BAD_CAST "GP", BAD_CAST "");
    
    xmlNewChild(gp, NULL, BAD_CAST "defaultFitness", BAD_CAST cic::to_string("%f", set->_gp.defaultFitness).c_str());
    xmlNewChild(gp, NULL, BAD_CAST "generatedPopulationSize", BAD_CAST cic::to_string("%i", set->_gp.generatedPopulationSize).c_str());
    xmlNewChild(gp, NULL, BAD_CAST "expectedGeneratedTreeSize", BAD_CAST cic::to_string("%i", set->_gp.expectedGeneratedTreeSize).c_str());
    xmlNewChild(gp, NULL, BAD_CAST "maximumGeneratedTreeDepth", BAD_CAST cic::to_string("%i", set->_gp.maximumGeneratedTreeDepth).c_str());
    xmlNewChild(gp, NULL, BAD_CAST "minimumGeneratedTreeDepth", BAD_CAST cic::to_string("%i", set->_gp.minimumGeneratedTreeDepth).c_str());
    xmlNewChild(gp, NULL, BAD_CAST "terminalVariableCount", BAD_CAST cic::to_string("%i", set->_gp.terminalVariableCount).c_str());
    xmlNewChild(gp, NULL, BAD_CAST "functionCount", BAD_CAST cic::to_string("%i", set->_gp.functionCount).c_str());
    
    child = xmlNewChild(gp, NULL, BAD_CAST "functionProbabilities", BAD_CAST "");
    xmlAddChild(child, this->serializeDoubleVector(set->_gp.functionProbabilities));
    
    child = xmlNewChild(gp, NULL, BAD_CAST "terminalProbabilities", BAD_CAST "");
    xmlAddChild(child, this->serializeDoubleVector(set->_gp.terminalProbabilities));
    
    xmlNewChild(gp, NULL, BAD_CAST "tournament_size", BAD_CAST cic::to_string("%i", set->_gp.tournament_size).c_str());
    xmlNewChild(gp, NULL, BAD_CAST "selection_type", BAD_CAST cic::to_string("%i", set->_gp.selection_type).c_str());
    xmlNewChild(gp, NULL, BAD_CAST "terminalConstsCount", BAD_CAST cic::to_string("%i", set->_gp.terminalConstsCount).c_str());
    xmlNewChild(gp, NULL, BAD_CAST "tournamentSelectionUseLexicographicParsimonyPressure", BAD_CAST cic::to_string("%i", set->_gp.tournamentSelectionUseLexicographicParsimonyPressure).c_str());
    xmlNewChild(gp, NULL, BAD_CAST "numberOfTreesInForestGenotype", BAD_CAST cic::to_string("%i", set->_gp.numberOfTreesInForestGenotype).c_str());
    xmlNewChild(gp, NULL, BAD_CAST "preventDuplicates", BAD_CAST cic::to_string("%i", set->_gp.preventDuplicates).c_str());
    
    //serialize simulator settings
    sim = xmlNewChild(set_node, NULL, BAD_CAST "Simulator", BAD_CAST "");
    
    xmlNewChild(sim, NULL, BAD_CAST "sample_from", BAD_CAST cic::to_string("%f", set->_simulator.sample_from).c_str());
    xmlNewChild(sim, NULL, BAD_CAST "sample_step", BAD_CAST cic::to_string("%f", set->_simulator.sample_step).c_str());
    xmlNewChild(sim, NULL, BAD_CAST "sample_to", BAD_CAST cic::to_string("%f", set->_simulator.sample_to).c_str());
    xmlNewChild(sim, NULL, BAD_CAST "delayed_start", BAD_CAST cic::to_string("%f", set->_simulator.delayed_start).c_str());
    xmlNewChild(sim, NULL, BAD_CAST "robot_type", BAD_CAST cic::to_string("%i", set->_simulator.robot_type).c_str());
    xmlNewChild(sim, NULL, BAD_CAST "extraTimeStepDivider", BAD_CAST cic::to_string("%i", set->_simulator.extraTimeStepDivider).c_str());
    xmlNewChild(sim, NULL, BAD_CAST "generatePovrayFiles", BAD_CAST cic::to_string("%i", set->_simulator.generatePovrayFiles).c_str());    
    
    //serialize control settings
    control = xmlNewChild(set_node, NULL, BAD_CAST "Control", BAD_CAST "");
    
    xmlNewChild(control, NULL, BAD_CAST "maximumIterations", BAD_CAST cic::to_string("%i", set->_control.maximumIterations).c_str());
    xmlNewChild(control, NULL, BAD_CAST "minimumFitness", BAD_CAST cic::to_string("%f", set->_control.minimumFitness).c_str());
    xmlNewChild(control, NULL, BAD_CAST "populationXmlLoadFrom", BAD_CAST set->_control.populationXmlLoadFrom.c_str());
    xmlNewChild(control, NULL, BAD_CAST "useMatlabPlot", BAD_CAST cic::to_string("%i", set->_control.useMatlabPlot).c_str());
    xmlNewChild(control, NULL, BAD_CAST "isAutonomousExperiment", BAD_CAST cic::to_string("%i", set->_control.isAutonomousExperiment).c_str());
    xmlNewChild(control, NULL, BAD_CAST "showAndQuitOnInputPopulation", BAD_CAST cic::to_string("%i", set->_control.showAndQuitOnInputPopulation).c_str());
    
    //serialize bloat control settings
    bloat = xmlNewChild(set_node, NULL, BAD_CAST "BloatControl", BAD_CAST "");
    
    xmlNewChild(bloat, NULL, BAD_CAST "useBloatControl", BAD_CAST cic::to_string("%i", set->_bloat.useBloatControl).c_str());
    xmlNewChild(bloat, NULL, BAD_CAST "bloatControlVariant", BAD_CAST cic::to_string("%i", set->_bloat.bloatControlVariant).c_str());
    
    //serialize neural settings
    neural = xmlNewChild(set_node, NULL, BAD_CAST "Neural", BAD_CAST "");
    
    xmlNewChild(neural, NULL, BAD_CAST "synopsisThreshold", BAD_CAST cic::to_string("%f", set->_neural.synopsisThreshold).c_str());
    xmlNewChild(neural, NULL, BAD_CAST "useMiddleLegs", BAD_CAST cic::to_string("%i", set->_neural.useMiddleLegs).c_str());
    xmlNewChild(neural, NULL, BAD_CAST "sampleAverageCount", BAD_CAST cic::to_string("%i", set->_neural.sampleAverageCount).c_str());
    
    return set_node;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
Settings* io::xmlAccess::parseSettings(xmlNodePtr set_xml) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    Settings *set = Settings::get();
    
    xmlNodePtr childSetting = nullptr;
    xmlNodePtr miniSetting = nullptr;
    
    //make sure we're parsing settings
    if (xmlStrcmp(set_xml->name, BAD_CAST "settings")) throw logic_error("we're not parsing settings!");
    
    set_xml = set_xml->xmlChildrenNode;
    while (set_xml != NULL) {
        if (!xmlStrcmp(set_xml->name, (const xmlChar *)"GP")) {
            //we have GP
            childSetting = set_xml->xmlChildrenNode;
            
            while (childSetting != NULL) {
                
                if (!xmlStrcmp(childSetting->name, (const xmlChar *)"defaultFitness")) {
                    if (childSetting->children) set->_gp.defaultFitness = cic::convertToDouble((const char*)childSetting->children->content);
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"generatedPopulationSize")) {
                    if (childSetting->children) set->_gp.generatedPopulationSize = cic::convertToInt((const char*)childSetting->children->content);
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"expectedGeneratedTreeSize")) {
                    if (childSetting->children) set->_gp.expectedGeneratedTreeSize = cic::convertToInt((const char*)childSetting->children->content);
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"maximumGeneratedTreeDepth")) {
                    if (childSetting->children) set->_gp.maximumGeneratedTreeDepth = cic::convertToInt((const char*)childSetting->children->content);
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"minimumGeneratedTreeDepth")) {
                    if (childSetting->children) set->_gp.minimumGeneratedTreeDepth = cic::convertToInt((const char*)childSetting->children->content);
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"terminalVariableCount")) {
                    if (childSetting->children) set->_gp.terminalVariableCount = cic::convertToInt((const char*)childSetting->children->content);
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"functionCount")) {
                    if (childSetting->children) set->_gp.functionCount = cic::convertToInt((const char*)childSetting->children->content);
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"functionProbabilities")) {
                    miniSetting = childSetting->xmlChildrenNode;
                    while (miniSetting != NULL) {
                        if (!xmlStrcmp(miniSetting->name, (const xmlChar *)"vector")) {
                            set->_gp.functionProbabilities = this->parseDoubleVector(miniSetting);
                        }
                        miniSetting = miniSetting->next;
                    }
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"terminalProbabilities")) {
                    miniSetting = childSetting->xmlChildrenNode;
                    while (miniSetting != NULL) {
                        if (!xmlStrcmp(miniSetting->name, (const xmlChar *)"vector")) {
                            set->_gp.terminalProbabilities = this->parseDoubleVector(miniSetting);
                        }
                        miniSetting = miniSetting->next;
                    }
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"tournament_size")) {
                    if (childSetting->children) set->_gp.tournament_size = cic::convertToInt((const char*)childSetting->children->content);
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"selection_type")) {
                    if (childSetting->children) set->_gp.selection_type = cic::convertToInt((const char*)childSetting->children->content);
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"terminalConstsCount")) {
                    if (childSetting->children) set->_gp.terminalConstsCount = cic::convertToInt((const char*)childSetting->children->content);
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"tournamentSelectionUseLexicographicParsimonyPressure")) {
                    if (childSetting->children) set->_gp.tournamentSelectionUseLexicographicParsimonyPressure = cic::convertToInt((const char*)childSetting->children->content);
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"numberOfTreesInForestGenotype")) {
                    if (childSetting->children) set->_gp.numberOfTreesInForestGenotype = cic::convertToInt((const char*)childSetting->children->content);
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"preventDuplicates")) {
                    if (childSetting->children) set->_gp.preventDuplicates = cic::convertToInt((const char*)childSetting->children->content);
                }
                
                childSetting = childSetting->next;
            }
            
        } else if (!xmlStrcmp(set_xml->name, (const xmlChar *)"Simulator")) {
            //we have Simulator
            
            childSetting = set_xml->xmlChildrenNode;
            
            while (childSetting != NULL) {
                
                if (!xmlStrcmp(childSetting->name, (const xmlChar *)"sample_from")) {
                    if (childSetting->children) set->_simulator.sample_from = cic::convertToDouble((const char*)childSetting->children->content);
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"sample_step")) {
                    if (childSetting->children) set->_simulator.sample_step = cic::convertToDouble((const char*)childSetting->children->content);
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"sample_to")) {
                    if (childSetting->children) set->_simulator.sample_to = cic::convertToDouble((const char*)childSetting->children->content);
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"delayed_start")) {
                    if (childSetting->children) set->_simulator.delayed_start = cic::convertToDouble((const char*)childSetting->children->content);
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"robot_type")) {
                    if (childSetting->children) set->_simulator.robot_type = (RobotType::Type)cic::convertToInt((const char*)childSetting->children->content);
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"extraTimeStepDivider")) {
                    if (childSetting->children) set->_simulator.extraTimeStepDivider = cic::convertToInt((const char*)childSetting->children->content);
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"generatePovrayFiles")) {
                    if (childSetting->children) set->_simulator.generatePovrayFiles = cic::convertToInt((const char*)childSetting->children->content);
                }
                
                childSetting = childSetting->next;
            }
            
        } else if (!xmlStrcmp(set_xml->name, (const xmlChar *)"Control")) {
            //we have Control
            
            childSetting = set_xml->xmlChildrenNode;
            
            while (childSetting != NULL) {
                
                if (!xmlStrcmp(childSetting->name, (const xmlChar *)"maximumIterations")) {
                    if (childSetting->children) set->_control.maximumIterations = cic::convertToInt((const char*)childSetting->children->content);
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"minimumFitness")) {
                    if (childSetting->children) set->_control.minimumFitness = cic::convertToDouble((const char*)childSetting->children->content);
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"populationXmlLoadFrom")) {
                    if (childSetting->children) set->_control.populationXmlLoadFrom = (const char*)childSetting->children->content;
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"useMatlabPlot")) {
                    if (childSetting->children) set->_control.useMatlabPlot = cic::convertToInt((const char*)childSetting->children->content);
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"isAutonomousExperiment")) {
                    if (childSetting->children) set->_control.isAutonomousExperiment = cic::convertToInt((const char*)childSetting->children->content);
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"showAndQuitOnInputPopulation")) {
                    if (childSetting->children) set->_control.showAndQuitOnInputPopulation = cic::convertToInt((const char*)childSetting->children->content);
                }
                
                childSetting = childSetting->next;
            }
            
        } else if (!xmlStrcmp(set_xml->name, (const xmlChar *)"BloatControl")) {
            //we have Bloat Control
            
            childSetting = set_xml->xmlChildrenNode;
            
            while (childSetting != NULL) {
                
                if (!xmlStrcmp(childSetting->name, (const xmlChar *)"useBloatControl")) {
                    if (childSetting->children) set->_bloat.useBloatControl = cic::convertToInt((const char*)childSetting->children->content);
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"bloatControlVariant")) {
                    if (childSetting->children) set->_bloat.bloatControlVariant = cic::convertToInt((const char*)childSetting->children->content);
                }
                
                childSetting = childSetting->next;
            }
        } else if (!xmlStrcmp(set_xml->name, (const xmlChar *)"Neural")) {
            //we have Neural settings
            
            childSetting = set_xml->xmlChildrenNode;
            
            while (childSetting != NULL) {
                
                if (!xmlStrcmp(childSetting->name, (const xmlChar *)"synopsisThreshold")) {
                    if (childSetting->children) set->_neural.synopsisThreshold = cic::convertToDouble((const char*)childSetting->children->content);
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"useMiddleLegs")) {
                    if (childSetting->children) set->_neural.useMiddleLegs = cic::convertToInt((const char*)childSetting->children->content);
                } else if (!xmlStrcmp(childSetting->name, (const xmlChar *)"sampleAverageCount")) {
                    if (childSetting->children) set->_neural.sampleAverageCount = cic::convertToInt((const char*)childSetting->children->content);
                }
                
                childSetting = childSetting->next;
            }
        }
        
        set_xml = set_xml->next;
    }
    
    return set;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
xmlNodePtr io::xmlAccess::serializeDoubleVector(vector<double> vec) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    xmlNodePtr vec_node = xmlNewNode(NULL, BAD_CAST "vector");
    xmlNewProp(vec_node, BAD_CAST "type", BAD_CAST "double");
    
    for (vector<double>::const_iterator it = vec.begin(); it != vec.end(); ++it) {
        xmlNewChild(vec_node, NULL, BAD_CAST "value", BAD_CAST cic::to_string("%f", *it).c_str());
    }
    return vec_node;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
vector<double> io::xmlAccess::parseDoubleVector(xmlNodePtr vec_xml) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    vector<double> vec;
    
    //make sure we're parsing a double vector
    xmlChar *type = xmlGetProp(vec_xml, BAD_CAST "type");
    if (xmlStrcmp(type, BAD_CAST "double")) throw logic_error("wrong vector type!");
    xmlFree(type);
    
    double val;
    
    //just fill the population with the individual (first put them in shared_ptrs)
    vec_xml = vec_xml->xmlChildrenNode;
    while (vec_xml != NULL) {
        if (!xmlStrcmp(vec_xml->name, (const xmlChar *)"value")) {
            //we have a value, first parse it
            val = cic::convertToDouble((const char*)vec_xml->children->content);
            vec.push_back(val);
        }
        vec_xml = vec_xml->next;
    }
    
    return vec;
}

//declare test methods
std::unique_ptr< cic::tree::Tree<io::xmlAccess::tree_type> > makeDummyTree();

#pragma mark - xmlHandler

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
string io::xmlHandler::savePopulationToXml(const Population* pop) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    unique_ptr< io::xmlAccess > access = make_unique<io::xmlAccess>();
    
    //get serialized data
    
    const genetic::Population* pp = dynamic_cast<const genetic::Population*>(pop);
    xmlNodePtr ser_pop = access->serializePopulation(pp);
    
    //create doc
    xmlDocPtr doc = xmlNewDoc(BAD_CAST "1.0");
    xmlDocSetRootElement(doc, ser_pop);
    
    //    mode_t permissions = 777;
    //    mkdir(folder.c_str(), permissions);
    string filename = Settings::get()->outputFilePrefix() + "_population.xml";
    
    //save to xml
    xmlSaveFormatFile(filename.c_str(), doc, 1);
    
    //free associated memory
    xmlFreeDoc(doc);
    
    return filename;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
std::shared_ptr<Population> io::xmlHandler::loadPopulationFromXml(const char* filename) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    unique_ptr< io::xmlAccess > access = make_unique<io::xmlAccess>();
    
    xmlDocPtr doc = xmlParseFile(filename);
    
    if (doc == NULL) {
        cout << "Document not parsed successfully." << endl;
        return nullptr;
    }
    
    xmlNodePtr root = xmlDocGetRootElement(doc);
    
    if (root == NULL) {
        cout << "empty document" << endl;
        xmlFreeDoc(doc);
        return nullptr;
    }
    
    //parse the population
    
    shared_ptr<Population> p = access->parsePopulation(root);
    xmlFreeDoc(doc);
    
    return p;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void io::xmlHandler::saveSettingsToXml(std::string filename) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    unique_ptr< io::xmlAccess > access = make_unique<io::xmlAccess>();
    
    //get serialized settings
    xmlNodePtr ser_set = access->serializeSettings();
    
    //add date and time to properties
    xmlNewProp(ser_set, BAD_CAST "timestamp", BAD_CAST cic::currentDateTime().c_str());
    
    //create doc
    xmlDocPtr doc = xmlNewDoc(BAD_CAST "1.0");
    xmlDocSetRootElement(doc, ser_set);
    
    //save to xml
    xmlSaveFormatFile(filename.c_str(), doc, 1);
    
    //free associated memory
    xmlFreeDoc(doc);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
bool io::xmlHandler::loadSettingsFromXml(std::string filename) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    unique_ptr< io::xmlAccess > access = make_unique<io::xmlAccess>();
    
    xmlDocPtr doc = xmlParseFile(filename.c_str());
    
    if (doc == NULL) {
        cout << "Document not parsed successfully." << endl;
        return false;
    }
    
    xmlNodePtr root = xmlDocGetRootElement(doc);
    
    if (root == NULL) {
        cout << "empty document" << endl;
        xmlFreeDoc(doc);
        return false;
    }
    
    //parse the population
    
    Settings *set = access->parseSettings(root);
    
    xmlFreeDoc(doc);
    
    //set settings
    Settings::set(set);
    
    return true;
}





/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_nn_Grid.cpp
//  hyperGP
//
//  Created by Honza Dvorsky on 3/17/13.
//

#include "cic_nn_Grid.h"
#include <cmath>
#include "memoryutils.h"
#include "cic_io_matlab_handler.h"
#include "cic_Settings.h"
#include "cic_tree_forest.h"

using namespace std;
using namespace cic;

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
nn::Grid::Grid(size_t sideX, size_t sideY) : _sideX(sideX), _sideY(sideY)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    if (_sideX % 2 == 0 || _sideY % 2 == 0) {
        throw logic_error("grid side needs to be an odd number!");
    }
    
    //construct a grid side x side
    //grid contains coordinates x: [-1, 1], y: [-1, 1]
    
    _neurons = new map<neuron_id, Neuron*>();
    
    //construct a sideX x sideY neural network with dummy neurons.
    
    int pos_max_x = floor(sideX/2);
    int pos_max_y = floor(sideY/2);
    
    for (int i = -pos_max_x; i <= pos_max_x; ++i) {
        for (int j = -pos_max_y; j <= pos_max_y; ++j) {
            
            neuron_id n_id = neuron_id(i,j);
            Neuron *neu = new Neuron();
            pair<neuron_id, Neuron*> pr(n_id, neu);
            
            _neurons->insert(pr);
        }
    }
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
nn::Grid::~Grid()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //delete all the neurons in the storage
    for (map<neuron_id, Neuron*>::iterator it = _neurons->begin(); it != _neurons->end(); ++it) {
        delete it->second;
    }
    delete _neurons;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
std::string nn::Grid::description()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    typedef map<neuron_id, Neuron*> neuron_map;
    string desc;
    for (neuron_map::const_iterator it = _neurons->begin(); it != _neurons->end(); ++it) {
        desc += "@ x = ";
        desc += cic::to_string("%i", it->first.x);
        desc += " y = ";
        desc += cic::to_string("%i ", it->first.y);
        desc += it->second->description();
        desc += "\n";
    }
    
    return desc;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
nn::Neuron* nn::Grid::neuronAtVirtualCoords(int i, int j) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    map<neuron_id, Neuron*>::iterator it = _neurons->find(neuron_id(i,j));
    
    if (it == _neurons->end()) {
        //didn't find it
        return nullptr;
    }
    
    //found it
    return it->second;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void nn::Grid::setupSimple5Robot()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //setup bias neuron
    nn::Neuron* bias = this->neuronAtVirtualCoords(0, 0);
    bias->setNeuronType(neuron_type::Bias);
    bias->setConstValue(1);
    
    //setup input neurons
    nn::Neuron* in1 = this->neuronAtVirtualCoords( 0,  2);
    nn::Neuron* in2 = this->neuronAtVirtualCoords( 0, -2);
    
    in1->setNeuronType(neuron_type::Input);
    in2->setNeuronType(neuron_type::Input);
    
    in1->setConstValue(1);
    in2->setConstValue(1);
    
    //setup output neurons
    nn::Neuron* out1 = this->neuronAtVirtualCoords(-2, 2);
    nn::Neuron* out2 = this->neuronAtVirtualCoords(-1, 2);
    nn::Neuron* out3 = this->neuronAtVirtualCoords( 1, 2);
    nn::Neuron* out4 = this->neuronAtVirtualCoords( 2, 2);
    
    nn::Neuron* out5 = this->neuronAtVirtualCoords(-2,-2);
    nn::Neuron* out6 = this->neuronAtVirtualCoords(-1,-2);
    nn::Neuron* out7 = this->neuronAtVirtualCoords( 1,-2);
    nn::Neuron* out8 = this->neuronAtVirtualCoords( 2,-2);
    
    out1->setNeuronType(neuron_type::Output);
    out2->setNeuronType(neuron_type::Output);
    out3->setNeuronType(neuron_type::Output);
    out4->setNeuronType(neuron_type::Output);
    out5->setNeuronType(neuron_type::Output);
    out6->setNeuronType(neuron_type::Output);
    out7->setNeuronType(neuron_type::Output);
    out8->setNeuronType(neuron_type::Output);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void nn::Grid::setupComplex7Robot()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //setup bias neuron
    nn::Neuron* bias = this->neuronAtVirtualCoords(0, 0);
    bias->setNeuronType(neuron_type::Bias);
    bias->setConstValue(1);

    //setup input neurons
    nn::Neuron* in1 = this->neuronAtVirtualCoords( 0,  3);
    nn::Neuron* in2 = this->neuronAtVirtualCoords( 0, -3);

    in1->setNeuronType(neuron_type::Input);
    in2->setNeuronType(neuron_type::Input);

    in1->setConstValue(1);
    in2->setConstValue(1);

    //setup output neurons
    nn::Neuron* out1 = this->neuronAtVirtualCoords(-3, 3);
    nn::Neuron* out2 = this->neuronAtVirtualCoords(-2, 3);
    nn::Neuron* out3 = this->neuronAtVirtualCoords(-1, 3);
    nn::Neuron* out4 = this->neuronAtVirtualCoords( 1, 3);
    nn::Neuron* out5 = this->neuronAtVirtualCoords( 2, 3);
    nn::Neuron* out6 = this->neuronAtVirtualCoords( 3, 3);
    
    nn::Neuron* out7 = this->neuronAtVirtualCoords(-3,-3);
    nn::Neuron* out8 = this->neuronAtVirtualCoords(-2,-3);
    nn::Neuron* out9 = this->neuronAtVirtualCoords(-1,-3);
    nn::Neuron* out10 = this->neuronAtVirtualCoords( 1,-3);
    nn::Neuron* out11 = this->neuronAtVirtualCoords( 2,-3);
    nn::Neuron* out12 = this->neuronAtVirtualCoords( 3,-3);
    
    //hips?
//    nn::Neuron* out13 = this->neuronAtVirtualCoords( 0,-3);
//    nn::Neuron* out14 = this->neuronAtVirtualCoords( 0, 2);
    
    out1->setNeuronType(neuron_type::Output);
    out2->setNeuronType(neuron_type::Output);
    out3->setNeuronType(neuron_type::Output);
    out4->setNeuronType(neuron_type::Output);
    out5->setNeuronType(neuron_type::Output);
    out6->setNeuronType(neuron_type::Output);
    
    out7->setNeuronType(neuron_type::Output);
    out8->setNeuronType(neuron_type::Output);
    out9->setNeuronType(neuron_type::Output);
    out10->setNeuronType(neuron_type::Output);
    out11->setNeuronType(neuron_type::Output);
    out12->setNeuronType(neuron_type::Output);
    
//    out13->setNeuronType(neuron_type::Output);
//    out14->setNeuronType(neuron_type::Output);
    
    if (Settings::get()->_neural.useMiddleLegs) {
        nn::Neuron* out13 = this->neuronAtVirtualCoords(-3, 0);
        nn::Neuron* out14 = this->neuronAtVirtualCoords(-2, 0);
        nn::Neuron* out15 = this->neuronAtVirtualCoords(-1, 0);
        nn::Neuron* out16 = this->neuronAtVirtualCoords( 1, 0);
        nn::Neuron* out17 = this->neuronAtVirtualCoords( 2, 0);
        nn::Neuron* out18 = this->neuronAtVirtualCoords( 3, 0);
        
        out13->setNeuronType(neuron_type::Output);
        out14->setNeuronType(neuron_type::Output);
        out15->setNeuronType(neuron_type::Output);
        out16->setNeuronType(neuron_type::Output);
        out17->setNeuronType(neuron_type::Output);
        out18->setNeuronType(neuron_type::Output);
    }

}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void nn::Grid::setupRect57Robot()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //setup bias neuron
    nn::Neuron* bias = this->neuronAtVirtualCoords(0, 0);
    bias->setNeuronType(neuron_type::Bias);
    bias->setConstValue(1);
    
    //setup input neurons
    nn::Neuron* in1 = this->neuronAtVirtualCoords( 0,  2);
    nn::Neuron* in2 = this->neuronAtVirtualCoords( 0, -2);
    
    in1->setNeuronType(neuron_type::Input);
    in2->setNeuronType(neuron_type::Input);
    
    in1->setConstValue(1);
    in2->setConstValue(1);
    
    //setup output neurons
    nn::Neuron* out1 = this->neuronAtVirtualCoords(-3, 2);
    nn::Neuron* out2 = this->neuronAtVirtualCoords(-2, 2);
    nn::Neuron* out3 = this->neuronAtVirtualCoords(-1, 2);
    nn::Neuron* out4 = this->neuronAtVirtualCoords( 1, 2);
    nn::Neuron* out5 = this->neuronAtVirtualCoords( 2, 2);
    nn::Neuron* out6 = this->neuronAtVirtualCoords( 3, 2);
    
    nn::Neuron* out7 = this->neuronAtVirtualCoords(-3,-2);
    nn::Neuron* out8 = this->neuronAtVirtualCoords(-2,-2);
    nn::Neuron* out9 = this->neuronAtVirtualCoords(-1,-2);
    nn::Neuron* out10 = this->neuronAtVirtualCoords( 1,-2);
    nn::Neuron* out11 = this->neuronAtVirtualCoords( 2,-2);
    nn::Neuron* out12 = this->neuronAtVirtualCoords( 3,-2);
    
    //hips?
    //    nn::Neuron* out13 = this->neuronAtVirtualCoords( 0,-3);
    //    nn::Neuron* out14 = this->neuronAtVirtualCoords( 0, 2);
    
    out1->setNeuronType(neuron_type::Output);
    out2->setNeuronType(neuron_type::Output);
    out3->setNeuronType(neuron_type::Output);
    out4->setNeuronType(neuron_type::Output);
    out5->setNeuronType(neuron_type::Output);
    out6->setNeuronType(neuron_type::Output);
    
    out7->setNeuronType(neuron_type::Output);
    out8->setNeuronType(neuron_type::Output);
    out9->setNeuronType(neuron_type::Output);
    out10->setNeuronType(neuron_type::Output);
    out11->setNeuronType(neuron_type::Output);
    out12->setNeuronType(neuron_type::Output);
    
    //    out13->setNeuronType(neuron_type::Output);
    //    out14->setNeuronType(neuron_type::Output);
    
    if (Settings::get()->_neural.useMiddleLegs) {
        nn::Neuron* out13 = this->neuronAtVirtualCoords(-3, 0);
        nn::Neuron* out14 = this->neuronAtVirtualCoords(-2, 0);
        nn::Neuron* out15 = this->neuronAtVirtualCoords(-1, 0);
        nn::Neuron* out16 = this->neuronAtVirtualCoords( 1, 0);
        nn::Neuron* out17 = this->neuronAtVirtualCoords( 2, 0);
        nn::Neuron* out18 = this->neuronAtVirtualCoords( 3, 0);
        
        out13->setNeuronType(neuron_type::Output);
        out14->setNeuronType(neuron_type::Output);
        out15->setNeuronType(neuron_type::Output);
        out16->setNeuronType(neuron_type::Output);
        out17->setNeuronType(neuron_type::Output);
        out18->setNeuronType(neuron_type::Output);
    }
    
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void nn::Grid::setupOneRobot()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //setup output neurons
    nn::Neuron* out1 = this->neuronAtVirtualCoords(0, 0);
    out1->setNeuronType(neuron_type::Output);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void nn::Grid::setupGrid()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //set all neurons as if they were all hidden
    for (map<neuron_id, Neuron*>::iterator it = _neurons->begin(); it != _neurons->end(); ++it) {
        it->second->setNeuronType(neuron_type::Hidden);
    }
    
    switch (Settings::get()->_simulator.robot_type) {
        case RobotType::Simple5:
            this->setupSimple5Robot();
            break;
        case RobotType::Complex7:
            this->setupComplex7Robot();
            break;
        case RobotType::Rect57:
            this->setupRect57Robot();
            break;
        case RobotType::One:
            this->setupOneRobot();
            break;
        default:
            assert("unrecognized robot type");
            break;
    }
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
std::vector<double> nn::Grid::realCoordinates(const nn::neuron_id& n_id) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    double quantum_x, quantum_y;
    vector<double> coords;
    
    quantum_x = 1/(double)floor(_sideX/2);
    quantum_y = 1/(double)floor(_sideY/2);
    
    coords.push_back(n_id.x*quantum_x);
    coords.push_back(n_id.y*quantum_y);
    
    return coords;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void nn::Grid::showMatlabFig()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //launch matlab, tell it to show neural network, close it
    
    io::MatlabHandler::get()->showNeuralNetwork(this);
}


//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
unique_ptr< map<nn::neuron_id, nn::Neuron*> > nn::Grid::filterNeurons(nn::neuron_type type) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //filter out just the output ones
    unique_ptr< map<nn::neuron_id, nn::Neuron*> > neur = make_unique< map<nn::neuron_id, nn::Neuron*> >();
    
    for (map<nn::neuron_id, nn::Neuron*>::const_iterator it = _neurons->begin(); it != _neurons->end(); ++it) {
        if (it->second->neuronType() == type) {
            neur->insert(*it);
        }
    }
    
    return neur;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void nn::Grid::fillSynapses()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //query the source for weights between all neurons (both ways and with repetition, because we allow recurrent synapsis)
    //convert to absolute coordinates!
    
    map<neuron_id, Neuron*>* all = this->neurons();
    double weight;
    double threshold = Settings::get()->_neural.synopsisThreshold;
    double stepTime = Settings::get()->_simulator.sample_step;
    double offset;
    
    map<neuron_id, Neuron*>::iterator from, to;
    
    for (from = all->begin(); from != all->end(); ++from) {
        for (to = all->begin(); to != all->end(); ++to) {
            //we don't allow inputs and biases to have input synapses
            if (to->second->neuronType() <= neuron_type::Input) continue;
            
            //query for the weight
            weight = weightFromNeuronToNeuron(*from, *to);
            
//            cout << "weight: " << weight << endl;
//#warning WEIGHTS DECREASED
//            weight /= 5;
            
            //only create a synapsis if the weight is at or above certain threshold
            if (abs(weight) < threshold) continue;
            
            //if the neurons are from the same layer and are connecting each other (Hidden-Hidden, Output-Output)
            //we need to 'delay' that synapsis until next step - memory!
            offset = (from->second->neuronType() < to->second->neuronType()) ? 0 : -stepTime;
            
            //finally register the synapsis to the receiver
            to->second->addInputSynapsis(from->second, weight, offset);
        }
    }
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
nn::SynapsisType nn::Grid::synapsisTypeBetweenNeurons(const nn::Neuron* from, const nn::Neuron* to) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    switch (from->neuronType()) {
        case nn::neuron_type::Bias:
            
            switch (to->neuronType()) {
                case nn::neuron_type::Hidden:
                    return nn::SynapsisType::BiasToHidden;
                    break;
                case nn::neuron_type::Output:
                    return nn::SynapsisType::BiasToOutput;
                    break;
                
                default:
                    assert(false);
                    break;
            }
            
            break;
            
        case nn::neuron_type::Input:
            
            switch (to->neuronType()) {
                case nn::neuron_type::Hidden:
                    return nn::SynapsisType::InputToHidden;
                    break;
                case nn::neuron_type::Output:
                    return nn::SynapsisType::InputToOutput;
                    break;
                    
                default:
                    assert(false);
                    break;
            }
            
            break;
            
        case nn::neuron_type::Hidden:
            
            switch (to->neuronType()) {
                case nn::neuron_type::Hidden:
                    return nn::SynapsisType::HiddenToHidden;
                    break;
                case nn::neuron_type::Output:
                    return nn::SynapsisType::HiddenToOutput;
                    break;
                    
                default:
                    assert(false);
                    break;
            }
            
            break;
            
        case nn::neuron_type::Output:
            
            switch (to->neuronType()) {
                case nn::neuron_type::Hidden:
                    return nn::SynapsisType::OutputToHidden;
                    break;
                case nn::neuron_type::Output:
                    return nn::SynapsisType::OutputToOutput;
                    break;
                    
                default:
                    assert(false);
                    break;
            }
            
            break;
            
        default:
            break;
    }
    
    assert(false);
    throw logic_error("wrong synapsis type!");
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
double nn::Grid::weightFromNeuronToNeuron(const pair<neuron_id, Neuron*>& nA, const pair<neuron_id, Neuron*>& nB)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    double x1, y1, x2, y2;
    vector<double> n1, n2;
    
    //convert to absolute coordinates that are always between -1 and 1
    n1 = this->realCoordinates(nA.first);
    n2 = this->realCoordinates(nB.first);
    
    //make them human readable
    x1 = n1[0];
    y1 = n1[1];
    x2 = n2[0];
    y2 = n2[1];
    
    //aaaaand, get the value from the source
    //***************************************************************************
    
    //get weights from source (tree this time)
    //convert to tree! make sure it's a tree!
    
    tree::Tree<tree_type>* tree;
    tree::Forest<tree_type>* forest = (tree::Forest<tree_type>*)_source;
    
    switch (this->synapsisTypeBetweenNeurons(nA.second, nB.second)) {
        case BiasToHidden:
            tree = forest->treeAtIndex(0);
            break;
            
        case BiasToOutput:
            
            if (Settings::get()->_gp.numberOfTreesInForestGenotype > 1) {
                tree = forest->treeAtIndex(1);
            } else {
                tree = forest->treeAtIndex(0);
            }
            
            break;

        case InputToHidden:
        case HiddenToHidden:
        case OutputToHidden:
            
            if (Settings::get()->_gp.numberOfTreesInForestGenotype > 2) {
                tree = forest->treeAtIndex(2);
            } else {
                tree = forest->treeAtIndex(0);
            }
            
            break;
            
        case InputToOutput:
        case HiddenToOutput:
        case OutputToOutput:
            
            if (Settings::get()->_gp.numberOfTreesInForestGenotype > 3) {
                tree = forest->treeAtIndex(3);
            } else {
                tree = forest->treeAtIndex(0);
            }
            
            break;
            
        default:
            assert(false);
            throw logic_error("wrong synapsis type!");
            break;
    }
    
    vector<double> *map = new vector<double>();
    
    map->push_back(x1);
    map->push_back(y1);
    map->push_back(x2);
    map->push_back(y2);
    
    tree->setTermMappedValues(map);
    
    //read the value of the weight
    return tree->rootValue();
}














/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_nn_Network.cpp
//  hyperGP
//
//  Created by Honza Dvorsky on 3/12/13.
//

#include "cic_nn_Network.h"
#include "cic_stringutils.h"
#include <iterator>
#include <limits>
#include <numeric>

using namespace std;
using namespace cic;

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
nn::Network::Network(size_t gridSideX, size_t gridSideY)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    _grid = new Grid(gridSideX, gridSideY);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void nn::Network::showMatlabFig()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    _grid->showMatlabFig();
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
map<double, map<nn::neuron_id, double> > nn::Network::computeOutputData(int avgOverSamples) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //make sure we have a source, thus probably filled synapses
    if (!_grid->weightSource()) {
        throw logic_error("synapses were probably not filled! missing source.");
    }
    
    //**********************
    
    double startTime = Settings::get()->_simulator.sample_from;
    double endTime = Settings::get()->_simulator.sample_to;
    double stepTime = Settings::get()->_simulator.sample_step;
    
    //**********************
    
    typedef map<neuron_id, Neuron*> neuron_map;
    typedef map<neuron_id, double> output_map;
    typedef map<double, output_map> out_data_type;
    
    unique_ptr< neuron_map > neurons = _grid->filterNeurons(neuron_type::Output);
    
    //prepare outdata
    out_data_type out_data;
    
    //query the neural network for outputs over time and record them
    double t, val;
    neuron_map::iterator c_it;
    output_map outputValues;
    
    vector<double> sampleAvgBuffer = vector<double>(neurons->size());
    
    int sampleNumber = 0;
    for (t = startTime; t <= endTime; t += stepTime, sampleNumber++) {
        
        //round to the fifth decimal place
        int places = 10;
        double bigT = pow(10, places) * t;
        bigT = round(bigT);
        t = bigT / pow(10,places);
        
        //fill the results
        for (c_it = neurons->begin(); c_it != neurons->end(); ++c_it) {
            val = c_it->second->outputValueAtTime(t, t);
            
            //add to the average
            size_t idx = distance(neurons->begin(), c_it);
            sampleAvgBuffer[idx] += val;
                        
            //if we're in the last sample of the cycle, average out and write
            if ((sampleNumber) % avgOverSamples == 0) {
                                
                sampleAvgBuffer[idx] /= avgOverSamples; //get average value
                
#warning MAKE NEURONS INTEGRATING
//                double newOutput = d outputValues.end()
                
                outputValues.insert(pair<neuron_id, double>(c_it->first, sampleAvgBuffer[idx])); //record
                sampleAvgBuffer[idx] = 0; //clean up
            }
        }
        
        if ((sampleNumber) % avgOverSamples == 0) {
                        
            //record the results for these time steps
            out_data.insert(pair<double, output_map>(t, outputValues));
            outputValues.clear();            //clean up
        }
        
    }
    
    //here we have out_data with all the data we need.
    return out_data;
}









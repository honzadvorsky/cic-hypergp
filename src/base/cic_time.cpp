/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_time.cpp
//  hyperGP
//
//  Created by Honza Dvorsky on 3/30/13.
//

#include "cic_time.h"
#include "cic_stringutils.h"
#include <sys/time.h>

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
long cic::tm::currentMs()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    timeval time;
    gettimeofday(&time, NULL);
    return (time.tv_sec * 1000) + (time.tv_usec / 1000);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
std::string cic::tm::formattedTimeFromMs(long ms)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    int h = (int)(ms / (1000*60*60));
    int m = (int)((ms - h*1000*60*60) / (1000*60));
    int s = (int)((ms - m*1000*60 - h*1000*60*60) / (1000));
    
    return "[h:" + cic::to_string("%i",h) + " m:" + cic::to_string("%i",m) +  " s:" + cic::to_string("%i",s) + "]";
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
std::string formattedDiff(long start_ms, long end_ms)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    return cic::tm::formattedTimeFromMs(end_ms - start_ms);
}



/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_nn_Grid.h
//  hyperGP
//
//  Created by Honza Dvorsky on 3/17/13.
//

#ifndef __hyperGP__cic_nn_Grid__
#define __hyperGP__cic_nn_Grid__

#include <iostream>
#include <vector>
#include <map>
#include <memory>
#include "cic_nn_Neuron.h"

namespace cic {
    namespace nn {
        class Grid;
        
        enum SynapsisType {
            BiasToHidden,
            BiasToOutput,
            
            InputToHidden,
            InputToOutput,
            
            HiddenToHidden,
            HiddenToOutput,

            OutputToHidden,
            OutputToOutput
        };
    }
}

/** Base class neural grid. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::nn::Grid
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    size_t _sideX, _sideY;
    std::map<neuron_id, Neuron*>* _neurons;
    
    typedef double tree_type;
    void* _source;
    
    Neuron* neuronAtVirtualCoords(int i, int j) const;
    SynapsisType synapsisTypeBetweenNeurons(const nn::Neuron* from, const nn::Neuron* to) const;
    
    void setupSimple5Robot();
    void setupComplex7Robot();
    void setupRect57Robot();
    void setupOneRobot();
    
public:
    
    /** 
     Side == number of neurons in one dimension in the grid.
     Odd number. Total number of neurons == side^2.
     */
    Grid(size_t sideX, size_t sideY);
    virtual ~Grid();
    
    /** Create grid, but no synapses yet. */
    virtual void setupGrid();
    
    /** Fill synapses values from the source. */
    virtual void fillSynapses();
    
    /** Query for the weight value. */
    virtual double weightFromNeuronToNeuron(const std::pair<neuron_id, Neuron*>& nA, const std::pair<neuron_id, Neuron*>& nB);
    
    /** Shows matlab figure of the network. */
    virtual void showMatlabFig();
    
    /** Integer -> real coordinates. */
    virtual std::vector<double> realCoordinates(const neuron_id& n_id) const;

    /** Get only a certain type of neurons. */
    virtual std::unique_ptr< std::map<neuron_id, Neuron*> > filterNeurons(neuron_type type) const;
    
    /** Get all neurons. */
    virtual std::map<neuron_id, Neuron*>* neurons() const { return _neurons; };
    
    /** Get string description. */
    virtual std::string description();
    
    /** Adds the source which is used to pull out weights. */
    virtual void setWeightSource(void* source) { _source = source; this->setupGrid(); this->fillSynapses(); };
    
    /** Read-only getter for the weight source. */
    virtual const void* weightSource() const { return _source; };
    
};

#endif /* defined(__hyperGP__cic_nn_Grid__) */








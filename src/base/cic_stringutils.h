/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_stringutils.h
//  hyperGP
//
//  Created by Honza Dvorsky on 2/10/13.
//

#ifndef __hyperGP__cic_stringutils__
#define __hyperGP__cic_stringutils__

#include <iostream>
#include <stdio.h>
#include <string>
#include <exception>
#include <sstream>
#include <time.h>

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
namespace cic {
    template <class T> const std::string to_string(const std::string& format, const T& val);
    template <class T, class U> const std::string to_string(const std::string& format, const T& val1, const U& val2);
    
    double convertToDouble(std::string const& s, bool failIfLeftoverChars);
    int convertToInt(std::string const& s, bool failIfLeftoverChars);
    
    std::string randomString(const int length);
    std::string currentDateTime();
}
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> const std::string cic::to_string(const std::string& format, const T& val)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //let's hope that's enough
    char buffer[100];
    std::sprintf(buffer, format.c_str(), val);
    return buffer;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T, class U> const std::string cic::to_string(const std::string& format, const T& val1, const U& val2)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //let's hope that's enough
    char buffer[100];
    std::sprintf(buffer, format.c_str(), val1, val2);
    return buffer;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
inline double cic::convertToDouble(std::string const& s, bool failIfLeftoverChars = true)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    std::istringstream i(s);
    double x;
    char c;
    if (!(i >> x) || (failIfLeftoverChars && i.get(c)))
        throw std::logic_error("bad cast from string to double");
    return x;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
inline int cic::convertToInt(std::string const& s, bool failIfLeftoverChars = true)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    std::istringstream i(s);
    int x;
    char c;
    if (!(i >> x) || (failIfLeftoverChars && i.get(c)))
        throw std::logic_error("bad cast from string to int");
    return x;
}


#endif /* defined(__hyperGP__cic_stringutils__) */

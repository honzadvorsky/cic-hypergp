/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_PopulationModifier.h
//  hyperGP
//
//  Created by Honza Dvorsky on 2/2/13.
//

#ifndef __hyperGP__cic_PopulationModifier__
#define __hyperGP__cic_PopulationModifier__

#include "cic_Population.h"
#include <memory>

namespace cic {
    class PopulationModifier;
} //namespace cic

/** Abstract base class for all population modifiers. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::PopulationModifier
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
public:
    /** Init all variables into desired state. */
    virtual void                                    init() = 0;
    /** The purpose of population modifier -> to take input population and spit out output population. */
    virtual std::shared_ptr<cic::Population>        processPopulation(std::shared_ptr<cic::Population> popIn) const = 0;
    
    virtual ~PopulationModifier() { };
};

#endif /* defined(__hyperGP__cic_PopulationModifier__) */

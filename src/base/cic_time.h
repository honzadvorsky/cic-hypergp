/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_time.h
//  hyperGP
//
//  Created by Honza Dvorsky on 3/30/13.
//

#ifndef __hyperGP__cic_time__
#define __hyperGP__cic_time__

#include <iostream>

namespace cic {
    namespace tm {
        long currentMs();
        std::string formattedTimeFromMs(long ms);
        std::string formattedDiff(long start_ms, long end_ms);
    }
}

#endif /* defined(__hyperGP__cic_time__) */

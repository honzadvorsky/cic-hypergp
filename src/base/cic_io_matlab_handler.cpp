/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_io_matlab_handler.cpp
//  hyperGP
//
//  Created by Honza Dvorsky on 3/4/13.
//

#include "cic_io_matlab_handler.h"
#include <cmath>
#include "engine.h"
#include <unistd.h>
#include <iterator>
#include "cic_stringutils.h"
#include "cic_Settings.h"
#include "cic_nn_Network.h"

using namespace cic;
using namespace std;

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void io::MatlabHandler::init()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    _matlab_engine = engOpen(NULL);
    
    _allKeys = new vector<string>();
    _allKeys->push_back("avg_fitness");
    _allKeys->push_back("avg_depth");
    _allKeys->push_back("max_fitness");
    _allKeys->push_back("max_depth");
    
    if (Settings::get()->_bloat.useBloatControl) {
        _allKeys->push_back("dynamic_limit");
    }

    _allStats = new map< string, vector<double>* >();

    for (vector<string>::const_iterator it = _allKeys->begin(); it != _allKeys->end(); ++it) {
        _allStats->insert(pair< string, vector<double>* >(*it, new vector<double>()));
    }
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void io::MatlabHandler::destroy()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    if (_matlab_engine) {
        engClose((Engine*)_matlab_engine);
    }
    
    //cleanup
    for (map< string, vector<double>* >::iterator it = _allStats->begin(); it != _allStats->end(); ++it) {
        delete it->second;
    }
    delete _allStats;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void io::MatlabHandler::updateFigure(bool saveToFile)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //update the figure (send data to matlab and redraw)
    
//    vector<string> atts { "avg_fitness", "max_fitness", "avg_depth", "max_depth", "avg_size", "max_size" };
    vector<string> atts(*_allKeys);
    
    //put in the strings of all attributes to plot
    string allStrings = "attStr = cellstr([";
    
    //fill each data set into matlab
    for (vector<string>::const_iterator it = atts.begin(); it != atts.end(); ++it, allStrings += ";") {
        
        //record this name
        allStrings += "'" + *it + "'";
        
        //get the ref vector
        vector<double>& vec = *_allStats->at(*it);
        
        //create vector in matlab
        mxArray *array = mxCreateDoubleMatrix(vec.size(),1,mxREAL);
        
        //find the extreme element for matlab
        double max = *max_element(vec.begin(), vec.end());
        double min;
        mxArray *extr;
        
        if (max > 0) {
            //if extr is greater than 0, it's all good, we needed a max.
            extr = mxCreateDoubleScalar(max);
        } else if ((min = *min_element(vec.begin(), vec.end())) < 0) {
            //nope, we were looking for a minimum
            extr = mxCreateDoubleScalar(min);
        } else {
            //both are probably == 0, just send it in
            extr = mxCreateDoubleScalar(0);
        }

        //get the array
        double* doub_arr = mxGetPr(array);
        
        //fill the datar
        for (int i = 0; i < vec.size(); ++i) {
            doub_arr[i] = vec[i];
        }
        
        //pass to matlab with names
        engPutVariable((Engine*)_matlab_engine, string("data_"+(*it)).c_str(), array);
        engPutVariable((Engine*)_matlab_engine, string("extr_"+(*it)).c_str(), extr);
        
    }
    
    allStrings += "]);";
    
    const char** c_strings = new const char*[ atts.size() ];
    for(size_t a=0; a<atts.size(); ++a) c_strings[a] = atts[a].c_str();
    
    mxArray *array_ptr = mxCreateCharMatrixFromStrings(atts.size(), c_strings);

    engPutVariable((Engine*)_matlab_engine, "allStr_arr", array_ptr);
    
    //------------
    
    mxArray *max_it = mxCreateDoubleScalar(Settings::get()->_control.maximumIterations);
    mxArray *save_mat = mxCreateLogicalScalar(saveToFile);
    
    engPutVariable((Engine*)_matlab_engine, "max_iter", max_it);
    engPutVariable((Engine*)_matlab_engine, "saveToFile", save_mat);
    
    char *path=NULL;
    size_t size = 0;
    path=getcwd(path,size);
        
    if (saveToFile) {
        mxArray *mat_path = mxCreateString(string(path + string("/") + Settings::get()->outputFilePrefix()).c_str());
        engPutVariable((Engine*)_matlab_engine, "mat_path", mat_path);
    }
    
    string runner = "run('" + string(path) + "/show_plot.m');";
    engEvalString((Engine*)_matlab_engine, runner.c_str());
    
    delete [] c_strings;
     
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void io::MatlabHandler::updateStatistic(std::map<std::string, double> statForPop, size_t it_number)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //add to the container
    for (map<string, double>::const_iterator it = statForPop.begin(); it != statForPop.end(); ++it) {
        _allStats->at((*it).first)->push_back((*it).second);
    }
    
    if (it_number % Settings::get()->_control.maximumIterations == 0) {
        this->updateFigure(false);
    }
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void io::MatlabHandler::showNeuralNetwork(void* network) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    bool saveToFile = true;
        
    nn::Grid *grid = (nn::Grid*)network;
    
    map<nn::neuron_id, nn::Neuron*>* neurons = grid->neurons();
    
    mxArray *array = mxCreateDoubleMatrix(3*neurons->size(),1,mxREAL);
    double* doub_arr = mxGetPr(array);
    
    int st = 0;
    std::vector<double> vec;
    
    map<nn::neuron_id, nn::Neuron*>::const_iterator it = neurons->begin();
    for (int i = 0; i != neurons->size(); ++i, ++it) {

        //get real coordinates
        vec = grid->realCoordinates(it->first);
        
        //x
        doub_arr[st] = vec[0];
        
        //y
        doub_arr[st+1] = vec[1];
        
        //type
        doub_arr[st+2] = it->second->neuronType();
        
        st += 3;
        
        vec.clear();
    }
    
    engPutVariable((Engine*)_matlab_engine, "network", array);
    
    mxArray *save_mat = mxCreateLogicalScalar(saveToFile);
    
    engPutVariable((Engine*)_matlab_engine, "saveToFile", save_mat);
    
    char *path=NULL;
    size_t size = 0;
    path=getcwd(path,size);
    
    mxArray *mat_path = mxCreateString(string(path + string("/") + Settings::get()->outputFilePrefix()).c_str());
    engPutVariable((Engine*)_matlab_engine, "mat_path", mat_path);
    
    string saveIt = "save(strcat(mat_path, '_data.mat'));";
    
    engEvalString((Engine*)_matlab_engine, saveIt.c_str());
    
    string runner = "run('" + string(path) + "/data_hypergp/show_neural_network.m');";
    engEvalString((Engine*)_matlab_engine, runner.c_str());
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void io::MatlabHandler::showNeuralOutputs(void* outputs, std::string appendix) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    typedef map< double, map<nn::neuron_id, double> > neuron_data;
    neuron_data * data = (neuron_data*)outputs;
        
    size_t time_steps = data->size();
    size_t robots = data->begin()->second.size(); //number of output neurons
    
    mxArray *array = mxCreateDoubleMatrix(robots,time_steps,mxREAL);
    double* doub_arr = mxGetPr(array);
    
    //copy the data into the array
    map< double, map<nn::neuron_id, double> >::iterator it_out = data->begin();    
    map<nn::neuron_id, double>::iterator it_in;
    map<nn::neuron_id, double>* run_map = nullptr;
    
    long i,j;
    
    for (; it_out != data->end(); ++it_out) {
        
        i = distance(data->begin(), it_out);
        run_map = &it_out->second;
        
        for (it_in = run_map->begin(); it_in != run_map->end(); ++it_in) {
            
            j = distance(run_map->begin(), it_in);
            
//            cout << "Coords: x = " << i << " y = " << j << " value: " << it_in->second << endl;
            
            doub_arr[i*robots+j] = it_in->second;
        }
    }
    
    //put the variable in
    engPutVariable((Engine*)_matlab_engine, "outputs", array);
    
    mxArray *save_mat = mxCreateLogicalScalar(true);
    engPutVariable((Engine*)_matlab_engine, "saveToFile", save_mat);

    //draw the outputs
    char *path=NULL;
    size_t size = 0;
    path=getcwd(path,size);
    
    mxArray *mat_path = mxCreateString(string(path + string("/") + Settings::get()->outputFilePrefix() + appendix).c_str());
    engPutVariable((Engine*)_matlab_engine, "mat_path", mat_path);
        
    string runner = "run('" + string(path) + "/data_hypergp/show_neural_outputs.m');";
    engEvalString((Engine*)_matlab_engine, runner.c_str());
    
}














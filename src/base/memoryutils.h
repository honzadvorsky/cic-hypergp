/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  memoryutils.h
//  hyperGP
//
//  Created by Honza Dvorsky on 2/17/13.
//

#ifndef __hyperGP__memoryutils__
#define __hyperGP__memoryutils__

#include <memory>
#include <vector>

namespace std {
    /** Utility to create and allocate a type at the same time more easily. */
    template <typename T, typename... Args> std::unique_ptr<T> make_unique(Args&&... args);
    
    /** Utility to dynamically cast types in unique pointer. */
    template <class From, class To> std::unique_ptr<To> dyn_cast(std::unique_ptr<From>& from);    
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template<typename T, typename... Args> std::unique_ptr<T> std::make_unique(Args&&... args)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class From, class To>
std::unique_ptr<To> std::dyn_cast(std::unique_ptr<From>& from)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    To* _toPtr = dynamic_cast<To*>(from.release());
    if (!_toPtr) throw bad_cast();
    return std::unique_ptr<To>(_toPtr);
}


#endif /* defined(__hyperGP__memoryutils__) */

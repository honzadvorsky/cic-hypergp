/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_Population.h
//  hyperGP
//
//  Created by Honza Dvorsky on 2/2/13.
//

#ifndef __hyperGP__cic_Population__
#define __hyperGP__cic_Population__

#include <memory>

#include "cic_Individual.h"

namespace cic {
    class Population;
} //namespace cic

/** The abstract base class for population type. Takes care of adding, removing and iterating individuals. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::Population
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
protected:
    /** Marks the number of generation. */
    size_t _iteration;
public:
    /** Getter for the population's number of iteration. */
    size_t iteration() const { return _iteration; };
    
    /** Setter for the population's number of iteration. */
    void setIteration(size_t it) { _iteration = it; };
        
//    virtual std::string description() const = 0;
    Population() : _iteration(0) { };
    virtual ~Population() { };
};

#endif /* defined(__hyperGP__cic_Population__) */

/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_manualtests.cpp
//  hyperGP
//
//  Created by Honza Dvorsky on 2/24/13.
//

#include "cic_manualtests.h"
#include <vector>
#include "memoryutils.h"
#include "cic_tree_TreeGenerator.h"
#include "cic_Settings.h"

std::unique_ptr< cic::tree::Tree<double> > makeTree();
std::unique_ptr< cic::tree::Tree<double> > makeTree2();
void tryTreeSearch(std::shared_ptr< cic::tree::Tree<double> >);
void tryTreeGenerator();
void tryTreeGen2();
void tryPtc1();

template <class T> void testTreeFunctionality();

typedef double tree_type;

//----------------------------------------------------------------------------
void cic::runManualTests()
//----------------------------------------------------------------------------
{
    //tryPtc1();
    //tryTreeGenerator();
    
    
}


//----------------------------------------------------------------------------
void tryPtc1()
//----------------------------------------------------------------------------
{
    cic::tree::TreeGenerator* gen = cic::tree::TreeGenerator::get();
    
    /*
     PTC1
     With eSize == 6 -> avg depth == ~2
     With eSize == 20 -> avg depth == ~5
     */
    
    int eSize = 10;
    int maxPredicted = 35;
    int maxDepth = 40;
    int minDepth = 0;
    
    std::vector<int> occurSize(maxPredicted);
    std::vector<int> occurDepth(maxDepth+1);
    int gens = 1000;
    
    //----------------------------------------------------------------------------
    
    //generate probabilities for terminals
    std::vector<double> termPs(4);
    std::fill(termPs.begin(), termPs.end(), -1); //pre-fail the vector so that we know we assigned every node
    
    //generate probabilities for functions
    std::vector<double> funPs(cic::tree::FunType::_LAST_ITEM_TYPE);
    std::fill(funPs.begin(), funPs.end(), -1); //pre-fail the vector so that we know we assigned every node
    
    //prepare the probabilities
    {
//        termPs[0] = 1;
//        termPs[1] = 1;
//        termPs[2] = 1;
//        termPs[3] = 1;
//        
//        funPs[cic::tree::FunType::Abs]  = 1;
//        funPs[cic::tree::FunType::Diff] = 1;
//        funPs[cic::tree::FunType::Exp]  = 1;
//        funPs[cic::tree::FunType::Prod] = 1;
//        funPs[cic::tree::FunType::Sin]  = 1;
//        funPs[cic::tree::FunType::Sum]  = 1;
        
    }
    
    //ensure the right count of the probabilities in both terminals and functions
    for (std::vector<double>::const_iterator it = termPs.begin(); it != termPs.end(); ++it)
        if (*it < 0) throw std::logic_error("probability cannot be negative!");
    for (std::vector<double>::const_iterator it = funPs.begin(); it != funPs.end(); ++it)
        if (*it < 0) throw std::logic_error("probability cannot be negative!");
    
    
    //----------------------------------------------------------------------------
    
    //also compute the average size
    double avgsumSize = 0;
    double avgsumDepth = 0;
    int floatingOccurSize;
    int floatingOccurDepth;
    
    for (int i = 0; i < gens; ++i) {
        std::unique_ptr< cic::tree::Tree<tree_type> > tree;
        
        tree = gen->genTree(cic::tree::GenType::PTC1,
                            cic::tree::GenParam(maxDepth, minDepth, eSize),
                            cic::tree::GenFilter(minDepth),
                            cic::tree::GenProbs(termPs, funPs));
        floatingOccurSize = tree->nodeCount();
        floatingOccurDepth = tree->depth();
        if (floatingOccurSize < maxPredicted) occurSize[floatingOccurSize]++;
        if (floatingOccurDepth <= maxDepth) occurDepth[floatingOccurDepth]++;
        avgsumSize += floatingOccurSize;
        avgsumDepth += floatingOccurDepth;
        
    }
    
    std::cout << "********************" << std::endl;
    
    std::cout << "The average tree size is " << avgsumSize/gens << "." << std::endl;
    std::cout << "The average tree depth is " << avgsumDepth/gens << "." << std::endl;
    
    std::cout << "********************" << std::endl;
    
    for (int i = 0; i <= maxPredicted; ++i) {
        double frac = ((double)occurSize[i])/gens;
        std::cout << "Size: " << i << ":\t" << occurSize[i] << " = \t" << 100*frac << " \%" << std::endl;
    }
    
    std::cout << "********************" << std::endl;
    
    for (int i = 0; i <= maxDepth; ++i) {
        double frac = ((double)occurDepth[i])/gens;
        std::cout << "Depth: " << i << ":\t" << occurDepth[i] << " = \t" << 100*frac << " \%" << std::endl;
    }
}

//----------------------------------------------------------------------------
void tryTreeGenerator()
//----------------------------------------------------------------------------
{
    int maxDepth = 10;
    int minDepth = 0;
    int numOfTrees = 1000;
    cic::tree::TreeGenerator* gen = cic::tree::TreeGenerator::get();
    std::unique_ptr< std::vector< cic::tree::Tree<tree_type>* > > trees = std::make_unique<std::vector< cic::tree::Tree<tree_type>* >>();
    
    std::unique_ptr< cic::tree::Tree<tree_type> > tree;
    const clock_t begin_time = clock();
    std::vector<int> *occurDepth = new std::vector<int>(maxDepth+1);
    std::vector<int> *occurSize = new std::vector<int>(2*(maxDepth+1));
    
    for (int i = 0; i < numOfTrees; ++i) {
        tree = gen->genTree(cic::tree::GenType::GROW, cic::tree::GenParam(maxDepth, minDepth));
        (*occurDepth)[tree->depth()]++;
        (*occurSize)[tree->nodeCount()]++;
        trees->push_back(tree.release());
    }
    
    std::cout << "generated " << trees->size() << " trees" << std::endl;
    std::cout << "******************************" << std::endl;
    
    for (int i = 0; i <= maxDepth; ++i) {
        double frac = ((double)(*occurDepth)[i])/numOfTrees;
        std::cout << "Depth: " << i << ":\t" << (*occurDepth)[i] << " trees, which is \t" << 100*frac << " \%" << std::endl;
    }
    std::cout << "******************************" << std::endl;
    for (int i = 0; i <= 2*maxDepth; ++i) {
        double frac = ((double)(*occurSize)[i])/numOfTrees;
        std::cout << "Size: " << i << ":\t" << (*occurSize)[i] << " trees, which is \t" << 100*frac << " \%" << std::endl;
    }
    std::cout << "******************************" << std::endl;
    
    const clock_t total = float( clock () - begin_time ) /  CLOCKS_PER_SEC * 1000;
    std::cout << std::endl << "Generator description: " << std::endl << gen->description() << std::endl;
    std::cout << "generating took " << total << " millis." << std::endl;
    
    
    //    std::cout << (*trees)[numOfTrees-1]->description() << std::endl;
    
    
    //    std::cout << std::endl << "tree generated (max depth:" << maxDepth << ", real depth: "<< tree->depth() << ")" << std::endl
    //    << tree->description() << std::endl;
}

//----------------------------------------------------------------------------
template <class T> void testTreeFunctionality()
//----------------------------------------------------------------------------
{
    std::unique_ptr< cic::tree::Tree<T> > tree1 = makeTree();
    std::unique_ptr< cic::tree::Tree<T> > tree2 = makeTree2();
    
    std::cout << std::endl << "tree1 before:" << std::endl << tree1->description() << std::endl;
    std::cout << std::endl << "tree2 before:" << std::endl << tree2->description() << std::endl;
    
    cic::tree::NodeInfo info1 = cic::tree::NodeInfo(0.2,2);
    //    std::unique_ptr< cic::tree::Tree<T> > tree2 = tree1->detachSubtree(info1);
    
    std::unique_ptr< cic::tree::Tree<T> > tree3 = tree1->attachSubtree(info1, tree2.release());
    
    //    cic::tree::NodeInfo info2 = cic::tree::NodeInfo(0.1,2);
    
    //    tree1->swapSubtrees(info1, tree2.get(), info2);
    
    std::cout << std::endl << "tree1 after:" << std::endl << tree1->description() << std::endl;
    std::cout << std::endl << "tree3 after:" << std::endl << tree3->description() << std::endl;
    std::cout << "tree1 depth: " << tree1->depth() << ", tree1 node count: " << tree1->nodeCount() << std::endl;
    std::cout << "tree3 depth: " << tree3->depth() << ", tree3 node count: " << tree3->nodeCount() << std::endl;
}

//----------------------------------------------------------------------------
std::unique_ptr< cic::tree::Tree<tree_type> > makeTree()
//----------------------------------------------------------------------------
{
//    //quick testing can be done here
//    
//    using namespace cic;
//    
//    typedef tree::TermNode<tree_type> ter;
//    typedef tree::ConstTermNode<tree_type> con;
//    typedef tree::FunNode<tree_type> fun;
//    
//    std::unique_ptr< std::vector<tree_type> > terminalValues = std::make_unique< std::vector<tree_type> >();
//    terminalValues->push_back(10);
//    terminalValues->push_back(20);
//    terminalValues->push_back(30);
//    terminalValues->push_back(40);
//    
//    ter* t1 = new ter(0);
//    ter* t2 = new ter(2);
//    fun* f1 = new fun(tree::Sum, t1, t2);
//    
//    ter* t3 = new ter(1);
//    fun*f2 = new fun(tree::Diff, f1, t3);
//    
//    //    ter* t3 = new ter(3);
//    //    fun* f2 = new fun(tree::Diff, f1, t3);
//    
//    fun* root = f2;
//    
//    std::unique_ptr< cic::tree::Tree<tree_type> > myAutoTree = std::make_unique< cic::tree::Tree<tree_type> >(root);
//    //attach the terminal values vector
//    myAutoTree->setTermMappedValues(terminalValues.release());
//    
//    return myAutoTree;
    return nullptr;
}

//----------------------------------------------------------------------------
std::unique_ptr< cic::tree::Tree<tree_type> > makeTree2()
//----------------------------------------------------------------------------
{
    //quick testing can be done here
    
    using namespace cic;
    
    typedef tree::TermNode<tree_type> ter;
    typedef tree::ConstTermNode<tree_type> con;
    typedef tree::FunNode<tree_type> fun;
    
    std::unique_ptr< std::vector<tree_type> > terminalValues = std::make_unique< std::vector<tree_type> >();
    terminalValues->push_back(11);
    terminalValues->push_back(22);
    terminalValues->push_back(33);
    terminalValues->push_back(44);
    
    ter* t1 = new ter(3);
    ter* t2 = new ter(1);
    fun* f1 = new fun(tree::Prod, t2, t1);
    
    //    fun* f2 = new fun(tree::Exp, f1);
    //    ter* t20 = new ter(1);
    
    //    fun* f3 = new fun(cic::tree::Sum, f2, t20);
    
    fun* root = f1;
    
    std::unique_ptr< cic::tree::Tree<tree_type> > myAutoTree = std::make_unique< cic::tree::Tree<tree_type> >(root);
    //attach the terminal values vector
    myAutoTree->setTermMappedValues(terminalValues.release());
    
    return myAutoTree;
}

//----------------------------------------------------------------------------
void tryTreeSearch(std::shared_ptr< cic::tree::Tree<tree_type> > tree)
//----------------------------------------------------------------------------
{
    std::cout << std::endl << "Original tree:" << std::endl << tree->description() << std::endl;
    
    cic::tree::att_t search_id = 1.5;
    cic::tree::att_t search_depth = 1;
    
    //find a const subtree
    std::shared_ptr< cic::tree::ConstTree<tree_type> > foundTree = tree->findSubtree(cic::tree::NodeInfo(search_id, search_depth));
    std::cout << std::endl << "Found tree:" << std::endl << foundTree->description() << std::endl;
    
    //detach the found subtree
    std::shared_ptr< cic::tree::Tree<tree_type> > detached = tree->detachSubtree(foundTree);
    std::cout << std::endl << "Detached tree:" << std::endl << detached->description() << std::endl;
    
    //even switch the terminal map
    std::unique_ptr< std::vector<tree_type> > terminalValues = std::make_unique< std::vector<tree_type> >();
    terminalValues->push_back(10);
    terminalValues->push_back(20);
    terminalValues->push_back(30);
    terminalValues->push_back(40);
    tree->setTermMappedValues(terminalValues.release());
    
    //now print both trees
    std::cout << "Modified tree:" << std::endl << tree->description() << std::endl;
    
}


/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_Settings.h
//  hyperGP
//
//  Created by Honza Dvorsky on 3/3/13.
//

#ifndef __hyperGP__cic_Settings__
#define __hyperGP__cic_Settings__

#include <iostream>
#include <memory>
#include <exception>
#include <cmath>
#include <vector>


namespace cic {
    class Settings;
}

/** Robot type for experiments. */
struct RobotType {
    /** Robot type for experiments. */
    enum Type {
        Simple5 = 0,
        Complex7 = 1,
        Rect57 = 2,
        One = 3
    };
};

/** Global experiment settings. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::Settings
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{    
public:
    
    /** Settings for Genetic programming. */
    struct GP {
        
        /** Default fitness for unassigned inds. */
        double defaultFitness = 1;
        
        /** Population size. */
        int generatedPopulationSize = 10;
        
        /** Expected generated size of trees. */
        int expectedGeneratedTreeSize = 6;
        
        /** Maximum generated tree depth. */
        int maximumGeneratedTreeDepth = 6;
        
        /** Minimum generated tree depth. */
        int minimumGeneratedTreeDepth = 0;
        
        /** How many terminal variables we want to use. */
        int terminalVariableCount = 4;
        
        /** How many functions we have. */
        int functionCount = 5;
        
        /** Node function probabilities. */
        std::vector<double> functionProbabilities;
        
        /** Node terminals probabilities. */
        std::vector<double> terminalProbabilities;
        
        /** Tournament selection: tournament size. */
        int tournament_size = 5;
        
        /** Selection used for GP. 0: Tournament, 1: Roulette */
        int selection_type = 0;
        
        /** How many constants in nodes we use, other than terminal variables. */
        int terminalConstsCount = 2;
        
        /** If tournament selection is used, smaller inds are chosen if fitness is equal. */
        int tournamentSelectionUseLexicographicParsimonyPressure = 1;
        
        /** Number of trees in a genotype. */
        int numberOfTreesInForestGenotype = 1;
        
        /** Whether should check for duplicates and only return unique trees. */
        int preventDuplicates = 1;
    };
    
    /** Simulator settings. */
    struct Simulator {
        
        /** Custom function for the simulator point generation. */
        static double myfun(double input) {
            return std::sin(M_PI*input) + 10*std::sin(M_PI*input/5);
        };
        
        /** Tested function. */
        double (*function)(double) = myfun;
        
        /** Lower bound for sample points. */
        double sample_from = 0.0;
        
        /** Sample width. */
        double sample_step = 1.0;
        
        /** Upper bound for sample points. */
        double sample_to = 30.0;
                
        /** Delayed start - from when we measure the walked path. */
        double delayed_start = 2.0;
        
        /** Select the robot type -  */
        RobotType::Type robot_type = RobotType::Simple5;
        
        /** Enables to run the simulation n-times faster, changing inputs every n-steps. */
        int extraTimeStepDivider = 1;
        
        /** Should generate povray files? (For camera recording of the simulation) */
        int generatePovrayFiles = 0;
    };

    /** Control block settings. */
    struct Control {
        
        /** Maximum iterations of the experiment. */
        int maximumIterations = 10;
        
        /** Minimum good-enough fitness (to stop prematurely). */
        double minimumFitness = 20.0;
        
        /** Population file. */
        std::string populationXmlLoadFrom = "";
        
        /** Use Matlab plotting? Slows down execution. */
        int useMatlabPlot = 0;
        
        /** Whether make sure the experiment doesn't stop and wait for user input. */
        int isAutonomousExperiment = 0;
        
        /** If input population, should just show the best and quit? Or should it use as input? */
        int showAndQuitOnInputPopulation = 1;
    };
    
    /** Bloat Control settings */
    struct BloatControl {
        
        /** Use bloat control or not. 0 is false, else true. */
        int useBloatControl = 1;
        
        /** Bloat control: dynamic depth variant: simple (0), heavy (1), very heavy (2). */
        int bloatControlVariant = 1;
        
        //DONT SERIALIZE
        /** Current value of dynamic limit. Do not serialize. */
        int currentLimit = 0;
    };
    
    /** Neural network settings */
    struct Neural {
        
        /** The threshold, under which we ignore synopses. */
        double synopsisThreshold = 0.2;
        
        /** Use middle legs - if available. */
        int useMiddleLegs = 1;
        
        //DONT SERIALIZE
        /** Neuron activation function. */
        static double activation(double input) {
            //sigmoid?
            return std::tanh(input);
//            return input;
        };

        //DONT SERIALIZE
        /** Neuron activation function. */
        double (*activationFunction)(double) = activation;

        /** Average n consecutive samples (bc of high frequencies). 1 doesn't really do anything. */
        int sampleAverageCount = 1;
    };

    /** Genetic programming settings struct. */
    GP _gp;
    
    /** Simulator settings struct. */
    Simulator _simulator;
    
    /** Control block settings struct. */
    Control _control;
    
    /** Bloat Control settings struct. */
    BloatControl _bloat;
    
    /** Neural settings struct. */
    Neural _neural;
    
    /** Instance getter. */
    static Settings* get()
    {
         //for some reason having the variable inside the class, the linker couldn't link it :/
        if(!_setting_inst) _setting_inst = new Settings();
        return _setting_inst;
    };
    
    /** Instance setter. */
    static void set(Settings *sett)
    {
        if (_setting_inst != sett) {
            delete _setting_inst;
            _setting_inst = sett;
        }
    }  

    /** Saves settings to xml. */
#ifdef TARGET_SYMREG
    void saveSettings(std::string filename = "symreg_settings.xml")
#elif TARGET_HYPERGP
    void saveSettings(std::string filename = "hypergp_settings.xml")
#endif
    {
        this->saveMe(filename);
    }
    
    /** Loads settings from xml. */
#ifdef TARGET_HYPERGP
    bool loadSettings(std::string filename = "hypergp_settings.xml")
#elif TARGET_SYMREG
    bool loadSettings(std::string filename = "symreg_settings.xml")
#else
    bool loadSettings(std::string filename)
#endif
    {
        bool b = this->loadMe(filename);
        if (b) {
            this->init();
        }
        return b;
    }
    
    /** Returns the file prefix, including the path, for current experiement's output files. */
    std::string outputFilePrefix();
    
    /** Resets the vectors after a size change. */
    void resetVectors();

private:
    static Settings* _setting_inst;
    
    std::string* _filePrefix;
    
    bool loadMe(std::string filename);
    void saveMe(std::string filename);
    
    void init();
    Settings();
    virtual ~Settings() { delete _filePrefix; };
};



#endif /* defined(__hyperGP__cic_Settings__) */

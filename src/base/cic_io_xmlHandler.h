/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_io_xmlHandler.h
//  hyperGP
//
//  Created by Honza Dvorsky on 3/1/13.
//

#ifndef __hyperGP__cic_io_xmlHandler__
#define __hyperGP__cic_io_xmlHandler__

#include <iostream>

#include <libxml/parser.h>
#include <libxml/tree.h>

#include "cic_genetic_Population.h"

namespace cic {
    namespace io {
        class xmlHandler;
        class xmlAccess;
    }
}

/** XML handler takes care of I/O operations. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::io::xmlHandler
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    
public:
    
    xmlHandler() { };
    virtual ~xmlHandler() { };
        
    /** Save a population to xml. Returns the name of the file. */
    std::string savePopulationToXml(const Population* pop) const;
    
    /** Create a population from xml, providing a xml file. */
    std::shared_ptr<Population> loadPopulationFromXml(const char* filename) const;

    /** Saves Settings to xml file. */
    void saveSettingsToXml(std::string filename) const;

    /** Loads Settings from xml file. */
    bool loadSettingsFromXml(std::string filename) const;

};

#endif /* defined(__hyperGP__cic_io_xmlHandler__) */

/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_tree_nodeFunx.h
//  hyperGP
//
//  Created by Honza Dvorsky on 2/9/13.
//

#ifndef __hyperGP__cic_tree_nodeFunx__
#define __hyperGP__cic_tree_nodeFunx__

#include <vector>
#include <exception>
#include <cmath>
#include <map>
#include <string>
#include <iostream>

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
namespace cic {
    namespace tree {
        enum FunType {
            Sum,
            Diff,
            Prod,
            Divide,
            Sin,
            Cos,
            Log,
            Exp,
            Abs,
            Atan,
            Gauss,
            //add more, but make sure there's no way the operation can crash.
            _LAST_ITEM_TYPE
        };
        
        /** Evaluates the function and returns the result. */
        template <class T> T funEval(FunType funType, std::vector<T>& args);
        
        /** Returns the arity of the function. */
        int funArity(FunType funType);
        
        /** Returns the string representation of the function. */
        std::string funString(FunType funType);
        
        /** Parse FunType from a string. */
        inline FunType funTypeFromString(const std::string s) {
            for (tree::FunType i = (tree::FunType)0; i < tree::FunType::_LAST_ITEM_TYPE; ++i) {
                if (s == tree::funString(i)) {
                    return i;
                }
            }
            return tree::FunType::_LAST_ITEM_TYPE;
        };
        
        /** Returns the identity value for a function.
         Identity value for product is 1, since a*1=a. For summation and difference it's 0, since a+0=a, a-0=a. For exp it's 1, since e^1=e.
         ConstNodes with identity values are put in a tree as a patch after subtrees are detached and taken out. This way, taking out a subtree
         doesn't completely break the result value of the tree. Discussion over identity values is allowed, since I haven't strongly defined them
         in the first place. */
        template <class T> T funIdentityValue(FunType funType);
    };
};
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> T cic::tree::funEval(cic::tree::FunType funType, std::vector<T>& arg)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //first check that arity of the function and the number of arguments is equal
    if (funArity(funType) != arg.size()) {
        throw std::length_error("bad number of arguments!");
    }
    
    //everything's fine, evaluate the function and return the value
    switch (funType) {
        case Sum:
            return arg[0]+arg[1];
            break;
        case Diff:
            return arg[0]-arg[1];
            break;
        case Prod:
            return arg[0]*arg[1];
            break;
        case Abs:
            return std::abs(arg[0]);
            break;
        case Divide:
            if (arg[1] != 0) {
                return arg[0]/arg[1];
            }
            return 1;
            break;
        case Sin:
        {
            return std::sin(arg[0]);
        }
            break;
        case Cos:
            return std::cos(arg[0]);
            break;
        case Log:
            if (arg[0] != 0) {
                return std::log(std::abs(arg[0]));
            }
            return 1;
            break;
        case Exp:
            return std::exp(arg[0]);
            break;
        case Atan:
            return std::atan(arg[0]);
            break;
        case Gauss:
            return std::exp(-std::pow(arg[0], 2));
            break;
        default:
            throw std::invalid_argument("unrecognized node function!");
            break;
    }
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> T cic::tree::funIdentityValue(cic::tree::FunType funType)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //if weighing between 0 or 1, just choose 1. that doesn't remove as much information as 0 does.
    switch (funType) {
        case Sum:
        case Diff:
        case Abs:
        case Sin:
        case Cos:
            return 0;
            break;
            
        case Prod:
        case Divide:
        case Exp:
        case Log:
            return 1;
            break;
            
        default:
            throw std::invalid_argument("unrecognized node function!");
            break;
    }
}

#endif /* defined(__hyperGP__cic_tree_nodeFunx__) */

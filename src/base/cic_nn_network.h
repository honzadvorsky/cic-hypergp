/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_nn_Network.h
//  hyperGP
//
//  Created by Honza Dvorsky on 3/12/13.
//

#ifndef __hyperGP__cic_nn_Network__
#define __hyperGP__cic_nn_Network__

#include <iostream>
#include <vector>
#include "cic_Settings.h"
#include "cic_nn_Grid.h"

namespace cic {
    namespace nn {
        class Network;
    }
}

/** Base class for all neural networks. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::nn::Network
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    Grid* _grid;

public:
    /** Constructor of the Network (with the size as input). */
    Network(size_t gridSideX, size_t gridSideY);
    virtual ~Network() { delete _grid; };
    
    /** Calls matlab to show the network topology. */
    virtual void showMatlabFig();
    
    /** Computes output data for all output neurons for the whole simulation time. */
    virtual std::map<double, std::map<nn::neuron_id, double> >  computeOutputData(int avgOverSamples = 1) const;

    /** Print string description. */
    virtual std::string description() const { return _grid->description(); };
    
    /** Adds the source which is used to pull out weights. */
    virtual void setWeightSource(void* source) { _grid->setWeightSource(source); };
    
};


#endif /* defined(__hyperGP__cic_nn_Network__) */














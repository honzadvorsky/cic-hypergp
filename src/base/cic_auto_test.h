/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_auto_test.h
//  hyperGP
//
//  Created by Honza Dvorsky on 2/9/13.
//

#ifndef __hyperGP__cic_auto_test__
#define __hyperGP__cic_auto_test__

#include <iostream>
#include "cic_tree_nodeFunx.h"
#include "cic_tree_node.h"

//these methods are run at the beginning of each run

//----------------------------------------------------------------------------------------------------
//------------------------------------------ Public --------------------------------------------------
//----------------------------------------------------------------------------------------------------

namespace cic {
    template <class T> void auto_test();
} //namespace cic

void printError(std::string);

//----------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------

// **** 1 **** FIRST DECLARE YOUR TEST FUNCTION HERE ( then see cic::auto_test() )
template <class T> void test_cic_tree_nodeFunx(bool verbose);
template <class T> void test_cic_tree_node(bool verbose);

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> void cic::auto_test()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    typedef void (*FunForTest)(bool);
    std::vector<FunForTest> functions;
    std::vector<bool> verbose;
    void (*myfun)(bool);
    
    //--------------------------------------------------------------------------
    // **** 2 **** INSERT FUNCTION NAMES HERE       INSERT VERBOSE MODES HERE ( then define the function at the end of this file )
    //--------------------------------------------------------------------------
    functions.push_back(test_cic_tree_nodeFunx<T>);    verbose.push_back(false);
    functions.push_back(test_cic_tree_node<T>);        verbose.push_back(false);
    
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    
    std::cout << "------------ TESTING: START ------------" << std::endl;
    
    for (int i = 0; i != functions.size(); ++i) {
        try {
            myfun = functions[i];
            (*myfun)(verbose[i]);
        } catch (std::invalid_argument e) {
            printError(e.what());
            return;
        } catch (std::length_error e) {
            printError(e.what());
            return;
        }
    }
    
    std::cout << "------------ TESTING: END ------------" << std::endl;
    std::cout << "-- Everything tested seems to be OK --" << std::endl;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> void test_cic_tree_nodeFunx(bool verbose)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    std::cout << "Testing ** cic_tree_nodeFunx.h ** ";
    if (verbose) std::cout << std::endl;
    else std::cout << " Progress ";
    
    if (verbose) std::cout << "Testing if all items in enums are recognized by functions" << std::endl;
    
    for (cic::tree::FunType i = (cic::tree::FunType)0; i < cic::tree::_LAST_ITEM_TYPE; ++i) {
        
        if (!verbose) std::cout << ".";
        
        std::string funStr = cic::tree::funString(i);
        
        if (verbose) std::cout << "Testing function: " << funStr << std::endl;
        if (verbose) std::cout << "Identity value: " << cic::tree::funIdentityValue<T>(i) << std::endl;
        
        int arity = cic::tree::funArity(i);
        
        std::vector<double> vec = std::vector<double>();
        double ar;
        if (verbose) std::cout << "Input:";
        for (int u = 0; u < arity; ++u) {
            ar = (-1 + 2*(u % 2)) * (u+1);
            //            ar = rand() % 100;
            
            if (verbose) std::cout << " " << ar;
            vec.push_back(ar);
        }
        if (verbose) std::cout << std::endl;
        
        double res = cic::tree::funEval(i, vec);
        if (verbose) std::cout << funStr << " provided output: " << res << std::endl;
        if (verbose) std::cout << std::endl;
        if (verbose) std::cout << "***************** " << funStr << " OK" << " *****************" << std::endl;
        if (verbose) std::cout << std::endl;
    }
    
    if (!verbose) std::cout << " OK!" << std::endl;
    else std::cout << "Testing ** cic_tree_nodeFunx.h ** SUCCESSFUL " << std::endl;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> void test_cic_tree_node(bool verbose)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    std::cout << "Testing ** cic_tree_node.h ** ";
    if (verbose) std::cout << std::endl;
    else std::cout << " Progress ";
    
    if (verbose) std::cout << "Testing if all items in enums are recognized by attribute" << std::endl;
    
    typedef cic::tree::Attribute att_type;
    
    for (att_type i = (att_type)0; i < att_type::_LAST_ITEM_ATT; ++i) {
        
        if (!verbose) std::cout << ".";
        
        std::string attStr = cic::tree::attString(i);
        
        if (attStr == "") throw std::invalid_argument("unrecognized attribute type!");
        
        if (verbose) std::cout << "Testing function: " << attStr << std::endl;
        if (verbose) std::cout << "***************** " << attStr << " OK" << " *****************" << std::endl;
    }
    
    if (!verbose) std::cout << " OK!" << std::endl;
    else std::cout << "Testing ** cic_tree_node.h ** SUCCESSFUL " << std::endl;
}

// **** 3 **** DEFINE YOUR TESTING FUNCTION ABOVE THIS COMMENT! :-) And that's it.


#endif /* defined(__hyperGP__cic_auto_test__) */

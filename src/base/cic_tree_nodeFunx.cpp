/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_tree_nodeFunx.cpp
//  hyperGP
//
//  Created by Honza Dvorsky on 2/9/13.
//

#include "cic_tree_nodeFunx.h"

using namespace cic;
using namespace std;

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
int tree::funArity(tree::FunType funType)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    switch (funType) {
        case Sum:
        case Diff:
        case Prod:
        case Divide:
            return 2;
            break;
            
        case Abs:
        case Exp:
        case Log:
        case Sin:
        case Cos:
        case Atan:
        case Gauss:
            return 1;
            break;
            
        default:
            throw std::invalid_argument("unrecognized node function!");
            break;
    }
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
string tree::funString(tree::FunType funType)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    switch (funType) {
        case Sum:
            return "Sum";
            break;
        case Diff:
            return "Diff";
            break;
        case Prod:
            return "Prod";
            break;
        case Divide:
            return "Divide";
            break;
        case Abs:
            return "Abs";
            break;
        case Exp:
            return "Exp";
            break;
        case Log:
            return "Log";
            break;
        case Sin:
            return "Sin";
            break;
        case Cos:
            return "Cos";
            break;
        case Atan:
            return "Atan";
            break;
        case Gauss:
            return "Gauss";
            break;
        default:
            throw std::invalid_argument("unrecognized node function!");
            break;
    }
}

////--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
//tree::FunType tree::funTypeFromString(const string& s)
////--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
//{
//    
//    for (tree::FunType i = (tree::FunType)0; i < tree::FunType::_LAST_ITEM_TYPE; ++i) {
//        if (s == tree::funString(i)) {
//            return i;
//        }
//    }
//    return tree::FunType::_LAST_ITEM_TYPE;
//}









/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_nn_Neuron.cpp
//  hyperGP
//
//  Created by Honza Dvorsky on 3/12/13.
//

#include "cic_nn_Neuron.h"
#include "cic_Settings.h"
#include <numeric>
#include "cic_stringutils.h"

using namespace std;
using namespace cic;

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
nn::Neuron::Neuron()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    _inputs = new vector<const Synapsis*>(); /* don't forget bias! */
    _outputHistory = new map<double, double>();
    _const_value = __DBL_MAX__;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
nn::Neuron::~Neuron()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    delete _outputHistory;
    
    for (vector<const Synapsis*>::iterator it = _inputs->begin(); it != _inputs->end(); ++it) {
        delete (*it);
    }
    delete _inputs;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
double nn::Neuron::outputValueAtTime(double time, double currentTime)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //check that we set the neuron type
    if (this->neuronType() == neuron_type::Unknown) {
        throw logic_error("this neuron doesn't seem to be initialized correctly.");
    }
    
    //if Bias or Input, just return the constant value. make sure it's not the default one.
    if (this->neuronType() == neuron_type::Bias || this->neuronType() == neuron_type::Input) {
        
        if (this->constValue() == __DBL_MAX__) {
            throw logic_error("this neuron doesn't have its constant set.");
        }
        
        if (_inputs->size() != 0) {
            throw logic_error("input and bias neurons can't have inputs!");
        }
        
//#warning INPUT NEURONS FUNCTION
//        if (this->neuronType() == neuron_type::Input) {
//            //throw in a sin function instead of a constant
//            
//            double amp = 1.0;
//            double phase = 0.0;
//            double omega = 5.0;
//            
//            return amp*std::sin(omega*time + phase);
//        }
        
        return this->constValue();
    }
    
    //ok, it's Output or Hidden, we need to get or compute the value
    
    //see if output for this time has been calculated yet
    map<double, double>::const_iterator it = _outputHistory->find(time);
    double out;
    
    if (it == _outputHistory->end()) {
        
        //if we're not computing for the current time, just return 0
        //for cases such as the first computation etc.
        if (time != currentTime) {
            return 0;
        }
        
        //didn't find it, we have to compute the output first
        double sum = 0;
        double signal, weight;
        
        for (vector<const Synapsis*>::const_iterator it = _inputs->begin(); it != _inputs->end(); ++it) {
            signal = (*it)->sourceNeuron()->outputValueAtTime(time + (*it)->timeOffset(), currentTime);
            weight = (*it)->weight();
            sum += signal*weight;
        }
        
        //run them through the activation function
        
//        cout << sum << endl;
        
//#warning divided sum!
//        out = (*Settings::get()->_neural.activationFunction)(sum/10);

        out = (*Settings::get()->_neural.activationFunction)(sum);
        
        //save to history
        _outputHistory->insert(pair<double, double>(time, out));
        
    } else {
        //we got it, just return
        out = it->second;
    }
    
    return out;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void nn::Neuron::addInputSynapsis(nn::Neuron* sourceNeuron, double weight, double timeOffset)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    _inputs->push_back(new Synapsis(sourceNeuron, weight, timeOffset));
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
std::string nn::Neuron::description() const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    string weights;
    
    for (auto it = _inputs->begin(); it != _inputs->end(); ++it) {
        weights += cic::to_string("%f ", (*it)->weight());
    }
    
    return "[Neuron " + neuronTypeToString(_neuron_type) + " with " + cic::to_string("%i", _inputs->size()) + " inputs" + "]"
    + "\n" + weights;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
std::string nn::neuronTypeToString(nn::neuron_type type)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    switch (type) {
        case nn::neuron_type::Unknown:
            return "Unknown";
            break;
        case nn::neuron_type::Bias:
            return "Bias";
            break;
        case nn::neuron_type::Input:
            return "Input";
            break;
        case nn::neuron_type::Hidden:
            return "Hidden";
            break;
        case nn::neuron_type::Output:
            return "Output";
            break;
            
        default:
            throw logic_error("unrecognized neuron type");
            break;
    }
}




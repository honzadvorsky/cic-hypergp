/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_tree_TreeGenerator.h
//  hyperGP
//
//  Created by Honza Dvorsky on 2/21/13.
//

#ifndef __hyperGP__cic_tree_TreeGenerator__
#define __hyperGP__cic_tree_TreeGenerator__

#include <iostream>
#include <memory>
#include <vector>
#include "memoryutils.h"
#include "cic_tree.h"
#include "cic_Settings.h"

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
namespace cic {
    namespace tree {
        class TreeGenerator;
                
        typedef double tree_type;
        
        enum GenType {
            FULL = 0,
            GROW = 1,
            RAMPED_HALF_AND_HALF = 2,
            PTC1 = 3,
            PTC2 = 4
        };
        
        /** GenParam helps pass the parameter of the generator. */
        struct GenParam {
            /** The maximum depth a generater tree can have. */
            int maxDepth;
            /** The minimum depth a generater tree can have. */
            int minDepth;
            /** The expected size (number of nodes) of the tree - average. */
            int expSize;
            /** The maximum size a generated tree can have. */
            int maxSize;
            /** Constructor for all params, expected size defaults to 0, max size to inf. */
            GenParam(int mxDepth, int mnDepth, int eSize = 0, int mSize = INT_MAX) : maxDepth(mxDepth), minDepth(mnDepth), expSize(eSize), maxSize(mSize) { };
        };
        
        /** GenFilter helps filter out trees returned from generator. */
        struct GenFilter {
            /** Tells the generator to only return trees with equal or higher depth than minDepth. */
            int minDepth;
            /** Constructor with all filters. MinDepth defaults to 0. */
            GenFilter(int miDepth = 0) : minDepth(miDepth) { };
        };
        
        /** GenProbs passes the apriori probabilities of nodes to the generator. */
        struct GenProbs {
            /** Vector of terminal nodes' probabilities. */
            std::vector<double> termProbs;
            /** Vector of function nodes' probabilities. */
            std::vector<double> funProbs;
            GenProbs() { };
            /** Constructor to pass in both vectors with node probabilities. */
            GenProbs(std::vector<double> tProbs, std::vector<double> fProbs) : termProbs(tProbs), funProbs(fProbs) { };
        };
        
    }
    

}
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**

/** Engine for generating random trees with certain properties. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::tree::TreeGenerator
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    static TreeGenerator* _treeGen_inst;
    
    std::vector< cic::tree::ExpressionNode<tree_type>* >*   _termNodes;
    std::vector< cic::tree::ExpressionNode<tree_type>* >*   _funNodes;
    std::vector< cic::tree::ExpressionNode<tree_type>* >*   _allNodes;
    
    std::vector< int >*                                     _allNodesStatistics;
    
    void                                                    initNodes();
    
    cic::tree::ExpressionNode<tree_type>*                   randomNode(const std::vector< cic::tree::ExpressionNode<tree_type>* >* availableNodes,
                                                                       const std::vector<double>& probsNodes) const;
    
    //generation algorithms
    cic::tree::ExpressionNode<tree_type>*                   grow(int depth, int minDepth, int maxDepth) const;
    cic::tree::ExpressionNode<tree_type>*                   full(int depth, int maxDepth) const;
    cic::tree::ExpressionNode<tree_type>*                   ptc1(int depth, int maxDepth, double nontermP,
                                                                 const std::vector<double>& termPs,
                                                                 const std::vector<double>& funPs,
                                                                 const std::vector<double>& allPs) const;
    
public:
    /** 
     Generates random tree with respect to the input algorithm type.
     You can filter that you only want e.g. tree with a minimum depth of X. then trees are going to get generated until
     one is found that passes the filter. Warning: can take longer time with large X!
     
     GROW: tree with depth <= maximum depth, unbalanced tree
     FULL: depth == maximum depth, balanced tree
     PTC1: specify maxDepth, expected size (how closely is respected depends on large enough maxDepth), terminal and function probabilities
     PTC2: - not yet implemented
     */
    std::unique_ptr< cic::tree::Tree<tree_type> >           genTree(GenType algType,
                                                                    GenParam params,
                                                                    GenFilter filter = GenFilter(),
                                                                    GenProbs probs = GenProbs()) const;
    
    /** Return a string representation. */
    std::string description() const;
    
    /** Returns a random NodeInfo that can be (is safe to) used on the tree. */
    NodeInfo randomNodeInfoForTree(const tree::BaseTree<tree_type>* tree) const;

    /** Returns the one instance. */
    static TreeGenerator* get() { if (!_treeGen_inst) _treeGen_inst = new TreeGenerator(); return _treeGen_inst; };
    
    /** Prints the statistics of the tree generator (which node was chosen how many times). */
    void printStatistics() const;
    
private:
    TreeGenerator();
    virtual ~TreeGenerator();
};

#endif /* defined(__hyperGP__cic_tree_TreeGenerator__) */











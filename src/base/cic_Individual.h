/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_Individual.h
//  hyperGP
//
//  Created by Honza Dvorsky on 2/2/13.
//

#ifndef __hyperGP__cic_Individual__
#define __hyperGP__cic_Individual__

#include <string>

namespace cic {
    class Individual;
} //namespace cic

/**
 All individual objects should be subclasses of this abstract superclass.
 all methods should be virtual and = 0
*/
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::Individual
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
public:
    virtual ~Individual() { };
    
    /** Get a deep copy of the individual. */
    virtual cic::Individual* clone() = 0;
    
    /** String description. */
    virtual std::string description() = 0;
};

#endif /* defined(__hyperGP__cic_Individual__) */

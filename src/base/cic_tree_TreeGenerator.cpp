/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_tree_TreeGenerator.cpp
//  hyperGP
//
//  Created by Honza Dvorsky on 2/21/13.
//

#include "cic_tree_TreeGenerator.h"
#include <functional>
#include <numeric>
#include <random>
#include "cic_algorithm.h"

using namespace std;
using namespace cic;
using namespace cic::tree;

TreeGenerator* TreeGenerator::_treeGen_inst;

#pragma mark - Lifetime
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
TreeGenerator::TreeGenerator()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    initNodes();
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void TreeGenerator::printStatistics() const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{    
    cout << "------ Tree generator stats -------" << endl;
    for (int i = 0; i < _allNodesStatistics->size(); ++i) {
        cout << "Number: " << i << " - " << (*_allNodesStatistics)[i] << " times" << endl;
    }
    
    cout << "-----------------------------------" << endl;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
TreeGenerator::~TreeGenerator()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //print the statistics before destruction
    this->printStatistics();
    
    alg::deleteObjects(_termNodes);
    alg::deleteObjects(_funNodes);
    
    delete _allNodes;
    delete _allNodesStatistics;
    delete _termNodes;
    delete _funNodes;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void TreeGenerator::initNodes()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    _termNodes = new vector< ExpressionNode<tree_type>* >();
    _funNodes = new vector< ExpressionNode<tree_type>* >();
    _allNodes = new vector< ExpressionNode<tree_type>* >();
        
    //TODO:load from file or something
    //first create constants for the terminals
    
    //put in predefined ones
    _termNodes->push_back(new ConstTermNode<tree_type>(1));
    _termNodes->push_back(new ConstTermNode<tree_type>(-1));
//    _termNodes->push_back(new ConstTermNode<tree_type>(M_PI));
//    _termNodes->push_back(new ConstTermNode<tree_type>(0));
    
    //generate number of random ones in a range
    int numberOfRandomConsts = Settings::get()->_gp.terminalConstsCount - (int)_termNodes->size();
    double rand_range_down = -1;
    double rand_range_up = 1;
    
    cout << "Tree Generator: " << "Const nodes used: ";
    for (vector< ExpressionNode<tree_type>* >::const_iterator it = _termNodes->begin(); it != _termNodes->end(); ++it) {
        cout << (*it)->value();
        if (it != _termNodes->end()-1) {
            cout << ", ";
        }
    }
    cout << endl;
    
    cout << "Tree Generator: " << "another " << numberOfRandomConsts
    << " const nodes will be generated from [" << rand_range_down << ", " << rand_range_up << "]" << endl;
    cout << "Tree Generator: " << "those are: ";
    
    random_device r;
    std::uniform_real_distribution<> dist(rand_range_down,rand_range_up);
    for (int i = 0; i < numberOfRandomConsts; ++i) {
        _termNodes->push_back(new ConstTermNode<tree_type>(dist.operator()(r)));
        cout << (*_termNodes->rbegin())->value();
        if (i < numberOfRandomConsts-1) {
            cout << ", ";
        }
    }
    cout << endl;
    
    //write how many const nodes we're using
    Settings::get()->_gp.terminalConstsCount = (int)_termNodes->size();
    
    //this generates 'variables'
    for (int i = 0; i < Settings::get()->_gp.terminalVariableCount; ++i) _termNodes->push_back(new TermNode<tree_type>(i));
    if (_termNodes->size() == 0) throw std::logic_error("you need at least one terminal node!");
    
    //fill all available functions
//    for (int i = 0; i < FunType::_LAST_ITEM_TYPE; ++i) _funNodes->push_back(new FunNode<tree_type>((FunType)i));
    
    _funNodes->push_back(new FunNode<tree_type>(FunType::Sum));
    _funNodes->push_back(new FunNode<tree_type>(FunType::Prod));
    _funNodes->push_back(new FunNode<tree_type>(FunType::Atan));
    _funNodes->push_back(new FunNode<tree_type>(FunType::Gauss));
    _funNodes->push_back(new FunNode<tree_type>(FunType::Sin));
    
    Settings::get()->_gp.functionCount = (int)_funNodes->size();
    Settings::get()->resetVectors();
    
    if (_funNodes->size() == 0) throw std::logic_error("you need at least one function node!");
    
    //create allnodes
    _allNodes->insert(_allNodes->end(), _termNodes->begin(), _termNodes->end());
    _allNodes->insert(_allNodes->end(), _funNodes->begin(), _funNodes->end());
    
    _allNodesStatistics = new vector<int>(_allNodes->size());
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
string TreeGenerator::description() const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    string desc = "";
    
    //describe terminals
    typedef std::vector< ExpressionNode<tree_type>* > Vec;
    TermNode<tree_type>* terminal = nullptr;
    FunNode<tree_type>* function = nullptr;
    
    desc += "TermNodes:(";
    for (Vec::const_iterator it = _termNodes->begin(); it != _termNodes->end(); ++it) {
        terminal = dynamic_cast< TermNode<tree_type>* >(*it);
        desc += cic::to_string("%0.0f", terminal->value()) + ", ";
    }
    desc += "),";
    desc += "FunNodes:(";
    for (Vec::const_iterator it = _funNodes->begin(); it != _funNodes->end(); ++it) {
        function = dynamic_cast< FunNode<tree_type>* >(*it);
        desc += funString(function->funType()) + ", ";
    }
    desc += ")";
    
    return desc;
}

#pragma mark - Utilities

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
NodeInfo TreeGenerator::randomNodeInfoForTree(const BaseTree<tree_type>* tree) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //horizontally, IDs are between 0 and 2
    //vertically, depths are between 0 and tree->depth()
    
    random_device r;
    std::uniform_real_distribution<> dist(0,2);
    double nd_id = dist.operator()(r);
    
    //needs to be depth+1 because the right edge of the interval is never chosen
    int nd_depth = alg::randomInteger(alg::equalProbs(tree->depth()+1));
    
//    double frac = ((double)nd_depth)/(tree->depth()+1);
//    cout << "Random depth: " << nd_depth << " / " << tree->depth()+1 << " = " << frac << ", id: " << nd_id << " / [0,2]" << endl;
    
    NodeInfo n(nd_id, nd_depth);
    return n;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
ExpressionNode<tree_type>* TreeGenerator::randomNode(const vector< ExpressionNode<tree_type>* >* availNodes,
                                                     const vector<double>& probsNodes) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    if (availNodes->size() != probsNodes.size()) throw logic_error("each node needs to have a real probability assigned!");
    
    int randInt = alg::randomInteger(probsNodes);
    int randIntAll = randInt + (int)(availNodes == _funNodes ? _termNodes->size() : 0);
    (*_allNodesStatistics)[randIntAll]++;
    
    ExpressionNode<tree_type> *node2copy = availNodes->at(randInt);
    return node2copy->clone();
}

#pragma mark - Private algorithm implementation
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
ExpressionNode<tree_type>* TreeGenerator::grow(int depth, int minDepth, int maxDepth) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    if (depth == maxDepth) {
        //return a random terminal
        return this->randomNode(_termNodes, alg::equalProbs((int)_termNodes->size()));
    }
    
    vector< cic::tree::ExpressionNode<tree_type>* >* selectedNodes = nullptr;
    
    if (depth < minDepth) {
        selectedNodes = _funNodes;
    } else {
        selectedNodes = _allNodes;
    }
    
    //get a random node from all nodes
    ExpressionNode<tree_type>* node = this->randomNode(selectedNodes, alg::equalProbs((int)selectedNodes->size()));
    
    //find out whether it's a terminal
    TermNode<tree_type>* term = dynamic_cast< TermNode<tree_type>* >(node);
    if (term) {
        //it's a terminal, just return
        return term;
    }
    
    //it's a function, fill all children (depending on arity)
    FunNode<tree_type>* fun = dynamic_cast< FunNode<tree_type>* >(node);
    if (!fun) throw logic_error("unrecognized type of node");
    
    switch (funArity(fun->funType())) {
        case 1:
            fun->setChildOnSide(this->grow(depth+1, minDepth, maxDepth), ChildSide::LEFT);
            break;
        case 2:
            fun->setChildOnSide(this->grow(depth+1, minDepth, maxDepth), ChildSide::LEFT);
            fun->setChildOnSide(this->grow(depth+1, minDepth, maxDepth), ChildSide::RIGHT);
            break;
        default:
            throw std::logic_error("incorrect function arity!");
            break;
    }
    
    //return the constructed node with children
    return fun;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
ExpressionNode<tree_type>* TreeGenerator::full(int depth, int maxDepth) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    if (depth == maxDepth) {
        //return a random terminal
        return this->randomNode(_termNodes, alg::equalProbs((int)_termNodes->size()));
    }
    
    //fill all children (depending on arity) with a random function node
    FunNode<tree_type>* fun = dynamic_cast< FunNode<tree_type>* >(this->randomNode(_funNodes, alg::equalProbs((int)_funNodes->size())));
    if (!fun) throw logic_error("incorrect nodes are offered in tree generator! must be a function node!");
    
    switch (funArity(fun->funType())) {
        case 1:
            fun->setChildOnSide(this->full(depth+1, maxDepth), ChildSide::LEFT);
            break;
        case 2:
            fun->setChildOnSide(this->full(depth+1, maxDepth), ChildSide::LEFT);
            fun->setChildOnSide(this->full(depth+1, maxDepth), ChildSide::RIGHT);
            break;
        default:
            throw std::logic_error("incorrect function arity!");
            break;
    }
    
    //return the constructed node with children
    return fun;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
ExpressionNode<tree_type>* TreeGenerator::ptc1(int depth, int maxDepth, double nontermP,
                                                          const std::vector<double>& termPs,
                                                          const std::vector<double>& funPs,
                                                          const std::vector<double>& allPs) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    if (depth == maxDepth) {
        //return a terminal
        return this->randomNode(_termNodes, termPs);
    }
    
    std::uniform_real_distribution<> dist;
    random_device r;
    double nontermThreshold = dist.operator()(r);
    
    if (nontermThreshold < nontermP) {
        //return a function
        FunNode<tree_type>* fun = dynamic_cast< FunNode<tree_type>* >(this->randomNode(_funNodes, funPs));
        if (!fun) throw logic_error("incorrect nodes are offered in tree generator! must be a function node!");
        
        switch (funArity(fun->funType())) {
            case 1:
                fun->setChildOnSide(this->ptc1(depth+1, maxDepth, nontermP, termPs, funPs, allPs), ChildSide::LEFT);
                break;
            case 2:
                fun->setChildOnSide(this->ptc1(depth+1, maxDepth, nontermP, termPs, funPs, allPs), ChildSide::LEFT);
                fun->setChildOnSide(this->ptc1(depth+1, maxDepth, nontermP, termPs, funPs, allPs), ChildSide::RIGHT);
                break;
            default:
                throw std::logic_error("incorrect function arity!");
                break;
        }
        return fun;
        
    } else {
        //return a terminal
        return this->randomNode(_termNodes, termPs);
    }
}

#pragma mark - Public methods
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
unique_ptr< Tree<tree_type> > TreeGenerator::genTree(GenType algType, GenParam params, GenFilter filter, GenProbs probs) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    unique_ptr< Tree<tree_type> > res = nullptr;
    
    switch (algType) {
        case GenType::FULL:
            res = make_unique< Tree<tree_type> >(this->full(0, params.maxDepth));
            break;
        case GenType::GROW:
            res = make_unique< Tree<tree_type> >(this->grow(0, params.minDepth, params.maxDepth));
            break;
        case GenType::PTC1:
        {
            //compute the odds of choosing a non-terminal
            double num = 1 - 1/(double)params.expSize;
            
            const vector<double>& funPs = probs.funProbs;
            const vector<double>& termPs = probs.termProbs;
            
            vector<double>::const_iterator it2 = funPs.begin();
            double sumFunPs = std::accumulate(funPs.begin(), funPs.end(), 0);
            
            double den = 0;
            for (vector< ExpressionNode<tree_type>* >::iterator it = _funNodes->begin(); it != _funNodes->end(); ++it, ++it2) {
                den += ((*it2)/sumFunPs)*funArity(((FunNode<tree_type>*)*it)->funType());
            }
            
            double nontermP = num/den;
            
            //prepare the allPs vector, terminals first
            vector<double> allPs(termPs);
            allPs.insert(allPs.end(), funPs.begin(), funPs.end());
            
            res = make_unique< Tree<tree_type> >(this->ptc1(0, params.maxDepth, nontermP, termPs, funPs, allPs));
        }
            break;
        case GenType::PTC2:
            throw logic_error("this algorithm has not been implemeneted yet!");
            break;
        default:
            throw logic_error("unrecognised algorithm type");
            break;
    }
    
    //pass filters
    
    //min depth
    if (res->depth() < filter.minDepth) {
        return this->genTree(algType, params, filter, probs);
    }
    
    //everything is ok, return the created tree
    return res;
}












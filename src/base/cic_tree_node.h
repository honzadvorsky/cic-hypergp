/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_tree_node.h
//  hyperGP
//
//  Created by Honza Dvorsky on 2/9/13.
//

#ifndef __hyperGP__cic_tree_node__
#define __hyperGP__cic_tree_node__

#include <string>
#include <vector>
#include <exception>
#include <map>
#include <string>
#include <cmath>
#include <assert.h>
#include "cic_tree_nodeFunx.h"
#include "cic_stringutils.h"
#include "cic_io_xmlHandler.h"

namespace cic {
    namespace tree {
        //we need to declare the tree classes here already
        /** Base abstract class for trees, for common methods */
        template <class T> class BaseTree;
        /** Mutable tree, owns its root, clears from memory after deletion. */
        template <class T> class Tree;
        /** Const tree, for reading subtrees, doesn't own root, doesn't delete root on deletion. */
        template <class T> class ConstTree;
        
        /** Abstract superclass for nodes, holds common functionality and form. */
        template <class T> class ExpressionNode;
        /** Terminal node - leaves of the tree, hold a value. */
        template <class T> class TermNode;
        /** Special type of TermNode - doesn't translate its value into index. NEVER. Used as constant terminal. */
        template <class T> class ConstTermNode;
        /** Function node - has one or two children (ExpressionNode), calculate value from children's value and the associated function. */
        template <class T> class FunNode;
        
        /** Describes attribute types. */
        enum Attribute {
            ID,             //propagate parent -> child (DOWN)
            DEPTH,          //propagate parent -> child (DOWN)
            DEPTH_UNDER,    //propagate child -> parent (UP)
            NODES_UNDER,     //propagate child -> parent (UP)
            _LAST_ITEM_ATT
        };
        
        typedef double att_t;
        typedef std::map<Attribute, att_t> Att_Map;
        
        /** Holds information about a node - depth and id. Can be used for searching - the info doesn't have to be exact.
            Searching with NodeInfo always returns nodes with real parameters: MIN[abs(real_id-node_id)] for all nodes, real_depth <= node_depth. */
        struct NodeInfo {
            /** The closest number to the real node_id of the node we're looking for. */
            att_t node_id;
            /** The number <= tree depth, the depth of the searched node. */
            att_t node_depth;
            /** Constructor for NodeInfo. Create directly with node_id and node_depth. */
            NodeInfo(att_t nd_id, att_t nd_depth) : node_id(nd_id), node_depth(nd_depth) { }
        };
        
        /** Initializes the attribute map to all -1 values. */
        void initAttMap(Att_Map& atts);
        /** Returns string representation of an attribute. */
        const std::string attString(Attribute);
        
        /** Describe the sides on which children can be. */
        enum ChildSide {
            LEFT,
            RIGHT
        };

    }; //namespace tree
}; //namespace cic

//-----------------------------------------------------------------------------------
//------------------------------------ PUBLIC ---------------------------------------
//-----------------------------------------------------------------------------------

// represents the abstract superclass of all nodes
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> class cic::tree::ExpressionNode
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    friend class Tree<T>;
    friend class io::xmlAccess;
    
protected:
    /** Pointer to the parent, 0 in case of root node. Used for attribute updates. */
    ExpressionNode<T>                   *_parent;
    /** Attribute map, hold information about the node and its surroundings. */
    Att_Map                             _attributes;
    /** Pointer to the real terminal values vector using saved terminal values as indices (vector supplied externally).
        If not present, saved terminal values are considered real values (= variables support). 
        Saved only if node is root, so that if the vector is saved, it's in the list and nowhere else. */
    std::vector<T>*                     _termMappedValues;
    
    /** Equal method getter. */
    virtual bool isEqualTo(const ExpressionNode<T>& anotherNode) const = 0;

public:
    
    /** Equal operator override */
    virtual bool operator==(const ExpressionNode<T>& anotherNode) const { return this->isEqualTo(anotherNode); };
    
    /** Check whether node is root. By checking for parent pointer. */
    bool                                isRoot() const;
    
    /** Get pointer to the parent. */
    virtual ExpressionNode<T>*          parent() const { return _parent; };
    
    /** (Re)Set pointer to the parent. */
    virtual void                        setParent(ExpressionNode<T> *parent = 0) { _parent = parent; };
    
    /** Get the term mapped values vector of the root (any node can be asked, will propagate request to the root). */
    virtual const std::vector<T>*       termMappedValues() const;
    
    /** Set the term mapped values vector (any node can be set, will propagate the action to the root). */
    virtual void                        setTermMappedValues(std::vector<T>* values);
    
    /** Get ref to the value of attribute. */
    virtual const att_t&                attribute(Attribute) const;
    
    /** String representation of all current attributes. */
    virtual const std::string           attributesString() const;
    
    /** Compute the ID for child from parent. */
    att_t                               computeIDForChild(const ChildSide& side) const;
    
    /** Search for nodes closest to node_id at depth <= node_depth. */
    virtual const ExpressionNode<T>*    searchNode(const NodeInfo& searchNodeInfo) const = 0;
    
    /** Set attribute value. */
    virtual void                        setAttribute(Attribute attType, att_t attValue = 0) = 0;
    
    /** String representation of the node. */
    virtual std::string                 description(bool withParent) const = 0;
    
    /** Visual representation of node and nodes underneath. */
    virtual std::string                 visualDescription(std::string::size_type pad, std::string::size_type step) const = 0;
    
    /** Calculate value of that node. */
    virtual T                           value() const = 0;
    
    /** Return a deep copy - no strings attached. */
    virtual ExpressionNode<T>*          clone() const = 0;
    
    /** Virtual descructor */
    virtual                             ~ExpressionNode() { delete _termMappedValues; };
    
    /** Returns a human readable form of the node. */
    virtual std::string                 humanReadable() const = 0;
    
    ExpressionNode() : _termMappedValues(nullptr) { };
};

//represents the terminal nodes (variables, constants)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> class cic::tree::TermNode : public cic::tree::ExpressionNode<T>
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    friend class io::xmlAccess;
    
protected:
    /** This is the saved value. If it also is the real value depends on whether the root has termMappedValues attached. */
    T                                   _value;
    
public:
    T                                   value() const;
    bool                                isEqualTo(const ExpressionNode<T>& anotherNode) const;
    /** Set value of terminal node. */
    void                                setValue(const T value);
    const ExpressionNode<T>*            searchNode(const NodeInfo& searchNodeInfo) const;
    void                                setAttribute(Attribute attType, att_t attValue = 0);
    std::string                         description(bool withParent) const;
    std::string                         visualDescription(std::string::size_type pad, std::string::size_type step) const;
    ExpressionNode<T>*                  clone() const;
    std::string                         humanReadable() const;
    
    //constructors
    /** Constructor, takes initial value. If value not provided, initiated with 0. */
    TermNode( T val = 0) : _value(val) { this->_parent = 0; initAttMap(this->_attributes); };
    ~TermNode() { };
};

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> class cic::tree::ConstTermNode : public cic::tree::TermNode<T>
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    friend class io::xmlAccess;
    
public:
    bool                                isEqualTo(const ExpressionNode<T>& anotherNode) const;
    T                                   value() const { return this->_value; };
    std::string                         description(bool withParent) const;
    std::string                         visualDescription(std::string::size_type pad, std::string::size_type step) const;
    std::string                         humanReadable() const;
    ExpressionNode<T>*                  clone() const;
    
    /** Constructor that takes value which is always nodes value (both saved and real). */
    ConstTermNode ( T val ) { this->_value = val; }
    /** Finds the identity value and selects it as the saved==real value. */
    ConstTermNode ( FunType type ) : ConstTermNode::ConstTermNode(cic::tree::funIdentityValue<T>(type)) { };
};

//represents the function nodes
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> class cic::tree::FunNode : public cic::tree::ExpressionNode<T>
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    friend class io::xmlAccess;
    friend class TermNode<T>;
    friend class ExpressionNode<T>;
    friend class Tree<T>;
    FunType                             _funType;
    ExpressionNode<T>*                  _left;
    ExpressionNode<T>*                  _right;
    
public:
    bool                                isEqualTo(const ExpressionNode<T>& anotherNode) const;
    T                                   value() const;
    const ExpressionNode<T>*            searchNode(const NodeInfo& searchNodeInfo) const;
    void                                setAttribute(Attribute attType, att_t attValue = 0);
    std::string                         description(bool withParent) const;
    std::string                         visualDescription(std::string::size_type pad, std::string::size_type step) const;
    ExpressionNode<T>*                  clone() const;
    /** Setter for children. */
    virtual void                        setChildOnSide(ExpressionNode<T>* child, ChildSide side);
    /** Getter for children. */
    virtual ExpressionNode<T>*          childOnSide(ChildSide side);
    /** Returns the function type in the node. */
    virtual FunType                     funType() { return _funType; };
    
    std::string                         humanReadable() const;
    
    //constructors
    /** Constructor that takes the function type (sin, product, sum, exp or else) and one or two children pointers. */
    FunNode ( FunType funType, ExpressionNode<T>* left = 0, ExpressionNode<T>* right = 0 );
    ~FunNode() { delete _left; delete _right; };
};

//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------

#pragma mark - FunNode Constructors definitions
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> cic::tree::FunNode<T>::FunNode(cic::tree::FunType funType, cic::tree::ExpressionNode<T>* left, cic::tree::ExpressionNode<T>* right)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
: _funType(funType), _left(left), _right(right)
{
    this->_parent = 0;
    initAttMap(this->_attributes);
    
    //check that arity is OK with the number of children, allowing for no children specified at the moment, too.
    bool check = true;
    switch (funArity(_funType)) {
        case 1: if (_left && _right) check = false; break;
        case 2: if (_left && !_right) check = false; break;
        default: check = false; break;
    }
    if (!check) throw std::logic_error("arity and the number of children have to be equal numbers!");
    
    if (_left ) _left->setParent(this);
    if (_right) _right->setParent(this);
}

#pragma mark - Comparing
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> bool cic::tree::FunNode<T>::isEqualTo(const ExpressionNode<T>& anotherNode) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    if (typeid(anotherNode) != typeid(FunNode)) return false;
    
    const FunNode<T>& anotherFunNode = static_cast<const FunNode<T>&>(anotherNode);
    
    if (_funType != anotherFunNode._funType) return false;
    if (!(*_left == *anotherFunNode._left)) return false;
    if (_right) {
        if (!(*_right == *anotherFunNode._right)) return false;
    }

    return true;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> bool cic::tree::TermNode<T>::isEqualTo(const ExpressionNode<T>& anotherNode) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    if (typeid(anotherNode) != typeid(TermNode)) return false;
    
    const TermNode<T>& anotherTermNode = static_cast<const TermNode<T>&>(anotherNode);
    
    if (_value != anotherTermNode._value) return false;
    
    return true;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> bool cic::tree::ConstTermNode<T>::isEqualTo(const ExpressionNode<T>& anotherNode) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    if (typeid(anotherNode) != typeid(ConstTermNode)) return false;
    
    const ConstTermNode<T>& anotherConstTermNode = static_cast<const ConstTermNode<T>&>(anotherNode);
    
    if (this->_value != anotherConstTermNode._value) return false;
    
    return true;
}

#pragma mark - Searching & manipulation
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> const cic::tree::ExpressionNode<T>* cic::tree::TermNode<T>::searchNode(const NodeInfo& searchNodeInfo) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    return this;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> const cic::tree::ExpressionNode<T>* cic::tree::FunNode<T>::searchNode(const NodeInfo& searchNodeInfo) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    if (searchNodeInfo.node_depth == this->attribute(Attribute::DEPTH)) {
        return this;
    }
    if (searchNodeInfo.node_id < this->attribute(Attribute::ID) || !_right) {
        return _left->searchNode(searchNodeInfo);
    } 
    return _right->searchNode(searchNodeInfo);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> void cic::tree::FunNode<T>::setChildOnSide(ExpressionNode<T>* child, ChildSide side)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    if (side == cic::tree::ChildSide::LEFT) {
        this->_left = child;
    } else if (side == cic::tree::ChildSide::RIGHT) {
        this->_right = child;
    } else {
        throw std::logic_error("unknown side!");
    }
    child->setParent(this);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> cic::tree::ExpressionNode<T>* cic::tree::FunNode<T>::childOnSide(cic::tree::ChildSide side)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    if (side == cic::tree::ChildSide::LEFT) {
        return this->_left;
    } else if (side == cic::tree::ChildSide::RIGHT) {
        return this->_right;
    }
    throw std::logic_error("unknown side!");
}

#pragma mark - General methods
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> bool cic::tree::ExpressionNode<T>::isRoot() const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    return !_parent;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> const std::vector<T>* cic::tree::ExpressionNode<T>::termMappedValues() const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    if (isRoot()) return _termMappedValues ? _termMappedValues : nullptr;
    return _parent->termMappedValues();
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> void cic::tree::ExpressionNode<T>::setTermMappedValues(std::vector<T>* values)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    if (isRoot()) {
        if (_termMappedValues == values) return; //if you're resetting the same vector, don't do anything
        if (_termMappedValues) delete _termMappedValues; //get rid of the previous one
        _termMappedValues = values; //assign a new mapped values vector
    }
    else _parent->setTermMappedValues(values);
}

#pragma mark - Cloning
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> cic::tree::ExpressionNode<T>* cic::tree::TermNode<T>::clone() const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    TermNode<T>* newNode = new TermNode<T>(_value);
    newNode->_attributes = this->_attributes;
    newNode->_termMappedValues = this->_termMappedValues?(new std::vector<T>(*this->_termMappedValues)):0;
    return newNode;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> cic::tree::ExpressionNode<T>* cic::tree::ConstTermNode<T>::clone() const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    ConstTermNode<T>* newNode = new ConstTermNode<T>(this->_value);
    newNode->_attributes = this->_attributes;
    newNode->_termMappedValues = this->_termMappedValues?(new std::vector<T>(*this->_termMappedValues)):0;
    return newNode;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> cic::tree::ExpressionNode<T>* cic::tree::FunNode<T>::clone() const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    FunNode<T>* newNode = new FunNode<T>(this->_funType, this->_left?this->_left->clone():0, this->_right?this->_right->clone():0);
    newNode->_attributes = this->_attributes;
    newNode->_termMappedValues = this->_termMappedValues?(new std::vector<T>(*this->_termMappedValues)):0;
    return newNode;
}

#pragma mark - Attribute magic
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> void cic::tree::TermNode<T>::setValue(const T new_val)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    _value = new_val;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> const cic::tree::att_t& cic::tree::ExpressionNode<T>::attribute(cic::tree::Attribute att_type) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    if (att_type >= Attribute::_LAST_ITEM_ATT) {
        throw std::invalid_argument("bad node attribute!");
    }
    return _attributes.at(att_type);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> void cic::tree::TermNode<T>::setAttribute(cic::tree::Attribute att_type, cic::tree::att_t att_val)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    switch (att_type) {
        case Attribute::DEPTH: // parent -> child
            this->_attributes.at(att_type) = att_val;
            //no children in terminal node, but bounce and update the depth_under and nodes_under UP
            this->setAttribute(Attribute::DEPTH_UNDER);
            this->setAttribute(Attribute::NODES_UNDER);
            break;
        case Attribute::ID: // parent -> child
            this->_attributes.at(att_type) = att_val;
            //no children in terminal node
            break;
        case Attribute::DEPTH_UNDER: // child -> parent
            this->_attributes.at(att_type) = 0;
            if (this->_parent) this->_parent->setAttribute(att_type);
            break;
        case Attribute::NODES_UNDER: // child -> parent
            this->_attributes.at(att_type) = 1;
            if (this->_parent) this->_parent->setAttribute(att_type);
            break;
        default:
            throw std::invalid_argument("bad node attribute!");
            break;
    }
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> void cic::tree::FunNode<T>::setAttribute(cic::tree::Attribute att_type, cic::tree::att_t att_val)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    switch (att_type) {
        case Attribute::DEPTH: // parent -> child
            this->_attributes.at(att_type) = att_val;
            this->_left->setAttribute(att_type, att_val+1);
            if (this->_right) this->_right->setAttribute(att_type, att_val+1);
            break;
        case Attribute::ID: // parent -> child
        {
            this->_attributes.at(att_type) = att_val;
            this->_left->setAttribute(Attribute::ID, this->computeIDForChild(ChildSide::LEFT));
            if (this->_right) this->_right->setAttribute(Attribute::ID, this->computeIDForChild(ChildSide::RIGHT));
        }
            break;
        case Attribute::DEPTH_UNDER: // child -> parent
            //only notification
            this->_attributes.at(att_type) = std::max(this->_left->attribute(att_type), (this->_right) ? this->_right->attribute(att_type) : 0) + 1;
            if (this->_parent) this->_parent->setAttribute(att_type);
            break;
        case Attribute::NODES_UNDER: // child -> parent
            //only notification
            this->_attributes.at(att_type) = this->_left->attribute(att_type) + ((this->_right!=0) ? (this->_right->attribute(att_type)) : 0) + 1;
            if (this->_parent) this->_parent->setAttribute(att_type);
            break;
        default:
            throw std::invalid_argument("bad node attribute!");
            break;
    }
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> const std::string cic::tree::ExpressionNode<T>::attributesString() const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    std::string outString;
    typedef Attribute Att;
    for (Att i = (Att)0; i < Att::_LAST_ITEM_ATT; i++ ) {
        outString += attString(i) + ":" + to_string((i==Att::ID)?"%0.4f":"%0.0f", _attributes.at(i));
        if (i != Att::_LAST_ITEM_ATT-1) {
            outString += ",";
        }
    }
    return outString;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> cic::tree::att_t cic::tree::ExpressionNode<T>::computeIDForChild(const cic::tree::ChildSide& child) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    att_t att_val = this->attribute(Attribute::ID);
    att_t par = (this->_parent) ? (this->_parent->attribute(Attribute::ID)) : 0;
    att_t step = std::abs(att_val - par)/2;
    return att_val+(step*( child == ChildSide::LEFT ? -1 : 1 ));
}

#pragma mark - Definitions of subclass specific methods
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> T cic::tree::FunNode<T>::value() const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    std::vector<T> v = std::vector<T>();
    v.push_back(_left->value());
    if (_right) v.push_back(_right->value());
    return funEval(_funType, v);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> T cic::tree::TermNode<T>::value() const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    const std::vector<T>* vals = this->termMappedValues();
    int idx = int(std::round(_value));
    if (vals && idx >= vals->size()) throw std::range_error("map term values out of range! indices too large!");
    if (vals != nullptr) return vals->at(idx);
    return _value;
}

#pragma mark - Description methods definition
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T>
std::string cic::tree::TermNode<T>::humanReadable() const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
#ifdef TARGET_HYPERGP
    int idx = (int)_value;
    switch (idx) {
        case 0:
            return "N1_x";
            break;
        case 1:
            return "N1_y";
            break;
        case 2:
            return "N2_x";
            break;
        case 3:
            return "N2_y";
            break;
            
        default:
            throw std::logic_error("unknown variable!");
            break;
    }
    
#else
    const char * letters = "xyzabcdefghijklmnopqrstuvw";
    int idx = (int)_value;
    return cic::to_string("%c", letters[idx]);
#endif
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T>
std::string cic::tree::ConstTermNode<T>::humanReadable() const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    return cic::to_string("%f", this->_value);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T>
std::string cic::tree::FunNode<T>::humanReadable() const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    switch (_funType) {
        case FunType::Abs:
            return std::string("|" + _left->humanReadable() + "|");
            break;
        case FunType::Sum:
            return std::string("(" + _left->humanReadable() + ")+(" + _right->humanReadable() + ")");
            break;
        case FunType::Diff:
            return std::string("(" + _left->humanReadable() + ")-(" + _right->humanReadable() + ")");
            break;
        case FunType::Prod:
            return std::string("(" + _left->humanReadable() + ")*(" + _right->humanReadable() + ")");
            break;
        case FunType::Divide:
            return std::string("(" + _left->humanReadable() + ")/(" + _right->humanReadable() + ")");
            break;
        case FunType::Sin:
            return std::string("sin(" + _left->humanReadable() + ")");
            break;
        case FunType::Cos:
            return std::string("cos(" + _left->humanReadable() + ")");
            break;
        case FunType::Exp:
            return std::string("exp(" + _left->humanReadable() + ")");
            break;
        case FunType::Log:
            return std::string("log(" + _left->humanReadable() + ")");
            break;
        case FunType::Atan:
            return std::string("atan(" + _left->humanReadable() + ")");
            break;
        case FunType::Gauss:
            return std::string("exp(-(" + _left->humanReadable() + ")^2)");
            break;
        default:
            throw std::logic_error("wrong function");
            break;
    }
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> std::string cic::tree::TermNode<T>::description(bool withParent) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    bool mapped = this->termMappedValues() != nullptr;
    std::string common = "(TermNode:" + (mapped?cic::to_string("idx:%i mapped to val:%0.2f",int(std::round(_value)),this->value()):to_string("%0.2f",_value)) + ")";
    std::string atts = "[" + this->attributesString() + "]";
    
    if (this->isRoot()) return common + (withParent?std::string(" 'result: " + std::to_string(this->value()) + "'"):"") +std::string(withParent?std::string(" " + atts):"");
    return common + (withParent ? std::string(" " + atts + " [Par:"+this->parent()->description(false)+"]"):"");
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> std::string cic::tree::ConstTermNode<T>::description(bool withParent) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    std::string common = "(ConstTermNode:" + (to_string("%0.2f",this->_value)) + ")";
    std::string atts = "[" + this->attributesString() + "]";
    
    if (this->isRoot()) return common + (withParent?std::string(" 'result: " + std::to_string(this->value()) + "'"):"") +std::string(withParent?std::string(" " + atts):"");
    return common + (withParent ? std::string(" " + atts + " [Par:"+this->parent()->description(false)+"]"):"");
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> std::string cic::tree::FunNode<T>::description(bool withParent) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    std::string common = "(FunNode:" + funString(_funType) + ")";
    std::string atts = "[" + this->attributesString() + "]";
    
    if (this->isRoot()) return common + (withParent?std::string(" 'result: " + std::to_string(this->value()) + "'"):"") + std::string(withParent?std::string(" " + atts):"");
    return common + (withParent ? std::string(" " + atts + " [Par:"+this->parent()->description(false)+"]"):"");
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> std::string cic::tree::TermNode<T>::visualDescription(std::string::size_type pad, std::string::size_type step) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    return std::string(pad-step, ' ') + "|" + std::string(step-1, '-') + description(true) + "\n";
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> std::string cic::tree::ConstTermNode<T>::visualDescription(std::string::size_type pad, std::string::size_type step) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    return TermNode<T>::visualDescription(pad, step);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> std::string cic::tree::FunNode<T>::visualDescription(std::string::size_type pad, std::string::size_type step) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    std::string desc = std::string(pad>=step?pad-step:pad, ' ') + (pad>=step?"|":"") + std::string(pad>=step?step-1:0, '-') + this->description(true) + "\n";

    pad += step;
    
    desc += std::string(pad-step, ' ') + _left->visualDescription(pad, step);
    if (funArity(_funType) == 2) {
        desc += std::string(pad-step, ' ') + _right->visualDescription(pad, step);
    }
    
    return desc;
}

#endif /* defined(__hyperGP__cic_tree_node__) */

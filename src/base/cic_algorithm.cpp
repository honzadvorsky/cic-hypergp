/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_algorithm.cpp
//  hyperGP
//
//  Created by Honza Dvorsky on 3/3/13.
//

#include "cic_algorithm.h"

using namespace cic;
using namespace std;

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
vector<double> alg::equalProbs(int count)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    vector<double> probs(count);
    std::fill(probs.begin(), probs.end(), ((double)1)/count);
    return probs;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
int alg::randomInteger(const vector<double>& probsNodes)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //also check that all probabilities are non-negative
    if (probsNodes.end() != std::find_if(probsNodes.begin(), probsNodes.end(), negative<double>) || !probsNodes.size()) {
        throw logic_error("probabilities must be non-negative real numbers!");
    }
    
    double int_width = ((double)1.0)/(probsNodes.size());
    vector<double> ints = vector<double>((int)(probsNodes.size())+1);
    //create intervals
    for (int i = 0; i <= probsNodes.size(); ++i) {
        ints[i] = i*int_width;
    }
    
    //create a distribution
    std::piecewise_constant_distribution<> dist_real(ints.begin(), ints.end(), probsNodes.begin());
    //create a seat generator
    random_device r;

    int res = std::floor(dist_real(r)*probsNodes.size());
    
    if (res < 0) {
        throw logic_error("here");
    }
    
    return res;
}

/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_tree_forest.h
//  hyperGP
//
//  Created by Honza Dvorsky on 4/9/13.
//

#ifndef __hyperGP__cic_tree_forest__
#define __hyperGP__cic_tree_forest__

#include <iostream>
#include <vector>
#include <numeric>
#include "cic_tree.h"

namespace cic {
    namespace tree {
        template <class T> class Forest;
    }
}

//-----------------------------------------------------------------------------------
//------------------------------------ PUBLIC ---------------------------------------
//-----------------------------------------------------------------------------------

/** Forest - an array of trees. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> class cic::tree::Forest
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    std::vector< cic::tree::Tree<T>* >* _trees;
    
protected:
    
    /** Equal method getter. */
    virtual bool isEqualTo(const Forest& anotherForest) const;
    
public:
    
    /** Equal operator override */
    virtual bool operator==(const Forest& anotherForest) const { return this->isEqualTo(anotherForest); };
    
    /** Forest constructor - takes how many trees will be held by the forest. */
    Forest(size_t treeCount) { _trees = new std::vector< cic::tree::Tree<T>* >(treeCount); };
    virtual ~Forest();
    
    /** Cloning a Forest always returns a unique, mutable Forest in auto_ptr */
    virtual std::unique_ptr< Forest<T> >        clone();
    
    /** String representation - forest structure visible */
    virtual std::string                         description() const;
    
    /** Returns an array of maximum depths of the inside trees. */
    virtual std::vector<int>                    treeDepths() const;
    
    /** Returns the sum of the depths of the trees. */
    virtual int                                 totalDepth() const;
    
    /** Returns the numbers of nodes in the trees. */
    virtual std::vector<int>                    treeNodeCounts() const;
    
    /** Return the sum of the sizes of the trees. */
    virtual int                                 totalSize() const;

    /** Returns the result of the tree (value of root). */
    virtual std::vector<T>                      treeRootValues() const;
    
    /** Returns a human readable form of the forest. */
    virtual std::string                         humanReadable() const;
    
    /** Getter for a particular tree. */
    virtual cic::tree::Tree<T>*                 treeAtIndex(size_t index) const;
    
    /** Setter for a particular tree (the tree will be retained.) */
    virtual void                                setTreeAtIndex(cic::tree::Tree<T>* tree, size_t index);
    
    /** Get number of trees. */
    virtual size_t                              treeCount() const { return _trees->size(); };
};

//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> bool cic::tree::Forest<T>::isEqualTo(const Forest& anotherForest) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    typedef typename std::vector< cic::tree::Tree<T>* >::iterator tr_it;
    tr_it itOwn = _trees->begin();
    tr_it itAnother = anotherForest._trees->begin();
    
    for (; itOwn != _trees->end(); ++itOwn, ++itAnother) {
        if (!(*itOwn == *itAnother)) {
            return false;
        }
    }
    return true;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> cic::tree::Forest<T>::~Forest<T>()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //delete the trees inside
    typename std::vector< cic::tree::Tree<T>* >::iterator it = _trees->begin();
    
    for (; it != _trees->end(); ++it) {
        delete *it;
    }
    
    delete _trees;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> std::unique_ptr< cic::tree::Forest<T> > cic::tree::Forest<T>::clone()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //create a new forest, fill it with clones of our trees
    
    size_t size = this->treeCount();
    std::unique_ptr< cic::tree::Forest<T> > newForest = std::make_unique< cic::tree::Forest<T> >(size);
    cic::tree::Tree<T> * tree = nullptr;
    
    for (int i = 0; i < size; ++i) {
        tree = this->treeAtIndex(i)->clone().release();
        newForest->setTreeAtIndex(tree, i);
    }
    
    return newForest;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> cic::tree::Tree<T>* cic::tree::Forest<T>::treeAtIndex(size_t index) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    assert(index < _trees->size());
    return (*_trees)[index];
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> void cic::tree::Forest<T>::setTreeAtIndex(cic::tree::Tree<T>* tree, size_t index)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    assert(index < _trees->size());
    if ((*_trees)[index] != nullptr) delete (*_trees)[index];
    (*_trees)[index] = tree;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> std::string cic::tree::Forest<T>::description() const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    std::string s = "[Forest - count: " + cic::to_string("%i", this->treeCount()) + "\n";
    
    for (size_t i = 0; i < this->treeCount(); ++i) {
        s += (*_trees)[i]->description() + "\n";
    }
    
    s += "]";
    return s;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> std::vector<int> cic::tree::Forest<T>::treeDepths() const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    size_t sz = this->treeCount();
    std::vector<int> s(sz);
    
    for (size_t i = 0; i < sz; ++i) {
        s[i] = (*_trees)[i]->depth();
    }
    
    return s;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> std::vector<int> cic::tree::Forest<T>::treeNodeCounts() const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    size_t sz = this->treeCount();
    std::vector<int> s(sz);
    
    for (size_t i = 0; i < sz; ++i) {
        s[i] = (*_trees)[i]->nodeCount();
    }
    
    return s;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> int cic::tree::Forest<T>::totalDepth() const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    const std::vector<int>& v = this->treeDepths();
    return std::accumulate(v.begin(), v.end(), 0);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> int cic::tree::Forest<T>::totalSize() const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    const std::vector<int>& v = this->treeNodeCounts();
    return std::accumulate(v.begin(), v.end(), 0);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> std::vector<T> cic::tree::Forest<T>::treeRootValues() const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    size_t sz = this->treeCount();
    std::vector<T> s(sz);
    
    for (size_t i = 0; i < sz; ++i) {
        s[i] = (*_trees)[i]->rootValue();
    }
    
    return s;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> std::string cic::tree::Forest<T>::humanReadable() const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    size_t sz = this->treeCount();
    std::string s;
    
    for (size_t i = 0; i < sz; ++i) {
        s += "Tree:" + cic::to_string("%i",i) + "[" + (*_trees)[i]->humanReadable() + "]";
    }
    
    return s;
}

#endif /* defined(__hyperGP__cic_tree_forest__) */







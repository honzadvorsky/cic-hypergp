/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_algorithm.h
//  hyperGP
//
//  Created by Honza Dvorsky on 3/3/13.
//

#ifndef __hyperGP__cic_algorithm__
#define __hyperGP__cic_algorithm__

#include <iostream>
#include <vector>
#include <numeric>
#include <random>
#include <cmath>

namespace cic {
    namespace alg {
        
        /** Creates a vector with all equal probabilities. */
        std::vector<double> equalProbs(int count);
        
        /** Finds random integer based on probabilities on input. */
        int randomInteger(const std::vector<double>& probsNodes);
        
        template <class T> bool negative(T val) { return val < 0; };
        template <class T> bool zero(T val) { return val == 0; };
        
        template <class T> void deleteObjects(std::vector<T>* container) {
            for (int i = 0; i < container->size(); ++i) {
                delete (*container)[i];
            }
        };
    }
}

#endif /* defined(__hyperGP__cic_algorithm__) */

/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_nn_Neuron.h
//  hyperGP
//
//  Created by Honza Dvorsky on 3/12/13.
//

#ifndef __hyperGP__cic_nn_Neuron__
#define __hyperGP__cic_nn_Neuron__

#include <iostream>
#include <vector>
#include <map>
#include "sim_consts.h"

namespace cic {
    namespace nn {
        class Neuron;
        class Synapsis;
        
        enum neuron_type {
            Unknown,
            Bias,
            Input,
            Hidden,
            Output,
        };
            
        std::string neuronTypeToString(neuron_type type);
    }
}

/** Base class for synapsis. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::nn::Synapsis
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    
    double  _timeOffset;
    double  _weight;
    Neuron* _source;
    
public:
    /** Constructor for synapsis - takes the source neuron and time offset. */
    Synapsis(Neuron* sourceNeuron, double weight, double timeOffset = 0) : _source(sourceNeuron), _weight(weight), _timeOffset(timeOffset) { };
    virtual ~Synapsis() { };
    
    /** Getter for the source neuron pointer. */
    Neuron* sourceNeuron() const { return _source; };
    
    /** Getter for the time offset. */
    double timeOffset() const { return _timeOffset; };
    
    /** Getter for the synoptic weight. */
    double weight() const { return _weight; }
};

/** Base class for neurons. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::nn::Neuron
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    
    std::vector<const Synapsis*>* _inputs;
    std::map<double, double>* _outputHistory;
    neuron_type _neuron_type = neuron_type::Unknown;
    double _const_value = __DBL_MAX__;
    
public:
    
    /** Returns the output value of the neuron at certain time step. */
    virtual double outputValueAtTime(double time, double currentTime);
    
    /** Adds input synapsis to the neuron. */
    virtual void addInputSynapsis(Neuron* sourceNeuron, double weight, double timeOffset);
    
    Neuron();
    virtual ~Neuron();
    
    /** Getter for the neuron type. */
    virtual neuron_type neuronType() const { return _neuron_type; }
    
    /** Setter for the neuron type. Bias and Input types also need their constValue set. */
    virtual void setNeuronType(neuron_type n_type) { _neuron_type = n_type; if (n_type <= neuron_type::Input) _const_value = __DBL_MAX__; }

    /** If the type is Bias or Input - getter for the constValue. */
    virtual double constValue() const { return _const_value; }
    
    /** If the type is Bias or Input - setter for the constant value. */
    virtual void setConstValue(double val) { _const_value = val; }
    
    /** Returns a neuron description. */
    virtual std::string description() const;
    
};


#endif /* defined(__hyperGP__cic_nn_Neuron__) */




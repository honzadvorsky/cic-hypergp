/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_tree.h
//  hyperGP
//
//  Created by Honza Dvorsky on 2/9/13.
//

#ifndef __hyperGP__cic_tree__
#define __hyperGP__cic_tree__

#include <memory>
#include <assert.h>
#include "cic_tree_node.h"
#include "memoryutils.h"

namespace cic {
    namespace tree {
        //classes declared in cic_tree_node.h
    }
}

//-----------------------------------------------------------------------------------
//------------------------------------ PUBLIC ---------------------------------------
//-----------------------------------------------------------------------------------

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> class cic::tree::BaseTree
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    friend class io::xmlAccess;
protected:
    /** Root pointer holder */
    ExpressionNode<T> *_root;
    
    /** Equal method getter. */
    virtual bool isEqualTo(const BaseTree& anotherTree) const;
    
public:
    virtual ~BaseTree() = 0;
    
    /** Equal operator override */
    virtual bool operator==(const BaseTree& anotherTree) const { return this->isEqualTo(anotherTree); };
    
    /** Get the term mapped values vector of the root. */
    virtual const std::vector<T>*               termMappedValues() const { return _root?_root->termMappedValues():nullptr; };
    
    /** Set the term mapped values vector of the root. (Probably clone before sending it in, if you need a unique one). */
    virtual void                                setTermMappedValues(std::vector<T>* values) { if(_root)_root->setTermMappedValues(values); };
    
    /** Reuturns const subtree (read-only) based on the node closest to input parameters (min(abs(search_id - real_id)), search_depth >= real_depth) */
    virtual std::unique_ptr< ConstTree<T> >     findSubtree(const NodeInfo& searchNodeInfo) const;
    
    /** Cloning a BaseTree (its subtype) always returns a unique, (mutable) Tree in auto_ptr */
    virtual std::unique_ptr< Tree<T> >          clone();
    
    /** String representation - tree structure visible */
    virtual std::string                         description() const;
    
    /** Returns the maximum depth of the tree. */
    virtual int                                 depth() const { return _root?_root->attribute(Attribute::DEPTH_UNDER):0; };
    
    /** Returns the number of nodes in the tree. */
    virtual int                                 nodeCount() const { return _root?_root->attribute(Attribute::NODES_UNDER):0; }
    
    /** Returns the result of the tree (value of root). */
    virtual T                                   rootValue() const { return _root->value(); };
    
    /** Returns a human readable form of the tree. */
    virtual std::string                         humanReadable() const { return _root->humanReadable(); };
};
template <class T> cic::tree::BaseTree<T>::~BaseTree<T>() {};

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> class cic::tree::ConstTree : public cic::tree::BaseTree<T>
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    friend class io::xmlAccess;
    
    friend class Tree<T>;
public:
    /** Constructor that takes ExpressionNode as input. Will not modify the subtree starting at root. */
    ConstTree(ExpressionNode<T>* root) { this->_root = root; };
    ~ConstTree() { };
};

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> class cic::tree::Tree : public cic::tree::BaseTree<T>
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    friend class io::xmlAccess;
    
    void initTreeAttributes();

public:
    
    /** Constructor that takes void or root, can be later modified, owns its root - takes memory responsibility.
     One ExpressionNode can only be input of one Tree, since Tree deletes the root in desctructor.
     */
    Tree(ExpressionNode<T>* root = 0) { this->_root = root; initTreeAttributes(); };
    ~Tree() { delete this->_root; };
    
    /** Factory for dummy trees with a constant root (ConstTermNode). */
    static Tree<T>*                         make_dummy(T val = 0) { return new Tree<T>(new ConstTermNode<T>(val)); };
    
    /** Detach subtree which is described by constTree. Returns the detached subtree. */
    std::unique_ptr< Tree<T> >              detachSubtree(std::shared_ptr< ConstTree<T> > constTree);
    
    /** Detach subtree which is described by nodeinfo. Returns the detached subtree. */
    std::unique_ptr< Tree<T> >              detachSubtree(const NodeInfo& nodeInfo);
    
    /** Attach attachTree (from attachInfo node) to this tree at this_nodeInfo node. Should be a released tree! */
    std::unique_ptr< Tree<T> >              attachSubtree(const NodeInfo& this_nodeInfo,
                                                          cic::tree::BaseTree<T>* attachTree,
                                                          const NodeInfo& attachInfo = NodeInfo(1,0));
    
    //returns subtree that was in this tree before the swap under this_nodeInfo (for pure detaching)
    /** Swapping of subtrees. Swaps subtrees described by this_nodeInfo (in this tree) and the one described by swap_nodeInfo (in swap_tree). */
    void                                    swapSubtrees(const NodeInfo& this_nodeInfo,
                                                         cic::tree::Tree<T>* swap_tree,
                                                         const NodeInfo& swap_nodeInfo);
};

//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------

#pragma mark - Utils

#pragma mark - Base methods
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> bool cic::tree::BaseTree<T>::isEqualTo(const BaseTree& anotherTree) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    assert(_root);
    if (this->depth() != anotherTree.depth()) return false;
    if (this->nodeCount() != anotherTree.nodeCount()) return false;
    return *_root == *anotherTree._root;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> std::string cic::tree::BaseTree<T>::description() const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    return _root->visualDescription(3,3);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> std::unique_ptr< cic::tree::ConstTree<T> > cic::tree::BaseTree<T>::findSubtree(const NodeInfo& searchNodeInfo) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //check correct search query
    if (searchNodeInfo.node_id < 0 || searchNodeInfo.node_id > 2) throw std::logic_error("search query for node_id must be >= 0 and <= 2!");
    if (searchNodeInfo.node_depth < 0 || searchNodeInfo.node_depth > _root->attribute(Attribute::DEPTH_UNDER))
        throw std::logic_error("search query for node_depth must be >= 0 and <= tree depth!");

    ExpressionNode<T>* foundNode = const_cast<ExpressionNode<T>*>(_root->searchNode(searchNodeInfo));
    return std::make_unique< ConstTree<T> >(foundNode);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> std::unique_ptr< cic::tree::Tree<T> > cic::tree::BaseTree<T>::clone()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //clone the root and underlying subtree and return
    return std::make_unique< Tree<T> >(_root->clone());
}

#pragma mark - Lifetime
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> void cic::tree::Tree<T>::initTreeAttributes()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //bubble down the IDs
    this->_root->setAttribute(Attribute::ID, 1);
    this->_root->setAttribute(Attribute::DEPTH, 0);
    //the depth call will bubble down and bounce back up with depth_under and nodes_under updates
}

#pragma mark - Tree Manipulation

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> std::unique_ptr< cic::tree::Tree<T> > cic::tree::Tree<T>::detachSubtree(const cic::tree::NodeInfo& subrootInfo)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    Tree<T>* ret = make_dummy();
    if (this->termMappedValues()) {
        ret->setTermMappedValues(new std::vector<T>(*this->termMappedValues()));
    }
    this->swapSubtrees(subrootInfo, ret, NodeInfo(1,0));
    return std::make_unique< Tree<T> >(*ret);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> std::unique_ptr< cic::tree::Tree<T> > cic::tree::Tree<T>::detachSubtree(std::shared_ptr< ConstTree<T> > constTree)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //check that the found node really belongs to both the ConstTree and to this tree. first find the node based on the root of ConstTree.
    const NodeInfo& rootInfo = NodeInfo(constTree->_root->attribute(Attribute::ID), constTree->_root->attribute(Attribute::DEPTH));
    ExpressionNode<T>* newRoot = const_cast<ExpressionNode<T>*>(this->_root->searchNode(rootInfo));
    if (newRoot != constTree->_root) {
        throw std::logic_error("trying to detach from the wrong tree! the const tree needs to be a subtree of the tree!");
        return nullptr;
    }
    return this->detachSubtree(rootInfo);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> std::unique_ptr< cic::tree::Tree<T> > cic::tree::Tree<T>::attachSubtree(const NodeInfo& this_nodeInfo,
                                                                                           cic::tree::BaseTree<T>* attachTree,
                                                                                           const NodeInfo& attachInfo) // = NodeInfo(1,0)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    Tree* tr = dynamic_cast<Tree*>(attachTree);
    if (!tr) {
        //it's a const tree
        tr = (tr->clone()).release();
    }
    if (this->termMappedValues() && !tr->termMappedValues()) {
        tr->setTermMappedValues(new std::vector<T>(*this->termMappedValues()));
    }
    this->swapSubtrees(this_nodeInfo, tr, attachInfo);
    return std::unique_ptr< Tree<T> >(tr);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
template <class T> void cic::tree::Tree<T>::swapSubtrees(const cic::tree::NodeInfo& this_nodeInfo,
                                                         cic::tree::Tree<T>* swap_tree,
                                                         const cic::tree::NodeInfo& swap_nodeInfo)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    using namespace std;
    
    assert(swap_tree);
    
    //find the swap node in this tree
    ExpressionNode<T>* this_subtree_node = const_cast<ExpressionNode<T>*>(this->_root->searchNode(this_nodeInfo));
    //find the swap node in the swap tree
    ExpressionNode<T>* swap_subtree_node = const_cast<ExpressionNode<T>*>(swap_tree->_root->searchNode(swap_nodeInfo));
    
//    cout << endl << "This:" << endl << this_subtree_node->visualDescription(3,3) << endl;
//    cout << endl << "Swap:" << endl << swap_subtree_node->visualDescription(3,3) << endl;
    
    ExpressionNode<T>** this_parent2child_ptr = 0;
    ExpressionNode<T>** swap_parent2child_ptr = 0;
    
    FunNode<T>** this_child2parent_ptr = (FunNode<T>**)(&this_subtree_node->_parent);
    FunNode<T>** swap_child2parent_ptr = (FunNode<T>**)(&swap_subtree_node->_parent);
    
    ChildSide this_child_side;
    ChildSide swap_child_side;

    //get parent pointers to children in both, first in this
    if (this_subtree_node->isRoot()) {
        this_parent2child_ptr = &this->_root;
    } else {
        this_child_side = (this_subtree_node == (*this_child2parent_ptr)->_left) ? ChildSide::LEFT : ChildSide::RIGHT;
        this_parent2child_ptr = (this_child_side == ChildSide::LEFT) ? &(*this_child2parent_ptr)->_left : &(*this_child2parent_ptr)->_right;
    }

    //the same in input tree
    if (swap_subtree_node->isRoot()) {
        swap_parent2child_ptr = &swap_tree->_root;
    } else {
        swap_child_side = (swap_subtree_node == (*swap_child2parent_ptr)->_left) ? ChildSide::LEFT : ChildSide::RIGHT;
        swap_parent2child_ptr = (swap_child_side == ChildSide::LEFT) ? &(*swap_child2parent_ptr)->_left : &(*swap_child2parent_ptr)->_right;
    }
    
    //do the swap for their parents
    ExpressionNode<T>* temp(*this_parent2child_ptr);
    *this_parent2child_ptr = *swap_parent2child_ptr;
    *swap_parent2child_ptr = temp;

    //do the swap for the children's pointers to parents
    FunNode<T>* temp2(*this_child2parent_ptr);
    *this_child2parent_ptr = *swap_child2parent_ptr;
    *swap_child2parent_ptr = temp2;

    //also swap termMappedValues
    std::vector<T>** this_termMap = &this_subtree_node->_termMappedValues;
    std::vector<T>** swap_termMap = &swap_subtree_node->_termMappedValues;
    std::vector<T>* term3(*this_termMap);
    *this_termMap = *swap_termMap;
    *swap_termMap = term3;
    
    //update swap's new subtree (this's old one)
    (*this_parent2child_ptr)->setAttribute(Attribute::ID, (*swap_child2parent_ptr)?(*swap_child2parent_ptr)->computeIDForChild(this_child_side):1);
    (*this_parent2child_ptr)->setAttribute(Attribute::DEPTH, (*swap_child2parent_ptr)?(*swap_child2parent_ptr)->attribute(Attribute::DEPTH)+1:0);
    //update this's new subtree (swap's old one)
    (*swap_parent2child_ptr)->setAttribute(Attribute::ID, (*this_child2parent_ptr)?(*this_child2parent_ptr)->computeIDForChild(swap_child_side):1);
    (*swap_parent2child_ptr)->setAttribute(Attribute::DEPTH, (*this_child2parent_ptr)?(*this_child2parent_ptr)->attribute(Attribute::DEPTH)+1:0);
        
    //that should be all :)
}


#endif /* defined(__hyperGP__cic_tree__) */























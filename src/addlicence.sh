#!/bin/bash

echo "Adding licence headers..."

licenceText=`cat $1`

# shopt -s globstar

for file in **/*.h
do
  if [[ ! -f "$file" ]]
  then
      continue
  fi
  echo "$file"
  # Do something else.      

#add the licence text to each file
	cat "$file" | pbcopy && echo "$licenceText" > $file && pbpaste >> $file

#remove the first n lines from the file
	# sed -e '6,6d' < "$file" | pbcopy && pbpaste > "$file";

done




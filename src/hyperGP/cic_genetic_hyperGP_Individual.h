/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_genetic_hyperGP_Individual.h
//  hyperGP
//
//  Created by Honza Dvorsky on 2/3/13.
//

#ifndef __hyperGP__cic_genetic_hyperGP_Individual__
#define __hyperGP__cic_genetic_hyperGP_Individual__

#include "cic_genetic_Individual.h"
#include "cic_tree.h"
#include "cic_io_xmlHandler.h"
#include "cic_nn_Network.h"

namespace cic {
    namespace genetic {
        namespace hyperGP {
            
            class Individual;
            class Genotype;
            class Phenotype;
                    
        } //namespace hyperGP
    } //namespace genetic
} //namespace cic

/** Specific genotype class for the hyperGP experiment. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::genetic::hyperGP::Genotype : public cic::genetic::Genotype
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    friend class io::xmlAccess;
    
public:
    /** Constructor for Genotype with input: Tree. */
    Genotype(tree::Forest<genetic::tree_type>* fr) : genetic::Genotype(fr) { };
    ~Genotype() { }
    
    genetic::Genotype* clone() { return new genetic::hyperGP::Genotype(_forest->clone().release()); };
};

/** Specific phenotype class for the hyperGP experiment. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::genetic::hyperGP::Phenotype : public cic::genetic::Phenotype
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    friend class io::xmlAccess;
    nn::Network* _neuralNetwork;
    
public:
    /** Phenotype constructor which translates the genotype to phenotype.. */
    Phenotype(nn::Network* net) : _neuralNetwork(net) { };
    ~Phenotype() { delete _neuralNetwork; };
    
    genetic::tree_type valueForInputs(const std::vector<tree_type>& inputs) const { throw std::logic_error("use neural-specific getter instead."); };

    /** Neural network getter. */
    nn::Network* neuralNetwork() const { return _neuralNetwork; };
    
    std::string description() const { return _neuralNetwork->description(); };
    
    //don't clone phenotypes
    genetic::Phenotype* clone() { return nullptr; };
};

/** HyperGP specific Individual type. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::genetic::hyperGP::Individual : public cic::genetic::Individual
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    friend class io::xmlAccess;
    
public:
    //should not introduce new public methods!
    
    /** Create a hyperGP individual (pretty much a regular genetic individual). */
    Individual(genetic::Genotype* gen) : genetic::Individual(gen) { };
    ~Individual() { };
    
    /** Strips down fitness and phenotype, only genotype is preserved. */
    cic::Individual* clone() { return new hyperGP::Individual(dynamic_cast<hyperGP::Genotype*>(this->_genotype->clone())); };

};

#endif /* defined(__hyperGP__cic_genetic_hyperGP_Individual__) */

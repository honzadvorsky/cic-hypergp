/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//
//  neuralController.cpp
//  hyperGP
//
//  Copyright (c) 2013 Honza Dvorsky. All rights reserved.
//

#include "neuralController.h"
#include <exception>
#include <cmath>
#include <assert.h>

using namespace std;

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
NeuralController::NeuralController(const data_map* data, sim_range range) : _data(data), _sim_range(range)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    _delayedStartPosition = sim::Vec3(0,0,0);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void NeuralController::init(sim::Sim *sim)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    _sim = sim;
    sim->regPreStep(this);
    
    sim->setTimeStep(sim::Time::fromMs(_sim_range.step*1000 * _sim_range.sampleAvgCount / _sim_range.extraStepDivider));
    sim->setTimeLimit(sim::Time::fromMs(_sim_range.stop*1000));
//    sim->setTimeSubSteps(_sim_range.extraStepDivider);
    
    //print data
    // std::map<double, std::map<cic::nn::neuron_id, double> >
//    data_map::const_iterator it = _data->begin();
//    for (; it != _data->end(); ++it) {
//        
//        cout << "Time: " << it->first << endl;
//        map<cic::nn::neuron_id, double>::const_iterator it_in = it->second.begin();
//        for (; it_in != it->second.end(); ++it_in) {
//            cout << "Neur: " << it_in->first.x << " " << it_in->first.y << " Sig: " << it_in->second << endl;
//        }
//    }
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
sim::Vec3 NeuralController::delayedStartPosition()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    if (_delayedStartPosition == sim::Vec3(0,0,0)) {
        std::cout << "Delayed start might not have been written!" << std::endl;
    }
    
    return _delayedStartPosition;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
sim::Vec3 NeuralController::endPosition()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //subtract the delayed start position
    return this->currentPosition() - _delayedStartPosition;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
sim::Vec3 NeuralController::currentPosition()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    const double widthX = 1.254; // these constants define dimensions of SSSA robots
    const double widthY = 1.254;
    
    for (size_t i = 0; i < _robots.size(); ++i) {
        
        sim::Vec3 pos = _robots[i]->relative_pos();
        int id_x = (int)(pos.x() / widthX);
        int id_y = (int)(pos.y() / widthY);
        cic::nn::neuron_id nid(id_x, id_y);
        if(id_x == 0 && id_y == 0) {
            return _robots[i]->pos();
        }
    }
    return sim::Vec3();
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void NeuralController::cbPreStep()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    sim::Time t = _sim->timeSimulated();
    double ts = t.s() + t.ms()/1000.0;
    
    //round to the fifth decimal place
    int places = 10;
    double bigT = pow(10.0, places) * ts;
    bigT = round(bigT);
    ts = bigT / pow(10.0,places);
    
//    double fmax = _robots[0]->getFMax();
    
    //get the data for this timestep
    data_map::const_iterator time_it = _data->find(ts);
    if  (time_it == _data->end()) {
//        assert(false);
#warning TURNED OFF ASSERT FOR FOUND DATA
        return;
    }
    
    if (ts <= _sim_range.delayedStart && ts + _sim_range.step > _sim_range.delayedStart) {
        //record the delayed start position
        _delayedStartPosition = this->currentPosition();
    }
    
    const std::map<cic::nn::neuron_id, double>& timeData = time_it->second;
    
    const double widthX = 1.254; // these constants define dimensions of SSSA robots
    const double widthY = 1.254;

    //give all the robots their velocities
    for (size_t i = 0; i < _robots.size(); ++i) {
        
        const sim::Vec3& pos = _robots[i]->relative_pos();
        int id_x = (int)(pos.x() / widthX);
        int id_y = (int)(pos.y() / widthY);
        cic::nn::neuron_id nid(id_x, id_y);
        
        if(timeData.find(nid) == timeData.end()) {
            _robots[i]->fixArm();
            continue;
        }
        
        double sign = timeData.at(nid);
        double angle = (M_PI/2.0)*sign;
        
//        std::cout << "x = " << id_x << " y = " << id_y << " sign: " << sign << " ang: " << angle << std::endl;
//        std::cout << "x = " << id_x << " y = " << id_y << " sign: " << sign << std::endl;

//#warning VELOCITY NOT POSITION
//        _robots[i]->setVelArm(sign);
        if(_robots[i]->reachArmAngle(angle)) {
            //if the arm is in that angle already, it gets fixed, but we don't want that
            _robots[i]->unfixArm();
        }
        
    }
    
}





/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_genetic_hyperGP_Simulator.h
//  hyperGP
//
//  Created by Honza Dvorsky on 2/3/13.
//

#ifndef __hyperGP__cic_genetic_hyperGP_Simulator__
#define __hyperGP__cic_genetic_hyperGP_Simulator__

#include "cic_genetic_Simulator.h"
#include "cic_Settings.h"
#include <map>
#include <vector>
#include "sim_consts.h"

namespace cic {
    namespace genetic {
        namespace hyperGP {
            class Simulator;
        } //namespace hyperGP
    } //namespace genetic
} //namespace cic

/** Simulator - connects to the robotic simulator. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::genetic::hyperGP::Simulator : public cic::genetic::Simulator
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    double fitnessForData(std::map<double, std::map<cic::nn::neuron_id, double> > *data, bool with_gui) const;
    double fitnessFromEndPosition(const double* endPosition) const;
public:
    
    /** Simulates individual, either with gui or without. */
    void                                            simulateIndividual(void* ind, bool with_gui, bool enable_plot_outputs, std::string appendix = "") const;
    
    void                                            init() { };
    
    /** Get info about robot type. */
    static std::vector<double> robotDimensionsForType(RobotType::Type robotType);
    
    /** Assigns each individual their fitness. */
    std::shared_ptr<cic::Population>        processPopulation(std::shared_ptr<cic::Population> popIn) const;
    
};

#endif /* defined(__hyperGP__cic_genetic_hyperGP_Simulator__) */

/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_genetic_hyperGP_Simulator.cpp
//  hyperGP
//
//  Created by Honza Dvorsky on 2/3/13.
//

#include "cic_genetic_hyperGP_Simulator.h"
#include "cic_genetic_Population.h"
#include <string>
#include <iostream>
#include "cic_genetic_hyperGP_Individual.h"
#include "cic_genetic_hyperGP_HypercubicEncoding.h"
#include "cic_io_matlab_handler.h"

#ifdef USE_GCD
#include <dispatch/dispatch.h>
#endif

#include "hyperGP_sim2.h"
#include "cic_nn_network.h"

using namespace cic;
using namespace std;

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
double genetic::hyperGP::Simulator::fitnessFromEndPosition(const double* endPosition) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    double fitness;
    
    switch (Settings::get()->_simulator.robot_type) {
        case RobotType::Simple5:
            fitness = abs(endPosition[0]) + (endPosition[2]-0.5);
            break;
        case RobotType::Complex7:
        case RobotType::Rect57:
            fitness = abs(endPosition[1]) + (endPosition[2]-0.5);
            break;
        default:
            fitness = 0;
            assert("unrecognized robot type");
            break;
    }
    
    return fitness;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
std::vector<double> cic::genetic::hyperGP::Simulator::robotDimensionsForType(RobotType::Type robotType)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    vector<double> dim(2);
    
    switch (robotType) {
        case RobotType::Simple5:
            dim[0] = 5;
            dim[1] = 5;
            break;
        case RobotType::Complex7:
            dim[0] = 7;
            dim[1] = 7;
            break;
        case RobotType::Rect57:
            dim[0] = 7;
            dim[1] = 5;
            break;
        case RobotType::One:
            dim[0] = 1;
            dim[1] = 1;
            break;
        default:
            assert(false);
            break;
    }
    
    return dim;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
double cic::genetic::hyperGP::Simulator::fitnessForData(std::map<double, std::map<nn::neuron_id, double> > *data, bool with_gui) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    const std::map<double, std::map<nn::neuron_id, double> > data_ref = *data;
    
#warning MAYBE WE DONT NEED GUI TO GET POVRAY?
    string path = (with_gui && Settings::get()->_simulator.generatePovrayFiles) ? Settings::get()->outputFilePrefix().c_str() : "";
    
    cic::sim::Simulator_Plugin2 simulator(path.c_str());
    
    sim_range range(
                    Settings::get()->_simulator.sample_from,
                    Settings::get()->_simulator.sample_to,
                    Settings::get()->_simulator.sample_step,
                    Settings::get()->_neural.sampleAverageCount,
                    Settings::get()->_simulator.delayed_start,
                    Settings::get()->_simulator.robot_type,
                    Settings::get()->_simulator.extraTimeStepDivider
                    );
    
    std::map<double, std::map<nn::neuron_id, double> >::const_iterator it;
    std::map<nn::neuron_id, double>::const_iterator it_in;
    
    const std::map<nn::neuron_id, double>* in_map;
    
    for (it = data_ref.begin(); it != data_ref.end(); ++it) {
        
        in_map = &it->second;
        size_t count = in_map->size();
        
        nn::neuron_id* neurons = NULL;
        double* signals = NULL;
        
        neurons = new nn::neuron_id[count];
        signals = new double[count];
        
        int i = 0;
        
        for (it_in = in_map->begin(); it_in != in_map->end(); ++it_in, ++i) {
            neurons[i] = it_in->first;
            signals[i] = it_in->second;
        }
        
        simulator.registerTimeData(it->first, neurons, signals, count);
        
        delete [] neurons;
        delete [] signals;
    }
    
    double* endPosition = simulator.start(range, with_gui, Settings::get()->_neural.useMiddleLegs);
    double fit = this->fitnessFromEndPosition(endPosition);
    delete [] endPosition;
    return fit;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void genetic::hyperGP::Simulator::simulateIndividual(void* _ind, bool with_gui, bool enable_plot_outputs, string appendix) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    genetic::Individual *ind = (genetic::Individual*)_ind;
    
    //generate points for the simulator
    const genetic::hyperGP::Phenotype* phen = dynamic_cast<const genetic::hyperGP::Phenotype*>(ind->phenotype());
    const nn::Network *net;
    map<double, map<cic::nn::neuron_id, double> > data;
    
//    if (ind->fitness() != Settings::get()->_gp.defaultFitness) {
//        cout << "Fitness before: " << ind->fitness() << endl;
//    }
    
    int avgSamples = Settings::get()->_neural.sampleAverageCount;
    
    if (!phen) {
        //generate network additionally, if called directly from control block
        hyperGP::HypercubicEncoding temp;
        ind->setPhenotype(temp.generatePhenotypeForGenotype(ind->genotype()));
        phen = dynamic_cast<const genetic::hyperGP::Phenotype*>(ind->phenotype());
        net = phen->neuralNetwork();
        data = net->computeOutputData(avgSamples);
    } else {
        net = phen->neuralNetwork();
        data = net->computeOutputData(avgSamples);
    }
    
    if (enable_plot_outputs && Settings::get()->_control.useMatlabPlot) {
        cout << net->description() << endl;
        io::MatlabHandler::get()->showNeuralOutputs(&data, appendix);
    }
    
//    cout << "Phen: " << phen << endl;
    
    //simulate the DATA in the simulator and assign fitness
    double fit = this->fitnessForData(&data, with_gui);
    
//    cout << "Fitness after: " << fit << endl;
    
    ind->setFitness(fit);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
shared_ptr<Population> genetic::hyperGP::Simulator::processPopulation(shared_ptr<cic::Population> _popIn) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    genetic::Population* pop = dynamic_cast<genetic::Population*>(_popIn.get());
    if (!pop) throw bad_cast();
    
    //---------------------------------------
    //----- SIMULATION TAKES PLACE HERE -----
    //---------------------------------------
    
    //assign/create a phenotype for the individual, IF IT DOESN'T HAVE ONE!
    
    __block int count = 0;
    
#if defined(USE_GCD) && defined(USE_GCD_WITH_SIM)
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_apply(pop->size(), queue, ^(size_t k)
                   {
                       genetic::Individual *ind = (pop->begin() + k)->get();
#else
                       genetic::Population::iterator it = pop->begin();
                       for (; it != pop->end(); ++it) {
                           genetic::Individual *ind = it->get();
#endif
                           //do the hard work
                           if (ind->fitness() == Settings::get()->_gp.defaultFitness) {
                               this->simulateIndividual(ind, false, false);
                               count++;
                           }
                           
#if defined(USE_GCD) && defined(USE_GCD_WITH_SIM)
                       });
#else
                   }
#endif
                   
                   //---------------------------------------
                   
                   cout << "Simulator updated " << count << "/" << pop->size() << " individuals." << endl;
                   
                   //we have our correct population, proceed
                   cout << "Iteration: " << pop->iteration() << " : Population " << pop << " just passed through Simulator!" << endl;
                   
                   //cast back up
                   return _popIn;
                   }
                   
                   

                   

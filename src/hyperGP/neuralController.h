/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  neuralController.h
//  hyperGP
//
//  Created by Honza Dvorsky on 3/25/13.
//

#ifndef __hyperGP__neuralController__
#define __hyperGP__neuralController__

#include <iostream>
#include <map>

#include <sim/sim.hpp>
#include <sim/robot/sssa.hpp>

#include "sim_consts.h"

/** Neural network source. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class NeuralController : public sim::Component
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    bool _useMiddleLegs;
    
public:
    /** Define the way we transfer data from NN to simulator. */
    typedef std::map<double, std::map<cic::nn::neuron_id, double> > data_map;
    
    /** NC constructor. */
    NeuralController(const data_map* data, sim_range range);
    
    ~NeuralController() { };
    
    /** Is called before the step. */
    void cbPreStep();
        
    /** Called to init with a sim. */
    void init(sim::Sim *sim);
    
    /** Adds robots. */
    void setRobots(std::vector<sim::robot::SSSA *> robots) { _robots = robots; }
    
    /** Saved simulation range. */
    sim_range simulationRange() const;
    
    /** Delayed start position getter. */
    sim::Vec3 delayedStartPosition();
    
    /** Traveled vector getter - returns the difference of delayedStartPosition and real end position. */
    sim::Vec3 endPosition();
    
    /** Gets the position of the center cube. */
    sim::Vec3 currentPosition();
    
    /** Middle legs getter. */
    bool shouldUseMiddleLegs() { return _useMiddleLegs; }
    
    /** Middle legs getter. */
    void setShouldUseMiddleLegs(bool useMidLegs) { _useMiddleLegs = useMidLegs; }
    
private:
    std::vector<sim::robot::SSSA *> _robots;
    const data_map* _data;
    sim_range _sim_range;
    sim::Vec3 _delayedStartPosition;
};


#endif /* defined(__hyperGP__neuralController__) */

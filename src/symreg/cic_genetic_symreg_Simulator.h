/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_genetic_symreg_Simulator.h
//  hyperGP
//
//  Created by Honza Dvorsky on 2/24/13.
//

#ifndef __hyperGP__cic_genetic_symreg_Simulator__
#define __hyperGP__cic_genetic_symreg_Simulator__

#include "cic_genetic_Simulator.h"
#include <iostream>
#include <vector>

namespace cic {
    namespace genetic {
        namespace symreg {
            class Simulator;
            class Phenotype;
        } //namespace hyperGP
    } //namespace genetic
} //namespace cic

/** symreg specific simulator class. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::genetic::symreg::Simulator : public cic::genetic::Simulator
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    std::vector<double>* _xPoints;
    std::vector<double>* _yPoints;
    
    void generateTestPoints();
    double errorForPhenotype(const symreg::Phenotype* phen) const;
    double fitnessFromError(double error) const;
    
public:

    Simulator() { std::cout << "Simulator created." << std::endl; };
    ~Simulator() { delete _xPoints; delete _yPoints; std::cout << "Simulator deleted." << std::endl; };
    
    void        init();
    
    /** Process population, aka test each individual's phenotype and assign them a fitness. */
    std::shared_ptr<cic::Population> processPopulation(std::shared_ptr<cic::Population> popIn) const;

};

#endif /* defined(__hyperGP__cic_genetic_symreg_Simulator__) */

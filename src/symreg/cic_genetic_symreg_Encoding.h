/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_genetic_symreg_Encoding.h
//  hyperGP
//
//  Created by Honza Dvorsky on 2/24/13.
//

#ifndef __hyperGP__cic_genetic_symreg_Encoding__
#define __hyperGP__cic_genetic_symreg_Encoding__

#include "cic_genetic_Encoding.h"
#include <iostream>

namespace cic {
    namespace genetic {
        namespace symreg {
            class Encoding;
        } //namespace hyperGP
    } //namespace genetic
} //namespace cic

/** symreg specific encoding class. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::genetic::symreg::Encoding : public cic::genetic::Encoding
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
public:
    Encoding() { std::cout << "Encoding created." << std::endl; };
    ~Encoding() { std::cout << "Encoding deleted." << std::endl; };
    
    void        init();
    
    /** Generate phenotype for a genotype. */
    std::unique_ptr<cic::genetic::Phenotype> generatePhenotypeForGenotype(const cic::genetic::Genotype* gen) const;
};
#endif /* defined(__hyperGP__cic_genetic_symreg_Encoding__) */

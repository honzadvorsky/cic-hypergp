/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_genetic_symreg_BloatControl.h
//  hyperGP
//
//  Created by Honza Dvorsky on 2/24/13.
//

#ifndef __hyperGP__cic_genetic_symreg_BloatControl__
#define __hyperGP__cic_genetic_symreg_BloatControl__

#include "cic_genetic_BloatControl.h"
#include <iostream>

namespace cic {
    namespace genetic {
        namespace symreg {
            class BloatControl;
        }
    }
}

/** Bloat control block - prevents individuals from unnecessarily becoming larger without improving fitness. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::genetic::symreg::BloatControl : public cic::genetic::BloatControl
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    
public:
    
    BloatControl() { std::cout << "Bloat Control created." << std::endl; };
    ~BloatControl() { std::cout << "Bloat Control deleted." << std::endl; };
    
};


#endif /* defined(__hyperGP__cic_genetic_symreg_BloatControl__) */

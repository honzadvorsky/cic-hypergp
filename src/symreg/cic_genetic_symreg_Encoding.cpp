/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_genetic_symreg_Encoding.cpp
//  hyperGP
//
//  Created by Honza Dvorsky on 2/24/13.
//

#include <iostream>
#include "memoryutils.h"
#include "cic_genetic_symreg_Encoding.h"
#include "cic_genetic_symreg_Population.h"
#include "cic_genetic_symreg_Individual.h"

using namespace std;
using namespace cic;

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void genetic::symreg::Encoding::init()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    cout << "Encoding inited." << endl;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
std::unique_ptr<genetic::Phenotype> genetic::symreg::Encoding::generatePhenotypeForGenotype(const genetic::Genotype* gen) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    const genetic::symreg::Genotype* symgen = dynamic_cast<const genetic::symreg::Genotype*>(gen);
    if (!symgen) throw logic_error("wrong genotype class!");
    
    return make_unique<genetic::symreg::Phenotype>(symgen);
}









/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_genetic_symreg_Simulator.cpp
//  hyperGP
//
//  Created by Honza Dvorsky on 2/24/13.
//

#include "cic_genetic_symreg_Simulator.h"

#include <iostream>
#include "memoryutils.h"
#include "cic_genetic_symreg_Population.h"
#include <cmath>
#include "cic_genetic_symreg_Individual.h"

using namespace std;
using namespace cic;

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void genetic::symreg::Simulator::init()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    this->generateTestPoints();
    
    cout << "Simulator inited." << endl;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
shared_ptr<Population> genetic::symreg::Simulator::processPopulation(shared_ptr<cic::Population> _popIn) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //cast down
    genetic::symreg::Population* pop = dynamic_cast<genetic::symreg::Population*>(_popIn.get());
    if (!pop) throw bad_cast();
    
    //perform the 'simulation' and assign all individuals their fitness
    genetic::symreg::Population::iterator it = pop->begin();
    double fitness;
    int count = 0;
    for (; it != pop->end(); ++it) {
        if ((*it)->fitness() == Settings::get()->_gp.defaultFitness) {
            fitness = this->fitnessFromError(this->errorForPhenotype(dynamic_cast<const symreg::Phenotype*>((*it)->phenotype())));
            (*it)->setFitness(fitness);
            count++;
        }
    }

    cout << "Updated fitness for " << count << " individuals." << endl;
    
    //we have our correct population, proceed
    cout << "Iteration: " << pop->iteration() << " : Population " << pop << " just passed through Simulator!" << endl;
    
    //cast back up
    return _popIn;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void genetic::symreg::Simulator::generateTestPoints()
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    double (*myfun)(double);

    //choose a function
    myfun = Settings::get()->_simulator.function;
    
    //compute points with it (for just one input at the moment)
    double step = Settings::get()->_simulator.sample_step;
    double stop = Settings::get()->_simulator.sample_to;
    
    //create vectors for the data
    _xPoints = new vector<double>();
    _yPoints = new vector<double>();
    
    for (double start = Settings::get()->_simulator.sample_from; start <= stop; start+=step) {
        _xPoints->push_back(start);
        double r = ((double)rand())/RAND_MAX - 0.5;
//        cout << r << endl;
        _yPoints->push_back((*myfun)(start) + 0.000001*r);
    }
    
    cout << _xPoints->size() << " test points generated." << endl;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
double genetic::symreg::Simulator::fitnessFromError(double error) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    return -error;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
double genetic::symreg::Simulator::errorForPhenotype(const symreg::Phenotype* phen) const
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    if (!phen) throw logic_error("no phenotype provided!");
    
    //compute the culumative error for the phenotype in all test points
    vector<double> inputs;
    vector<double>::iterator it_x = _xPoints->begin();
    vector<double>::iterator it_y = _yPoints->begin();
    double error = 0;
    for (; it_x != _xPoints->end(); ++it_x, ++it_y) {
        inputs.push_back(*it_x);
        error += std::abs( *it_y - phen->valueForInputs(inputs) );
        inputs.clear();
    }
    
    if (std::isnan(error)) {
        throw logic_error("nan in simulator!");
    }
    
    return error;
}
















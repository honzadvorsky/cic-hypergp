/***
 * cic
 * ---------------------------------
 * Copyright (c) 2013 Honza Dvorsky 
 * <jan.dvorsky@yahoo.com>
 * <http://www.honzadvorsky.com>
 *
 *  This file is part of cic.
 *
 *  cic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  cic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//
//  cic_genetic_symreg_Individual.h
//  hyperGP
//
//  Created by Honza Dvorsky on 2/24/13.
//

#ifndef __hyperGP__cic_genetic_symreg_Individual__
#define __hyperGP__cic_genetic_symreg_Individual__

#include "cic_genetic_Individual.h"
#include "cic_io_xmlHandler.h"
#include "cic_tree_TreeGenerator.h"

namespace cic {
    namespace genetic {
        namespace symreg {
            
            class Individual;
            class Genotype;
            class Phenotype;
                    
        } //namespace symreg
    } //namespace genetic
} //namespace cic

/** Specific genotype class for the symreg experiment. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::genetic::symreg::Genotype : public cic::genetic::Genotype
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    friend class io::xmlAccess;
    
public:
    /** Create genotype for symreg experiment with a tree. */
    Genotype(tree::Tree<genetic::tree_type>* tr) : genetic::Genotype(tr) { };
    ~Genotype() { };
        
    genetic::Genotype* clone() { return new genetic::symreg::Genotype(_tree->clone().release()); };
    
};

/** Specific phenotype class for the symreg experiment. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::genetic::symreg::Phenotype : public cic::genetic::Phenotype
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    friend class io::xmlAccess;
    
    const genetic::symreg::Genotype*           _source;
public:
    /** Phenotype constructor which just reads the data from the genotype. */
    Phenotype(const genetic::symreg::Genotype* src) : _source(src) { };
    ~Phenotype() { };
    
    genetic::tree_type valueForInputs(const std::vector<tree_type>& inputs) const { return _source->valueForInputs(inputs); };
    std::string description() const { return "\n Phenotype: \n" + _source->description() + "\n"; };
    
    genetic::Phenotype* clone() { return nullptr; };
};

/** symreg specific Individual type. */
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class cic::genetic::symreg::Individual : public cic::genetic::Individual
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    friend class io::xmlAccess;
        
public:
    //should not introduce new public methods!
    
    /** Create a symreg individual (pretty much a regular genetic individual). */
    Individual(genetic::Genotype* gen) : genetic::Individual(gen) { };
    ~Individual() { };
    
    /** Strips down fitness and phenotype, only genotype is preserved. */
    cic::Individual* clone() { return new symreg::Individual(dynamic_cast<symreg::Genotype*>(this->_genotype->clone())); };

};


#endif /* defined(__hyperGP__cic_genetic_symreg_Individual__) */









//
//  hyperGP_sim.cpp
//  TestSim
//
//  Created by Honza Dvorsky on 3/24/13.
//  Copyright (c) 2013 Honza Dvorsky. All rights reserved.
//

#include "hyperGP_sim2.h"

#include <iostream>
#include <string.h>
#include <sim/sim.hpp>
#include <sim/world.hpp>
#include <sim/sensor/camera.hpp>
#include <sim/comp/sssa.hpp>
#include <sim/msg.hpp>
#include <sim/comp/povray.hpp>

#include "neuralController.h"

#include <map>
#include <sys/stat.h>

using sim::Vec3;
using sim::Quat;
using sim::Time;
using namespace std;

bool use_ode = true;
bool use_gui = false;
bool use_wheels = false;
bool use_boxes = true;
bool sim_time_real = use_gui;

class SSSA;
class S;
class RobotsManager;

SSSA *active = 0;

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class SSSA : public sim::comp::SSSA
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
public:
    SSSA(const Vec3 &pos, const Quat &rot = Quat(0., 0., 0., 1.))
    : sim::comp::SSSA(use_wheels, use_boxes, pos, rot)
    {
    }
    
    void init(sim::Sim *sim)
    {
        sim::comp::SSSA::init(sim);
        sim->regMessage(this, sim::MessageKeyPressed::Type);
    }
    
    void processMessage(const sim::Message &msg)
    {
        if (msg.type() == sim::MessageKeyPressed::Type){
            if (active == this)
                _keyPressedMsg((const sim::MessageKeyPressed &)msg);
        }
    }
    
    void _keyPressedMsg(const sim::MessageKeyPressed &msg)
    {
        int key = msg.key();
        
DBG("Component: " << this << " - key pressed: " << msg.key());
        
        if (key == 'h'){
            _robot->addVelLeft(0.1);
        }else if (key == 'j'){
            _robot->addVelLeft(-0.1);
        }else if (key == 'k'){
            _robot->addVelRight(0.1);
        }else if (key == 'l'){
            _robot->addVelRight(-0.1);
            
        }else if (key == 'n'){
            _robot->addVelArm(0.1);
        }else if (key == 'm'){
            _robot->addVelArm(-0.1);
        }else if (key == ','){
            _robot->fixArm();
        }else if (key == '.'){
            _robot->unfixArm();
        }else if (key == 'v'){
            _robot->reachArmAngle(M_PI / 4.);
        }else if (key == 'b'){
            _robot->reachArmAngle(-M_PI / 4.);
        }
        DBG("Velocity: " << _robot->velLeft() << " " << _robot->velRight() << " " << _robot->velArm());
    }
};

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class RobotsManager : public sim::Component
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    std::vector<SSSA *> sssas;
    size_t active_id;
    
public:
    RobotsManager()
    : active_id(0)
    {
    }
    
    void addSinCont(sim::Sim* s, sim::robot::SSSA* rob, int name)
    {
        //        SinController *sinc = new SinController(rob,0.5,2,name*M_PI,name);
        //        s->addComponent(sinc);
    }
    
    void init(sim::Sim *sim);
    
    void processMessage(const sim::Message &msg)
    {
        if (msg.type() == sim::MessageKeyPressed::Type){
            const sim::MessageKeyPressed &m = (const sim::MessageKeyPressed &)msg;
            if (m.key() == 'u'){
                _activateRobot(active_id + 1);
            }else if (m.key() == 'i'){
                _activateRobot(active_id - 1);
            }
        }
    }
    
protected:
    void _activateRobot(size_t i)
    {
        std::list<sim::VisBody *> vis;
        std::list<sim::VisBody *>::iterator it, it_end;
        
        if (i >= sssas.size()){
            i = 0;
        }
        
        active_id = i;
        
        if (active){
            active->robot()->chasis()->visBodyAll(&vis);
            
            it = vis.begin();
            it_end = vis.end();
            for (; it != it_end; ++it){
                (*it)->setColor(0., 0.1, 0.7, 0.6);
            }
            
            vis.clear();
        }
        
        active = sssas[i];
        
        vis.clear();
        active->robot()->chasis()->visBodyAll(&vis);
        
        it = vis.begin();
        it_end = vis.end();
        for (; it != it_end; ++it){
            (*it)->setColor(0.7, 0.1, 0., 0.6);
        }
    }
};

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
class S : public sim::Sim
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    
    NeuralController* _nc;
    
public:
    
    /*
    struct RobotType {
        enum Type {
            Simple5 = 0,
            Complex7 = 1,
            Rect57 = 2
        };
    };
     */
    
    S(NeuralController* nc, int robot_type, std::string expPath)
    : Sim(0,0,use_gui), _nc(nc)
    {
        if (use_ode){
            initODE();
        }
        
        setSimulateReal(sim_time_real);
        
        if (use_gui) {
            pauseSimulation();
        }
        
        createArena();
        const SSSA* centerRobot = createRobot(robot_type);
        
        if (!expPath.empty()) {
            std::string path = std::string(expPath + "_povray/");
            mkdir(path.c_str(), 0777);
            
            bool povray = true;
            
            if (povray) {
                std::cout << "Povray exported to path: " << path << std::endl;
                sim::comp::Povray *pc = new sim::comp::Povray(path.c_str());
                addComponent(pc);
            } else {
                _cam = new sim::sensor::Camera();
                _cam->enableDump(path);
                _cam->setLookAt(sim::Vec3(7,-5,6), sim::Vec3(2,1,1));
                _cam->setWidthHeight(400, 400);
                addComponent(_cam);
            }
        }
    }
    
    NeuralController* neuralController() { return _nc; };
    
    
    void initBullet()
    {
    }
    
    void initODE()
    {
        DBG("Using ODE");
        
        sim::WorldODE *w = sim::WorldFactory::ODE();
        
        setWorld(w);
        
        w->setCFM(0.0001);
        w->setERP(0.8);
        w->setStepType(sim::WorldODE::STEP_TYPE_QUICK);
        w->setAutoDisable(0.01, 0.01, 5, 0.);
        
        w->setContactApprox1(true);
        w->setContactApprox2(true);
        w->setContactBounce(0.1, 0.1);
    }
    
    void createArena()
    {
        const double arenaSize = 500;
        osg::Vec4 color(119/255.0, 136/255.0, 153/255.0, 1.);
        sim::Body *c;
        int id;
        sim::World *w = world();
        
        c = w->createBodyCompound();
        sim::Vec3 v3 = Vec3(arenaSize, arenaSize, 0.1);
        id = c->addBox(v3);
        c->visBody(id)->setColor(color);
        c->visBody(id)->setTexture("wood.ppm");
        
        c->activate();
    }
        
    const SSSA* setupComplex7Robot()
    {
        SSSA *rob;
        
        //red == x
        //green == y
        //blue == z
        
        const double widthX = 1.254; // these constants define dimensions of SSSA robots
        const double widthY = 1.254;
        const double initPosX = 0;
        const double initPosY = 0;
        
        //create robot
        vector<sim::Vec3> positions;
        vector<sim::Quat> rotations;
        
        //BODY (not moving)
        //create the center cube
        positions.push_back(Vec3(initPosX, initPosY, .6));
        rotations.push_back(Quat(Vec3(0., 0., 1.), -M_PI / 2.));
        
        //now the front
        positions.push_back(Vec3(initPosX, initPosY + widthY, .6));
        rotations.push_back(Quat(Vec3(0., 0., 1.), -M_PI / 2.));
        
        positions.push_back(Vec3(initPosX, initPosY + 2*widthY, .6));
        rotations.push_back(Quat(Vec3(0., 0., 1.), -M_PI / 2.));
        
        positions.push_back(Vec3(initPosX, initPosY + 3*widthY, .6));
        rotations.push_back(Quat(Vec3(0., 0., 1.), -M_PI / 2.));
        
        //back
        positions.push_back(Vec3(initPosX, initPosY - widthY, .6));
        rotations.push_back(Quat(Vec3(0., 0., 1.), -M_PI / 2.));
        
        positions.push_back(Vec3(initPosX, initPosY - 2*widthY, .6));
        rotations.push_back(Quat(Vec3(0., 0., 1.), -M_PI / 2.));
        
        positions.push_back(Vec3(initPosX, initPosY - 3*widthY, .6));
//        rotations.push_back(Quat(Vec3(0., 0., 1.), -M_PI / 2.));
        rotations.push_back(Quat(Vec3(-sqrt(2)/2, sqrt(2)/2, 0.), M_PI));
        
        
        //LEGS (moving)
        //front left leg
        
        positions.push_back(Vec3(initPosX - 3*widthX, initPosY + 3*widthY, .6));
        rotations.push_back(Quat(Vec3(0., 1., 0.), M_PI));
        
        positions.push_back(Vec3(initPosX - 2*widthX, initPosY + 3*widthY, .6));
        rotations.push_back(Quat(Vec3(0., -sqrt(2)/2, sqrt(2)/2), M_PI));
//        rotations.push_back(Quat(Vec3(0., 1., 0.), M_PI));
        
        positions.push_back(Vec3(initPosX - 1*widthX, initPosY + 3*widthY, .6));
        rotations.push_back(Quat(Vec3(0., 1., 0.), M_PI));
//        rotations.push_back(Quat(Vec3(0., -sqrt(2)/2, sqrt(2)/2), M_PI));
        
        //back left leg
        positions.push_back(Vec3(initPosX - 3*widthX, initPosY - 3*widthY, .6));
        rotations.push_back(Quat(Vec3(0., 1., 0.), M_PI));
        
        positions.push_back(Vec3(initPosX - 2*widthX, initPosY - 3*widthY, .6));
        rotations.push_back(Quat(Vec3(0., -sqrt(2)/2, sqrt(2)/2), M_PI));
//        rotations.push_back(Quat(Vec3(0., 1., 0.), M_PI));
        
        positions.push_back(Vec3(initPosX - 1*widthX, initPosY - 3*widthY, .6));
        rotations.push_back(Quat(Vec3(0., 1., 0.), M_PI));
//        rotations.push_back(Quat(Vec3(0., -sqrt(2)/2, sqrt(2)/2), M_PI));
        
        //front right leg
        positions.push_back(Vec3(initPosX + 1*widthX, initPosY + 3*widthY, .6));
        rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI));
//        rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI/2.0));
        
        positions.push_back(Vec3(initPosX + 2*widthX, initPosY + 3*widthY, .6));
        rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI/2.0));
//        rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI));
        
        positions.push_back(Vec3(initPosX + 3*widthX, initPosY + 3*widthY, .6));
        rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI));
        
        //back right leg
        positions.push_back(Vec3(initPosX + 1*widthX, initPosY - 3*widthY, .6));
        rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI));
//        rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI/2.0));
        
        positions.push_back(Vec3(initPosX + 2*widthX, initPosY - 3*widthY, .6));
        rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI/2.0));
//        rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI));
        
        positions.push_back(Vec3(initPosX + 3*widthX, initPosY - 3*widthY, .6));
        rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI));
        
        //middle legs
        if (_nc->shouldUseMiddleLegs()) {
            
            //middle left leg
            positions.push_back(Vec3(initPosX - 3*widthX, initPosY, .6));
            rotations.push_back(Quat(Vec3(0., 1., 0.), M_PI));
            
            positions.push_back(Vec3(initPosX - 2*widthX, initPosY, .6));
            rotations.push_back(Quat(Vec3(0., -sqrt(2)/2, sqrt(2)/2), M_PI));
//            rotations.push_back(Quat(Vec3(0., 1., 0.), M_PI));
            
            positions.push_back(Vec3(initPosX - 1*widthX, initPosY, .6));
            rotations.push_back(Quat(Vec3(0., 1., 0.), M_PI));
//            rotations.push_back(Quat(Vec3(0., -sqrt(2)/2, sqrt(2)/2), M_PI));
            
            //middle right leg
            positions.push_back(Vec3(initPosX + 1*widthX, initPosY, .6));
            rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI));
//            rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI/2.0));
            
            positions.push_back(Vec3(initPosX + 2*widthX, initPosY, .6));
            rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI/2.0));
//            rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI));
            
            positions.push_back(Vec3(initPosX + 3*widthX, initPosY, .6));
            rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI));
            
        }
        
        if (positions.size() != rotations.size()) {
            throw EXC_SOFTWARE;
        }
        
        SSSA * centerRobot = NULL;
        
        //fill the robot
        for (int i = 0; i < positions.size(); ++i) {
            
            rob = new SSSA(positions[i], rotations[i]);
            
            if (i == 0) {
                centerRobot = rob;
            }
            
            addComponent(rob);
        }
        
        // and as last add component which will connect all robots
        addComponent(new RobotsManager());
        
        return centerRobot;
    }
    
    const SSSA* setupSimple5Robot()
    {
        SSSA *rob;
        
        //red == x
        //green == y
        //blue == z
        
        const double widthX = 1.254; // these constants define dimensions of SSSA robots
        const double widthY = 1.254;
        const double initPosX = 0;
        const double initPosY = 0;
        
        //create robot
        vector<sim::Vec3> positions;
        vector<sim::Quat> rotations;
        
        //BODY (not moving)
        //create the center cube
        positions.push_back(Vec3(initPosX, initPosY, .6));
        rotations.push_back(Quat(Vec3(0., 0., 1.), -M_PI / 2.));
        
        //now the front
        positions.push_back(Vec3(initPosX, initPosY + widthY, .6));
        rotations.push_back(Quat(Vec3(0., 0., 1.), -M_PI / 2.));
        
        //back two cubes
        positions.push_back(Vec3(initPosX, initPosY - widthY, .6));
        rotations.push_back(Quat(Vec3(0., 0., 1.), -M_PI / 2.));
        
        //front hip (not moving)
        positions.push_back(Vec3(initPosX, initPosY + 2*widthY, .6));
        rotations.push_back(Quat(Vec3(0., 0., 1.), -M_PI / 2.));
        
        //HIPS (moving)
        
        //back hip
        
        positions.push_back(Vec3(initPosX, initPosY - 2*widthY, .6));
        rotations.push_back(Quat(Vec3(0., 0., 1.), -M_PI / 2.));
        
        //LEGS (moving)
        //front left leg
        positions.push_back(Vec3(initPosX - widthX, initPosY + 2*widthY, .6));
        rotations.push_back(Quat(Vec3(0., 1., 0.), M_PI));
        
        positions.push_back(Vec3(initPosX - 2*widthX, initPosY + 2*widthY, .6));
        rotations.push_back(Quat(Vec3(0., 1., 0.), M_PI));
        
        //front right leg
        positions.push_back(Vec3(initPosX + widthX, initPosY + 2*widthY, .6));
        rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI));
        
        positions.push_back(Vec3(initPosX + 2*widthX, initPosY + 2*widthY, .6));
        rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI));
        
        //back left leg
        positions.push_back(Vec3(initPosX - widthX, initPosY - 2*widthY, .6));
        rotations.push_back(Quat(Vec3(0., 1., 0.), M_PI));
        
        positions.push_back(Vec3(initPosX - 2*widthX, initPosY - 2*widthY, .6));
        rotations.push_back(Quat(Vec3(0., 1., 0.), M_PI));
        
        //back right leg
        positions.push_back(Vec3(initPosX + widthX, initPosY - 2*widthY, .6));
        rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI));
        
        positions.push_back(Vec3(initPosX + 2*widthX, initPosY - 2*widthY, .6));
        rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI));
        
        if (positions.size() != rotations.size()) {
            throw EXC_SOFTWARE;
        }
        
        SSSA * centerRobot = NULL;
        
        //fill the robot
        for (int i = 0; i < positions.size(); ++i) {
            
            rob = new SSSA(positions[i], rotations[i]);
            
            if (i == 0) {
                centerRobot = rob;
            }
            
            addComponent(rob);
        }
        
        // and as last add component which will connect all robots
        addComponent(new RobotsManager());
        
        return centerRobot;
    }
    
    const SSSA* setupRect57Robot()
    {
        SSSA *rob;
        
        //red == x
        //green == y
        //blue == z
        
        const double widthX = 1.254; // these constants define dimensions of SSSA robots
        const double widthY = 1.254;
        const double initPosX = 0;
        const double initPosY = 0;
        
        //create robot
        vector<sim::Vec3> positions;
        vector<sim::Quat> rotations;
        
        //BODY (not moving)
        //create the center cube
        positions.push_back(Vec3(initPosX, initPosY, .6));
        rotations.push_back(Quat(Vec3(0., 0., 1.), -M_PI / 2.));
        
        //now the front
        positions.push_back(Vec3(initPosX, initPosY + widthY, .6));
        rotations.push_back(Quat(Vec3(0., 0., 1.), -M_PI / 2.));
        
        positions.push_back(Vec3(initPosX, initPosY + 2*widthY, .6));
        rotations.push_back(Quat(Vec3(0., 0., 1.), -M_PI / 2.));
                
        //back
        positions.push_back(Vec3(initPosX, initPosY - widthY, .6));
        rotations.push_back(Quat(Vec3(0., 0., 1.), -M_PI / 2.));
        
        positions.push_back(Vec3(initPosX, initPosY - 2*widthY, .6));
        //        rotations.push_back(Quat(Vec3(0., 0., 1.), -M_PI / 2.));
        rotations.push_back(Quat(Vec3(-sqrt(2)/2, sqrt(2)/2, 0.), M_PI));
        
        //LEGS (moving)
        //front left leg
        
        positions.push_back(Vec3(initPosX - 3*widthX, initPosY + 2*widthY, .6));
        rotations.push_back(Quat(Vec3(0., 1., 0.), M_PI));
        
        positions.push_back(Vec3(initPosX - 2*widthX, initPosY + 2*widthY, .6));
        //        rotations.push_back(Quat(Vec3(0., -sqrt(2)/2, sqrt(2)/2), M_PI));
        rotations.push_back(Quat(Vec3(0., 1., 0.), M_PI));
        
        positions.push_back(Vec3(initPosX - 1*widthX, initPosY + 2*widthY, .6));
        //        rotations.push_back(Quat(Vec3(0., 1., 0.), M_PI));
        rotations.push_back(Quat(Vec3(0., -sqrt(2)/2, sqrt(2)/2), M_PI));
        
        //back left leg
        positions.push_back(Vec3(initPosX - 3*widthX, initPosY - 2*widthY, .6));
        rotations.push_back(Quat(Vec3(0., 1., 0.), M_PI));
        
        positions.push_back(Vec3(initPosX - 2*widthX, initPosY - 2*widthY, .6));
        //        rotations.push_back(Quat(Vec3(0., -sqrt(2)/2, sqrt(2)/2), M_PI));
        rotations.push_back(Quat(Vec3(0., 1., 0.), M_PI));
        
        positions.push_back(Vec3(initPosX - 1*widthX, initPosY - 2*widthY, .6));
        //        rotations.push_back(Quat(Vec3(0., 1., 0.), M_PI));
        rotations.push_back(Quat(Vec3(0., -sqrt(2)/2, sqrt(2)/2), M_PI));
        
        //front right leg
        positions.push_back(Vec3(initPosX + 1*widthX, initPosY + 2*widthY, .6));
        //        rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI));
        rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI/2.0));
        
        positions.push_back(Vec3(initPosX + 2*widthX, initPosY + 2*widthY, .6));
        //        rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI/2.0));
        rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI));
        
        positions.push_back(Vec3(initPosX + 3*widthX, initPosY + 2*widthY, .6));
        rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI));
        
        //back right leg
        positions.push_back(Vec3(initPosX + 1*widthX, initPosY - 2*widthY, .6));
        //        rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI));
        rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI/2.0));
        
        positions.push_back(Vec3(initPosX + 2*widthX, initPosY - 2*widthY, .6));
        //        rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI/2.0));
        rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI));
        
        positions.push_back(Vec3(initPosX + 3*widthX, initPosY - 2*widthY, .6));
        rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI));
        
        //middle legs
        if (_nc->shouldUseMiddleLegs()) {
            
            //middle left leg
            positions.push_back(Vec3(initPosX - 3*widthX, initPosY, .6));
            rotations.push_back(Quat(Vec3(0., 1., 0.), M_PI));
            
            positions.push_back(Vec3(initPosX - 2*widthX, initPosY, .6));
            //            rotations.push_back(Quat(Vec3(0., -sqrt(2)/2, sqrt(2)/2), M_PI));
            rotations.push_back(Quat(Vec3(0., 1., 0.), M_PI));
            
            positions.push_back(Vec3(initPosX - 1*widthX, initPosY, .6));
            //            rotations.push_back(Quat(Vec3(0., 1., 0.), M_PI));
            rotations.push_back(Quat(Vec3(0., -sqrt(2)/2, sqrt(2)/2), M_PI));
            
            //middle right leg
            positions.push_back(Vec3(initPosX + 1*widthX, initPosY, .6));
            //            rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI));
            rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI/2.0));
            
            positions.push_back(Vec3(initPosX + 2*widthX, initPosY, .6));
            //            rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI/2.0));
            rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI));
            
            positions.push_back(Vec3(initPosX + 3*widthX, initPosY, .6));
            rotations.push_back(Quat(Vec3(1., 0., 0.), M_PI));
            
        }
        
        if (positions.size() != rotations.size()) {
            throw EXC_SOFTWARE;
        }
        
        SSSA * centerRobot = NULL;
        
        //fill the robot
        for (int i = 0; i < positions.size(); ++i) {
            
            rob = new SSSA(positions[i], rotations[i]);
            
            if (i == 0) {
                centerRobot = rob;
            }
            
            addComponent(rob);
        }
        
        // and as last add component which will connect all robots
        addComponent(new RobotsManager());
        
        return centerRobot;
    }
    
    const SSSA* setupOneRobot()
    {
        SSSA *rob;
        
        //red == x
        //green == y
        //blue == z
        
        const double widthX = 1.254; // these constants define dimensions of SSSA robots
        const double widthY = 1.254;
        const double initPosX = 0;
        const double initPosY = 0;
        
        //create robot
        vector<sim::Vec3> positions;
        vector<sim::Quat> rotations;
        
        //BODY (not moving)
        //create the center cube
        positions.push_back(Vec3(initPosX, initPosY, .6));
        rotations.push_back(Quat(Vec3(0., 0., 1.), -M_PI / 2.));
                
        if (positions.size() != rotations.size()) {
            throw EXC_SOFTWARE;
        }
        
        SSSA * centerRobot = NULL;
        
        //fill the robot
        for (int i = 0; i < positions.size(); ++i) {
            
            rob = new SSSA(positions[i], rotations[i]);
            
            if (i == 0) {
                centerRobot = rob;
            }
            
            addComponent(rob);
        }
        
        // and as last add component which will connect all robots
        addComponent(new RobotsManager());
        
        return centerRobot;
    }
    
    /*
     Simple5 = 0,
     Complex7 = 1,
     Rect57 = 2,
     One = 3
     */
    
    const SSSA * createRobot(int robot_type)
    {
        switch (robot_type) {
            case 0:
                //simple5
                return setupSimple5Robot();
                break;
            case 1:
                //complex7
                return setupComplex7Robot();
                break;
            case 2:
                //rect57
                return setupRect57Robot();
                break;
                
            case 3:
                //one
                return setupOneRobot();
                break;
                
            default:
                break;
        }
        return NULL;
    }
};



//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void RobotsManager::init(sim::Sim* sim)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    std::list<sim::Component *> comps;
    sim->components(&comps);
    
    std::list<sim::Component *>::iterator it, it_end;
    SSSA *sssa;
    
    it = comps.begin();
    it_end = comps.end();
    for (; it != it_end; ++it){
        sssa = dynamic_cast<SSSA *>(*it);
        
        if (sssa && sssa->robot()){
            DBG(sssa << " " << sssa->robot());
            sssas.push_back(sssa);
        }
    }
    
    std::vector<sim::robot::SSSA*> myrobs;
    
    for (size_t i = 0; i < sssas.size(); i++){
        for (size_t j = i + 1; j < sssas.size(); j++){
            if (!sssas[i]->robot()->connectTo(*sssas[j]->robot())){
                sssas[j]->robot()->connectTo(*sssas[i]->robot());
            }
        }
        
        sssas[i]->robot()->setRelativePos(sssas[i]->robot()->pos());
        myrobs.push_back(sssas[i]->robot());
    }
    
    NeuralController* nc = ((S*)sim)->neuralController();
    nc->setRobots(myrobs);
    sim->addComponent(nc);
    
//    if (sim->cam()) {
//        sim->cam()->attachToBody(sssas[0]->robot()->chasis(), sim::Vec3(-4, -4, 3), sim::Quat(sim::Vec3(0,0,1), M_PI/4));
//        std::cout << "Camera attached." << std::endl;
//    }
    
    sim->regMessage(this, sim::MessageKeyPressed::Type);
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
cic::sim::Simulator_Plugin2::Simulator_Plugin2(const char* experimentPath)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
    //save the path
    _expPath = std::string(experimentPath);
    
    //nothing to be done
    _data = new std::map<double, std::map<nn::neuron_id, double> >();
};

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
double* cic::sim::Simulator_Plugin2::start(sim_range range, bool with_gui, bool shouldUseMiddleLegs)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
//    const clock_t begin_time = clock();
    
    NeuralController* nc = new NeuralController(_data, range);
    nc->setShouldUseMiddleLegs(shouldUseMiddleLegs);
    
    use_gui = with_gui;
//    use_boxes = false;
    sim_time_real = use_gui;
    
    S s(nc, range.robot_type, _expPath);
    
//    std::cout << "Sim load took: " << float( clock () - begin_time ) /  CLOCKS_PER_SEC * 1000 << " millis." << std::endl;

    s.run();
    
    ::sim::Vec3 endPos = nc->endPosition();
    
    double* endP = NULL;
    endP = new double[3];
    
    endP[0] = endPos.x();
    endP[1] = endPos.y();
    endP[2] = endPos.z();
    
    return endP;
}

//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
void cic::sim::Simulator_Plugin2::registerTimeData(double d1, cic::nn::neuron_id d2[], double d3[], size_t count)
//--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
{
//    cout << "registering data for time " << d1 << endl;
    
    std::map<nn::neuron_id, double> inner;
    
    for (int i = 0; i < count; ++i) {
        inner.insert(std::pair<nn::neuron_id, double>(d2[i], d3[i]));
    }
    
    _data->insert( std::pair<double, std::map<cic::nn::neuron_id, double> >(d1, inner) );
}












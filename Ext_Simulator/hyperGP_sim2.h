//
//  hyperGP_sim.h
//  TestSim
//
//  Created by Honza Dvorsky on 3/24/13.
//  Copyright (c) 2013 Honza Dvorsky. All rights reserved.
//

#ifndef __TestSim__hyperGP_sim__
#define __TestSim__hyperGP_sim__

#include <iostream>
#include <map>
#include "sim_consts.h"

namespace cic {
    namespace sim {
        class Simulator_Plugin2;
    }
}

class cic::sim::Simulator_Plugin2
{
    std::map<double, std::map<nn::neuron_id, double> >* _data;
    std::string _expPath;
    
public:
    
    Simulator_Plugin2(const char* experimentPath = "");
    virtual ~Simulator_Plugin2() { delete _data; };

//    virtual void start(const std::map<double, std::map<cic::nn::neuron_id, double> >* data, sim_range range);
//    virtual void start(sim_range range);

    virtual void registerTimeData(double d1, cic::nn::neuron_id d2[], double d3[], size_t count);
    virtual double* start(sim_range range, bool with_gui, bool shouldUseMiddleLegs = true);
    
};




#endif /* defined(__TestSim__hyperGP_sim__) */

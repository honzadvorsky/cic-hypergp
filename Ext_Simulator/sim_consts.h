//
//  sim_consts.h
//  hyperGP
//
//  Created by Honza Dvorsky on 3/25/13.
//  Copyright (c) 2013 Honza Dvorsky. All rights reserved.
//

#ifndef hyperGP_sim_consts_h
#define hyperGP_sim_consts_h

namespace cic {
    
    namespace nn {

/** Neuron id - integer coordinates. */
struct neuron_id {
    
    /** Horizontal coordinate. */
    int x;
    
    /** Vertical coordinate. */
    int y;
    
    /** Constructor of the neuron id. */
    neuron_id(int _x, int _y) : x(_x), y(_y) { };
    
    neuron_id() { };
    
    /** Comparator function so that it can be used as a key for a map. */
    bool const operator<(const neuron_id &o) const {
        return x < o.x || (x == o.x && y < o.y);
        }
        
        };
        
        }
        
        }
        
        /** Marks the start, stop and step of the simulation. */
        struct sim_range
        {
            
            /** Time of start. */
            double start;
            
            /** Time of stop. */
            double stop;
            
            /** Step. */
            double step;
            
            /** Sample avg */
            int sampleAvgCount;
            
            /** Delayed start time. */
            double delayedStart;
            
            /** Robot type. */
            int robot_type;
            
            /** Extra time step divider. */
            int extraStepDivider;
            
            /** Range constructor. */
            sim_range(double _start,
                      double _stop,
                      double _step,
                      int _avgCount,
                      double _delayedStart,
                      int _robot_type,
                      int _extra_step_divider)
            :
            start(_start),
            stop(_stop),
            step(_step),
            sampleAvgCount(_avgCount),
            delayedStart(_delayedStart),
            robot_type(_robot_type),
            extraStepDivider(_extra_step_divider)
            { };
            
            sim_range() { };
        };
        

#endif
